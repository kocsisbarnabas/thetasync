function channel_difference(thetaData, thetaTime)
%CHANNEL_DIFFERENCE(THETADATA, THETATIME) helps to find pyramidal layer in hippocampus
%(based on phase shift in theta filtered unit activity).
%   THETADATA is a matrix ((sampling rate*time)x(number of channels) sized,
%   usually: (20000*5sec)x32) containing a snippet of the recording.
%   THETATIME: time of dominant theta (2x1 vector: start and end point
%   in seconds)
%
%   Section 1: computes and plots the differences between zscored channel
%   recordings.
%   Section 2: computes and plots the differences between phases of channel
%   recordings.
%
%   See also MSHCSP, CHANNEL_SELECTOR_CALLER, CHANNEL_SELECTOR.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 15/06/2017

time = linspace(thetaTime(1),thetaTime(2), size(thetaData, 1)); %time vector for plots

% %SECTION 1:
% %plot the differences between adjacent channels:
% figure; plot(time, diff(zscore(thetaData'))'+repmat([1:1:31], size(thetaData, 1), 1));
% set(gca, 'YTick', [1:31]', 'YTickLabel', num2str([1:31]'));
% title('zscored channel differences');
% xlabel('time (s)')
% ylabel('signal diff')
% %plot of the total differences between adjacent channels:
% figure; plot(sum(abs(diff((zscore(thetaData')))), 2))
% title('zscored channel differences, total');
% xlabel('channel')
% ylabel('differences')

%SECTION 2:
%phase of signals
transf = hilbert(thetaData);
ang = angle(transf);

%Plot phase of signals:
figure; plot(time, ang+repmat([1:1:32].*2*pi, size(ang, 1), 1));
set(gca, 'YTick', [2*pi:2*pi:32*2*pi]', 'YTickLabel', num2str([1:32]'));
title('channel phase differences');
xlabel('time (s)')
ylabel('phase')
%plot of the total differences between adjacent channels phases:
figure; plot(sum(abs(diff(ang.')), 2));
title('channel phase differences, total');
xlabel('channels')
ylabel('differences')

close all;
end
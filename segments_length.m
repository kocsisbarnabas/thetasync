function segments_length()

global rootDir
global workfolder

files = dir([rootDir workfolder 'tsccg\theta_segments']);

for it = 1:length(files)
    if files(it).isdir == 1
        continue
    end
    load([rootDir workfolder 'tsccg\theta_segments\' files(it).name]);
    theta_length = sum(theta);
    delta_length = length(theta)-theta_length;
    save([rootDir workfolder 'tsccg\theta_segments\segments_length\' files(it).name(1:end-4) 'segments_length.mat'], 'theta_length', 'delta_length');
end
end
function data_loader(issave)
%DATA_LOADER   Burst index and oscillation frequency.
%   DATA_LOADER(ISSAVE) loads data from CELL_FEATURES() output and compiles
%   them to one matrix (allCell). It als calculate burstindex and
%   oscillation frequency (to separate slow and fast delta cells) indices.
%   ISSAVE: save?.
%
%   See also MSHCSP, CELL_FEATUES, LOAD_RADPOISSON, RANDPOISSON_PROCESS, RANDPOISSON, CELL_GROUPS.

%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global NSR
global CGWINDOW
global CGBINS
resultdir = fullfile(RESULTDIR, 'MSsync');

% Create matrix for parameters and ACGs
allCell = []; % all data for all cells
animals = [repmat(3, [31, 1]); repmat(4, [30, 1]); repmat(5, [48, 1]); repmat(7, [102, 1]); repmat(8, [261, 1]); repmat(9, [270, 1]); repmat(10, [163, 1])];

% List the content of the input folder
folder1 = dir(fullfile(resultdir, 'cell_features', 'correlograms'));
for it = 3:length(folder1)
    d = load(fullfile(resultdir, 'cell_features', 'correlograms', folder1(it).name));
    s = strfind(folder1(it).name, '_'); % extract cell IDs
    Id = [animals(it-2), str2num(folder1(it).name(6:s(1)-1)), str2num(folder1(it).name(s(1)+1:s(2)-1)), str2num(folder1(it).name(end-4))];
    load(fullfile(resultdir, 'tsccg', 'theta_segments', 'segments_length', folder1(it).name(1:s(1)-1)));
    allCell = [allCell; Id, d.ThAcgThEnergy, d.ThAcgDeEnergy, d.ThAcgWiEnergy, d.DeAcgThEnergy, d.DeAcgDeEnergy, d.DeAcgWiEnergy, ...
        d.ThAcgThInx, d.ThAcgDeInx, d.DeAcgThInx, d.DeAcgDeInx, d.numApTheta, d.numApDelta, ...
        thetaLength, deltaLength, d.thsumacr, d.desumacr, ...
        d.thetaHang, d.thetaHmvl, d.thetaZ, d.thetaPRayleigh, d.thetaU, d.thetaPRao, ...
        d.deltaHang, d.deltaHmvl, d.deltaZ, d.deltaPRayleigh, d.deltaU, d.deltaPRao, ...
        zscore(d.thetaAcg'), zscore(d.deltaAcg')];   % build data table
end

% Create map for column access
keySet = {'animalId', 'recordId', 'cellId', 'shankId', 'ThAcgThEnergy', 'ThAcgDeEnergy', 'ThAcgWiEnergy', 'DeAcgThEnergy', 'DeAcgDeEnergy', 'DeAcgWiEnergy', ...
    'ThAcgThInx', 'ThAcgDeInx', 'DeAcgThInx', 'DeAcgDeInx', 'numApTheta', 'numApDelta', ...
    'thetaLength', 'deltaLength', 'thsumacr', 'desumacr', ...
    'thetaHang', 'thetaHmvl', 'thetaZ', 'thetaPRayleigh', 'thetaU', 'thetaPRao', ...
    'deltaHang', 'deltaHmvl', 'deltaZ', 'deltaPRayleigh', 'deltaU', 'deltaPRao', ...
    'thetaAcg', 'deltaAcg', ...
    'duringDelta', 'duringTheta', 'isBurst', 'isFast'};
acgLe = CGWINDOW * NSR * 2 / CGBINS; % length of ACGs
valueSet = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, [26, 27], 28, 29, 30, 31, 32, [33,34], [35, (acgLe + 34)], [acgLe + 35, (acgLe*2 + 34)], acgLe*2 + 35, acgLe*2 + 36, acgLe*2 + 37, acgLe*2 + 38};
mO = containers.Map(keySet,valueSet); % map object

% Save
if issave
    rdr = fullfile(resultdir, 'data_loader');
    if ~isdir(rdr)
        mkdir(rdr);
    end
    save(fullfile(resultdir, 'data_loader', 'mapObj.mat'), 'mO');
end

% Load thresholds (See LOAD_RANDPOISSON())
load(fullfile(resultdir, 'load_randpoisson', 'duringThetaParams.mat'), 'threshold1', 'threshold2');
ThInth = threshold1;
ThInde = threshold2;
load(fullfile(resultdir, 'load_randpoisson', 'duringDeltaParams.mat'), 'threshold1', 'threshold2');
DeInth = threshold1;
DeInde = threshold2;

% Thresholds to discard noisy ACGs
% USE THE SAME AS USED IN LOAD_RANDPOISSON()!!!
Sumacr = 2000; % integratal of ACG
frtresh = 0.0004; % action potential frequency in 1/msec

% Change nan elements to 0 (make matrix operation computable)
allCell = [allCell, zeros(size(allCell, 1), 2)];
nans = isnan(allCell);
allCell(nans) = 0;

% Not enough data (<AP frequency threshold, or <acg integrate threshold)
allCell(allCell(:, mO('desumacr'))>Sumacr & allCell(:, mO('numApDelta'))./allCell(:, mO('deltaLength'))>frtresh, end-1) = 3;
allCell(allCell(:, mO('thsumacr'))>Sumacr & allCell(:, mO('numApTheta'))./allCell(:, mO('thetaLength'))>frtresh, end) = 3;

% During delta
thetainxes = (allCell(:, mO('DeAcgThInx'))/ThInde > max(allCell(:, mO('DeAcgDeInx'))/DeInde,1)); % cells firing with theta rhythm (theta index> threshold (determined by LOAD_RANDPOISSON))
deltainxes = (allCell(:, mO('DeAcgDeInx'))/DeInde > max(allCell(:, mO('DeAcgThInx'))/ThInde,1)); % cells firing with delta rhythm

% Not enough data (<AP frequency threshold, or <acg integrate threshold):
thetainxes(allCell(:, mO('desumacr'))<Sumacr | allCell(:, mO('numApDelta'))./allCell(:, mO('deltaLength'))<frtresh) = 0;
deltainxes(allCell(:, mO('desumacr'))<Sumacr | allCell(:, mO('numApDelta'))./allCell(:, mO('deltaLength'))<frtresh) = 0;

allCell(thetainxes, end-1) = 2; % theta rhythmic cells under delta
allCell(deltainxes, end-1) = 1; % delta rhythmic cells under delta

% Under theta
thetainxes = (allCell(:, mO('ThAcgThInx'))/ThInth > max(allCell(:, mO('ThAcgDeInx'))/DeInth,1)); %cells firing with theta rhytm (theta index> threshold (determined by LOAD_RANDPOISSON))
deltainxes = (allCell(:, mO('ThAcgDeInx'))/DeInth > max(allCell(:, mO('ThAcgThInx'))/ThInth,1)); %cells firing with delta rhytm

% Not enough data (<AP frequency threshold, or <acg integrate threshold)
thetainxes(allCell(:, mO('thsumacr'))<Sumacr | allCell(:, mO('numApTheta'))./allCell(:, mO('thetaLength'))<frtresh) = 0;
deltainxes(allCell(:, mO('thsumacr'))<Sumacr | allCell(:, mO('numApTheta'))./allCell(:, mO('thetaLength'))<frtresh) = 0;

allCell(thetainxes, end) = 2; %theta rhythmic cells under theta
allCell(deltainxes, end) = 1; %delta rhythmic cells under theta

allCell(nans)= NaN;  % revert to NaNs

% Calculate "burst index":
ThAcgCols = mO('thetaAcg');
allCell = [allCell, mean(allCell(:, (ThAcgCols(1) + acgLe/2 + 20/CGBINS) : (ThAcgCols(1) + acgLe/2 + 60/CGBINS)), 2)./allCell(:, mO('thsumacr'))]; % (15-55 ms)/integrate of theta_acg

% Calculate "speed index"
DeAcgCols = mO('deltaAcg');
allCell = [allCell, mean(allCell(:, (DeAcgCols(1) + acgLe/2 + 440/CGBINS) : (DeAcgCols(1) + acgLe/2 + 700/CGBINS)), 2)./allCell(:, mO('desumacr'))]; % (440-700 ms)/integrate of delta_acg

allCell = sortrows(allCell, [1, 2, 4, 3]); %#ok<NASGU>

% Save
if s
    save(fullfile(resultdir, 'data_loader', 'allCell.mat'), 'allCell');
end

end
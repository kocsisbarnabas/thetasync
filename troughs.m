function tp = troughs(varargin)
% TROUGHS() : Table for delta-theta transitions in hippocampal 
%   troughs() output is a delta-theta transition point in the specified
%   recording.
%
%   See also ANALYSE_ACG_IMAGESC, MSHCsp.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 01/06/2017

global rootDir;
global workfolder;

if nargin > 0
    record = varargin{1};
end

%TABLE I.: multiple timepoints can belongs to one record
%table record: 20100{1}, -> transition point: {2} (in sec)
% transitions = {3041, 1964; 3042 685; 3043 364; ...
%     3171 2597; 3172	1485; 3173 1630; 3174 1039; ...
%     3291 1520.5; 3292 [501, 833, 1477, 1257.5]; 3293 [442.5, 1833.5, 1805]; 3294 357; ...
%     6021 1062; 6022 1105.5; 6023 [882, 1249]; 6024 [792.5, 1135.3, 1271.8]; 6025, [298.8, 403.5, 661.8, 778.2]; 6026 741.1; 6027 543; 6028 [276, 522]; 6029 581.2; 60210 [100.5, 314]; ...
%     6161 233; 6162 1040; 6163 [992.5, 1150.2]; 6164 946; 6165 1057; 6166 1299.5; 6167 871; 6168 732.5; 6169 838; 61610 758.5; ...
%     7281 1255; 7282 [803, 946]; 7283 [1290, 1407]; 7284 [1236.7, 1376, 1513]; 7285 1138; 7286 883.5; 7287 578.5; 7288 938; 7289 [772.5, 662]; 72810 878; ...
%     80501 [830.7, 1190.8]; 80502 607.5; 80503 607; 80504 607.5; 80505 567; 80506 647};

%TABLE II.: strictly one transition point to one record
transitions = {3041, 1964; 3042 685; 3043 364; ...
    3171 2597; 3172	1485; 3173 1630; 3174 1039; ...
    3291 1520.5; 3292 501; 3293 442.5; 3294 357; ...
    6021 1062; 6022 1105.5; 6023 882; 6024 792.5; 6025, 298.8; 6026 741.1; 6027 543; 6028 276; 6029 581.2; 60210 100.5; ...
    6161 233; 6162 1040; 6163 992.5; 6164 946; 6165 1057; 6166 1299.5; 6167 871; 6168 732.5; 6169 838; 61610 758.5; ...
    7281 1255; 7282 803; 7283 1290; 7284 1236.7; 7285 1138; 7286 883.5; 7287 578.5; 7288 938; 7289 772.5; 72810 878; ...
    80501 830.7; 80502 607.5; 80503 607; 80504 607.5; 80505 567; 80506 647};

index = find([transitions{:}] == record);
tp = transitions{index, 2}; %transition point in the specified record

% status1 = mkdir([rootDir workfolder mfilename]);
% save([rootDir, workfolder, mfilename '\troughs.mat'], 'transitions');
end
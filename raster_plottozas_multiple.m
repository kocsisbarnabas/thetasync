function multiple_raster_plot(varargin)
%MULTIPLE_RASTER_PLOT() Creates raster plot of multiple cells. 
%   We can compare multiple cell's APs patterns in one record.
%   multiple_raster_plot() reads in hippocampal unit activity and cut out
%   WINDOWS*2 window around the tP (transition point: delta to theta) from
%   it. Filter to theta and delta band, and standardize it.
%   Than cut out the same period from the specified cell's (only CTB, CTT, CDS, CDF, DT_) activities.
%   Additionally depicts the angles of hippocampal oscillation.
%
%   See also RASTER_PLOTTOZAS_II, MSHCsp.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/05/2017

global RESULTDIR;
global SR;
global NSR;

dr = SR/NSR;

if nargin == 0
    recordInx = 36; %36th record -> '201007285'
    tP = 0; %transition point
    windowS = NSR*6; %Plot tP +/- windowS (tP defined below)
else
    recordInx = varargin(1);
    tP = varargin{2};
    windowS = varargin(3);
end


%load cells matrix:
load(fullfile(RESULTDIR, 'data_loader', 'allCell.mat'));
%load map for allCell matrix (mO):
load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));

[recordID, inx] = unique(allCell(:, mO('recordId')), 'stable');

%cellTypes: 1st column are records ID, 2nd: vector of IDs of consthburst cells in the record, 3rd: vector of IDs of consthtonic cells in the record ...
cellTypes = num2cell([(allCell(inx, 1)), recordID]);
inx = [inx; length(allCell(:, 2))+1]; % length(allCell(:, 2))+1 is the endpoint
for rec = 1:length(inx)-1
    consthburst = celltype('CTB');
    cellTypes{rec, 3} = consthburst(ismember(consthburst, inx(rec):inx(rec+1)-1));
    consthtonic = celltype('CTT');
    cellTypes{rec, 4} = consthtonic(ismember(consthtonic, inx(rec):inx(rec+1)-1));
    consdeslo = celltype('CDS');
    cellTypes{rec, 5} = consdeslo(ismember(consdeslo, inx(rec):inx(rec+1)-1));
    consdefas = celltype('CDF');
    cellTypes{rec, 6} = consdefas(ismember(consdefas, inx(rec):inx(rec+1)-1));
    deth = celltype('DT_');
    cellTypes{rec, 7} = deth(ismember(deth, inx(rec):inx(rec+1)-1));
end

groupTable = {'CTB', 'CTT', 'CDS', 'CDF', 'DT_'};

%Define theta filter:
theta_flt = fir1(1024,[3 8]/(NSR/2),'bandpass');
delta_flt = fir1(1024,[0.5 3]/(NSR/2),'bandpass');
    
%Load hippocampal unit activity:
o = load(fullfile(RESULTDIR, 'mspreadeeg_rw', ['20100' num2str(cellTypes{recordInx, 2}) '_radiatum.mat']));
unitAct = double(o.fieldPot); %radiatum
load(fullfile(RESULTDIR, 'tsccg', 'theta_segments',  ['20100' num2str(cellTypes{recordInx, 2})]));
delta = (theta==0); %create delta vector (theta's complementer)
load(fullfile(RESULTDIR, 'tsccg', 'theta_angles', ['20100' num2str(cellTypes{recordInx, 2})]));
load(fullfile(RESULTDIR, 'tsccg', 'delta_angles', ['20100' num2str(cellTypes{recordInx, 2})]));

%get transition point:
tP = troughs(cellTypes{recordInx, 2})*NSR;

%Cut out sections (tp +/- windowS):
unitAct = unitAct(tP-windowS:tP+windowS);
theta = theta(tP-windowS:tP+windowS);
delta = delta(tP-windowS:tP+windowS);
thetaAng = thetaAng(tP-windowS:tP+windowS);
deltaAng = deltaAng(tP-windowS:tP+windowS);

domPhase = [thetaAng.*theta+deltaAng.*delta]; %dominant oscillation phase (concatenate phase vectors)

%filter to theta:
theta_feeg = filtfilt(theta_flt,1,unitAct);
theta_sfeeg = (theta_feeg - mean(theta_feeg)) ./ std(unitAct); %standardize

%filter to delta:
delta_feeg = filtfilt(delta_flt,1,unitAct);
delta_sfeeg = (delta_feeg - mean(delta_feeg)) ./ std(unitAct); %standardize
    
%plot unit activity:
time = 1:length(unitAct);
f1 = figure;
sunitAct = (unitAct-mean(unitAct))./std(unitAct); %standardize
shifty = (max(sunitAct)+abs(min(sunitAct)))/2 + 1;
plot(time/NSR, sunitAct+shifty);
hold on;
plot(time/NSR, theta_sfeeg+shifty);
plot(time/NSR, delta_sfeeg+shifty);
plot(time/NSR, domPhase/pi, 'k');
plot(time/NSR, theta+shifty);


plCells = {};

for it1 = 3:7 %different types of cells (cellTypes 3-7th column):
    for it2 = 1:length(cellTypes{recordInx, it1}) %cells of one type
        cellID = cellTypes{recordInx, it1}(it2);
        plCells = [plCells; groupTable{it1-2}];

        animal = num2str(allCell(cellID, mO('animalId')));
        record = num2str(allCell(cellID, mO('recordId')));
        SHANKNO = num2str(allCell(cellID, mO('shankId')));
        EXAMINED_CELL = allCell(cellID, mO('cellId'));
        
        %Loads res and clu files of cell:
        record_res = ['20100' record '.res.' num2str(SHANKNO)]; %res (timpoints)
        record_clu = ['20100' record '.clu.' num2str(SHANKNO)]; %clu (which cell)
        
        res = load(record_res);
        clu = load(record_clu);
        
        clu = (clu(2:end)); %clu(1) is the number of cells in the record
        
        
        % the time points when cell fires:
        cell = res(clu==EXAMINED_CELL);
        cell = round(cell/dr);
        cell(cell == 0) = 1; %change index if equals to 0 after round
        
        %adjust APs:
        cell = cell(cell>tP-windowS & cell<tP+windowS);
        cell = cell-(tP-windowS);
        
        figure(f1)
        hold on;
%         %Plot APs as points:
%         plot((cell/NSR), ones(1, length(cell'))*(-pi-pi*length(plCells)), 'r.');
        
        %Plot APs as lines
        line([(cell/NSR)'; (cell/NSR)'], [ones(1, length(cell'))*(-1-length(plCells)); ones(1, length(cell'))*(-length(plCells))], 'Color', [1, 0, 0])
        text(0, -0.5-length(plCells), num2str(allCell(cellID, mO('recordId'):mO('cellId')))); %Cell number, shank number -> Cell ID
        
%         %open examined cells acgs:
%         openfig([rootDir workfolder 'cell_features\figures\20100' num2str(allCell(cellID, mO('recordId'))) '_autocorr_of_cell_' num2str(allCell(cellID, mO('cellId'))) '_shn' num2str(allCell(cellID, mO('shankId'))) '.fig']);

%         % open phase preference histogram:
%         openfig([rootDir workfolder 'cell_features\phase_histograms\20100' num2str(allCell(cellID, mO('recordId'))) '_phase_pref_of_cell_' num2str(allCell(cellID, mO('cellId'))) '_shn' num2str(allCell(cellID, mO('shankId'))) '.fig']);
    end
end

%Write on cell's groups:
set(gca, 'YTick', fliplr(-1.5:-1:-(length(plCells)+1)), 'YTickLabel', flipud(plCells))
legend('EEG', 'theta', 'delta', 'domPhase', 'istheta?', 'APs')
xlabel(['transition at ' num2str(tP/NSR) ', +/- ' num2str(windowS/NSR) ' (s)'])
title(['record: 20100' num2str(cellTypes{recordInx, 2})])


end
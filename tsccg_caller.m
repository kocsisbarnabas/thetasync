function tsccg_caller(issave)
% TSCCG_CALLER(ISSAVE) Wrapper function for TSCCG
%   issave: save?, logic varriable
%
%   See also MSHCsp, TSCCG, MSPREADEEG_RW.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR

% Create folder for figures and data
resdir = fullfile(RESULTDIR,'MSsync','tsccg');
if ~exist(resdir,'dir')
    mkdir(resdir);
    
    % Create subfolders:
    mkdir(fullfile(resdir, 'theta_segments')); %for theta segments (logic vector)
    mkdir(fullfile(resdir, 'theta_segments', 'segments_length')); %for segments length
    mkdir(fullfile(resdir, 'theta_angles')); %for theta segments angles
    mkdir(fullfile(resdir, 'delta_angles')); %for delta segments angles
    mkdir(fullfile(resdir, 'eeg_figs')); %for field potential figures
end

%viktor3
for it = 1:3
    record = ['20100304' num2str(it)];
    tsccg(record, issave, resdir);
end

%viktor4
for it = 1:4
    record = ['20100317' num2str(it)];
    tsccg(record, issave, resdir);
end

%viktor5
for it = 1:4
    record = ['20100329' num2str(it)];
    tsccg(record, issave, resdir);
end

%viktor7
for it = 1:10
    record = ['20100602' num2str(it)];
    tsccg(record, issave, resdir);
end

%viktor8
for it = 1:10
    record = ['20100616' num2str(it)];
    tsccg(record, issave, resdir);
end


%viktor9
for it = 1:10
    record = ['20100728' num2str(it)];
    tsccg(record, issave, resdir);
end

%viktor10
for it = 1:6
    record = ['201008050' num2str(it)];
    tsccg(record, issave, resdir);
end

end
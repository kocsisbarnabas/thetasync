function mspreadeeg_caller(issave)
% MSPREADEEG_CALLER(ISSAVE) Wrapper function for MSPREADEEG_RW
%   S: save?, logic varriable
%
%   See also MSHCSP, CHANNEL_SELECTOR, MSPREADEEG_RW.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR

% Create folder for figures and data
resdir = fullfile(RESULTDIR,'MSsync','mspreadeeg_rw');
if ~exist(resdir,'dir')
    mkdir(resdir);
end

%animal 3
animal = 'viktor3\';
chosen_channel = 41; %pyramidal layer on output figure: 41+8-32 = 17
% pre-defined value (based on analysing the phase changes)
% This channel is 8*50 mikrometers under the pyramidal layer (in the
% radiatum), because theta amplitude is bigger here
for it = 1:3
    record = ['20100304' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal 4
animal = 'viktor4\';
chosen_channel = 45; %pyramidal layer on output figure: 45+8-32 = 21
for it = 1:4
    record = ['20100317' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal 5
animal = 'viktor5\';
chosen_channel = 46; %pyramidal layer on output figure: 46+8-32 = 22
for it = 1:4
    record = ['20100329' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal  7
animal = 'viktor7\';
chosen_channel = 56; %pyramidal layer on output figure: 56+8-32 = 32
for it = 1:10
    record = ['20100602' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal 8
animal = 'viktor8\';
chosen_channel = 49; %pyramidal layer on output figure: 49+8-32 = 25
for it = 1:10
    record = ['20100616' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal 9
animal = 'viktor9\';
chosen_channel = 56; %pyramidal layer on output figure: 56+8-32 = 32
for it = 1:10
    record = ['20100728' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end

%animal 10
animal = 'viktor10\';
chosen_channel = 56; %pyramidal layer on output figure: 56+8-32 = 32
for it = 1:6
    record = ['201008050' num2str(it)];
    mspreadeeg_rw(animal, record, chosen_channel, issave, resdir);
end


end
function [outputs, chosen_cell1] = randpoisson_process(animal, recording, shankId, cellId)
%RANDPOISSON_PROCESS(ANIMAL, RECORDING, SHANKID, CELLID) (called by RANDPOISSON_PROCESS_SHANK)
%calculates autocorrelation for modified (but statistically equivalent) timevectors of cells.
%   Creates firing pattern vector based on the cell original firing pattern,
%   shuffled with a poisson process. Then calculates auto correlation (and analyse them).
%   Other properties are the same as CELL_FEATURES().
%   Parameters:
%   ANIMAL animalId
%   RECORDING recordingId
%   SHANKID shankId
%   CELLID cellId
%
%   See also MSHCsp, RANDPOISSON_PROCESS_SHANK_CALLER, RANDPOISSON_PROCESS_SHANK, LOAD_RANDPOISSON, CELL_FEATURES, RANDPOISSON, CORRELATION, THETAINDEX, DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global DATADIR
global SR
global NSR
global CGBINS
display = 0; % don't plot results
resdir = fullfile(RESULTDIR, 'MSsync');

% Sampling rate
dr = SR / NSR;

% Initalize output arrays
outputs = nan(1,16);
chosen_cell1 = nan(1,4); 

% Load theta segment assignment (logical array)
load(fullfile(resdir, 'tsccg', 'theta_segments', recording), 'theta');

% Load res and clu files
res = load(fullfile(DATADIR, ['viktor' num2str(animal)], [recording '.res.' num2str(shankId)])); %res (timpoints)
clu = load(fullfile(DATADIR, ['viktor' num2str(animal)], [recording '.clu.' num2str(shankId)])); %clu (which activation time)
clu = (clu(2:end)); % clu(1) is the number of cells in the recording

% The time point when cell_1 fires
actTime = res(clu==cellId); % activity pattern
actTime = round(actTime/dr);
actTime(actTime == 0) = 1; % change index if equals to 0 after round
actPattern = zeros(size(theta, 2), 1);
actPattern(actTime) = 1; % index in the actPattern (1 where actTime fires, 0 where not)
if sum(actTime)<10  % if not enough APs recorded return
    return
end

% During THETA: cut non-theta segments
thetaActPattern = actPattern;
thetaActPattern(theta~=1) = [];
numApTheta = sum(thetaActPattern); % number of action potentials under theta of cell

% Create frequency-matched Poisson process
thetaPoissonActTime = randpoisson(numApTheta, length(thetaActPattern));
thPoissonPtrn = zeros(size(thetaActPattern));
thPoissonPtrn(thetaPoissonActTime) = 1;

% During DELTA: cut THETA segments
deltaActPattern = actPattern;
deltaActPattern(theta==1) = [];
numApDelta = sum(deltaActPattern); % number of action potentials under delta of cell

% Create frequency-matched Poisson process
deltaPoissonActTime = randpoisson(numApDelta, length(deltaActPattern));
dePoissonPtrn = zeros(size(deltaActPattern));
dePoissonPtrn(deltaPoissonActTime) = 1;

% Autocorrelations
[thetaAcg, thsumccr, alag] = correlation(thPoissonPtrn, thPoissonPtrn, display);
ThAcgThInx = thetaindex(thetaAcg, alag, display);
ThAcgDeInx = deltaindex(thetaAcg, alag, display);
[ThAcgThEnergy, ThAcgDeEnergy, ThAcgWiEnergy] = spectralanalysis(thetaAcg(length(thetaAcg)/2:end), NSR/CGBINS, display); % NSR/bins is the sampling rate on correlograms
% hold on
[deltaAcg, desumccr, alag] = correlation(dePoissonPtrn, dePoissonPtrn, display);
DeAcgThInx = thetaindex(deltaAcg, alag, display);
DeAcgDeInx = deltaindex(deltaAcg, alag, display);
[DeAcgThEnergy, DeAcgDeEnergy, DeAcgWiEnergy] = spectralanalysis(deltaAcg(length(deltaAcg)/2:end), NSR/CGBINS, display); % NSR/bins is the sampling rate on correlograms
% title(['cross correlation cells:' num2str(cellId) ' and ' num2str(EXAMINED_CELL_2)])
% xlabel('msec');
% legend('under theta', 'under delta');
% hold off

% Load segments length
load(fullfile(resdir,  'tsccg', 'theta_segments', 'segments_length', recording));

chosen_cell1 = [animal, str2num(recording), shankId, cellId]; %animal, recording, shankId, cellId
outputs = [ThAcgThEnergy, ThAcgDeEnergy, ThAcgWiEnergy, DeAcgThEnergy, DeAcgDeEnergy, DeAcgWiEnergy, ThAcgThInx, ThAcgDeInx, DeAcgThInx, DeAcgDeInx, numApTheta, numApDelta, thetaLength, deltaLength, thsumccr, desumccr, thetaAcg', deltaAcg'];
end
%counts the recorded cells
animals = [3 4 5 7 8 9 10];
fsize = [3, 4, 4, 10, 10, 10, 6];
file_vector = [[201003041 201003042 201003043] [201003071 201003072 201003073 201003074] [201003291 201003292 201003293 201003294] [201006021 201006022 201006023 201006024 201006025 201006026 201006027 201006028 201006029 2010060210] [201006161 201006162 201006163 201006164 201006165 201006166 201006167 201006168 201006169 2010061610] [201007281 201007282 201007283 201007284 201007285 201007286 201007287 201007288 201007289 2010072810] [2010080501, 2010080502, 2010080503, 2010080504, 2010080505, 2010080506]];

recorded_cell_num = [];

for it = 1:length(animals)
    path2 = ['C:\Users\kocsis.barnabas\Desktop\MSHCsp\viktor' num2str(animals(it)) '\'];
    for it2 = 1:fsize(it)
        file2 = num2str(file_vector(sum(fsize(1:it-1))+it2));
        for s = 1:4
            SHANKNO2 = s;
            if exist ([path2 file2 '.clu.' num2str(s)], 'file') ==2
                clu = load([path2 file2 '.clu.' num2str(s)]);
                recorded_cell_num = [recorded_cell_num clu(1)];
            else
                recorded_cell_num = [recorded_cell_num NaN];
            end
        end
    end
end
function imageccgs(ccgs, thetaIndices, deltaIndices, xticks, xlabels)
% IMAGECCGS plots ccgs list (one row = one ccg) as an image.
%
%   See also CELL_GROUPS, DATA_LOADER, CELLTPYES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global rootDir;
global workfolder;
global sr;
global nsr;
global windowsize;
global bins;


figure; subplot(1, 2, 1);
imagesc(ccgs(:,thetaIndices(1):thetaIndices(2)));
set(gca,'clim',[-2 2]);
set(gca,'xtick',xticks);
set(gca,'xticklabel',xlabels)
xlabel('msec')
title('during theta');

text(zeros(1, size(ccgs, 1)), [1:size(ccgs, 1)], num2str(ccgs(:, 2:4)));

subplot(1, 2, 2);
imagesc(ccgs(:,deltaIndices(1):deltaIndices(2)));
set(gca,'clim',[-2 2]);
set(gca,'xtick',xticks);
set(gca,'xticklabel',xlabels)
xlabel('msec')
title('during delta');

text(zeros(1, size(ccgs, 1)), [1:size(ccgs, 1)], num2str(ccgs(:, 5:6)));

end
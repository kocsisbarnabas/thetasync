function ccg_calculations(varargin)
%CCG_CALCULATIONS(VARARGINS) calculates cross correlation under theta, delta.
%   CCG_CALCULATIONS(THETA, ANIMAL, RECORDING, SHANKID1, SHANKID2, CELLID1, CELLID2, MAXLAG, XBINS, ISSAVE, STR, DISPLAY)
%   Loads in res (action potential's timepoints) and clu (which cell fires)
%   files concerning to RECORDS (examined eeg). THETA is a row vector 
%   (1 at dominant theta times, 0 elsewhere).
%   SHANKID1-2: recorder shanks of cells
%   CELLID1-2: cell IDs in clu files
%   MAXLAG is the windowsize for crosscorrelation
%   ISSAVE: save?
%   STR: 3 letter ID of cell group (string, e.g.: 'CTB' -> constitutive bursting theta cells),
%   DISPLAY controlls whether to plot ccg.
%
% Example:
%   Learning and memory: Two cells are synchronized under theta rythm.
%   cell_1 fires at 20, 40, 60, 174!, 186!, 270. For exampple, 20
%   means 0.1 sec after downsampling (200 HZ*20th=0.1 sec).
%   Action potential's times (0.1, 0.2, 0.4, 0.87!, 0.93!, 1.35 sec)
%   cell_2 fires at 34, 54, 104, 182!, 222, 288. For example, 34 means
%   0.17 sec after downsampling (200 HZ*34th=0.17).
%   Action potential's places (0.17, 0.27, 0.52, 0.91!, 1.11, 1.44 sec)
%   Theta lasts from 0.1 to 0.6 and 1.1 to 1.5 sec (1 in these intervalls).
%   Outside delta was dominant (0 in those intervalls).
%   Action potentials denoted with !, are out of theta. So
%   theta_timeseries will be 0 at those points.
%   At other times action potentials 'enabled', and valuated with 1 in
%   theta_timeseries.
%   Call [acor, lag]=xcorr() function, and plot(lag, acor) clearly
%   shows peaks at +6 and -14 (0.03 and -0.07 sec). We can say that
%   cell_2 fires after cell_1 with 0.07 delay. Obviously, the two cells
%   are synchronized under theta.
%
%   See also MSHCSP, SOROZAT_CCG_INTRAGROUPS, SOROZAT_CCG_INTERGROUPS, CELL_GROUPS, CORRELATION, CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR;
global DATAPATH;
global SR;
global NSR;


p = inputParser;
defaultAnimal = 3;
defaultRecording = '201003041';
defaultShankId1 = 2;
defaultShankId2 = 2;
defaultCellId1 = 2;
defaultCellId2 = 3;
defaultMaxlag = 3000; %3 sec
defaultBinS = 20;
defaultSave = false;
defaultGroup = '';
defaultDisplay = true;

addOptional(p, 'theta', @isnumeric);
addOptional(p, 'animal', defaultAnimal, @isnumeric);
addOptional(p, 'recording', defaultRecording, @ischar);
addOptional(p, 'shankId1', defaultShankId1, @isnumeric);
addOptional(p, 'shankId2', defaultShankId2, @isnumeric);
addOptional(p, 'cellId1', defaultCellId1, @isnumeric);
addOptional(p, 'cellId2', defaultCellId2, @isnumeric);
addOptional(p, 'maxlag', defaultMaxlag, @isnumeric);
addOptional(p, 'xBinS', defaultBinS, @isnumeric);
addOptional(p, 'issave', defaultSave, @islogical);
addOptional(p, 'rhtgroup', defaultGroup, @ischar);
addOptional(p, 'display', defaultDisplay, @isnumeric);
parse(p,varargin{:});

theta = p.Results.theta;
animal = p.Results.animal;
recording = p.Results.recording;
shankId1 = p.Results.shankId1;
shankId2 = p.Results.shankId2;
cellId1 = p.Results.cellId1;
cellId2 = p.Results.cellId2;
maxlag = p.Results.maxlag;
xBinS = p.Results.xBinS;
issave = p.Results.issave;
rhtgroup = p.Results.rhtgroup;
display = p.Results.display;

dr = SR/NSR;

if size(varargin)==0
    load(fullfile(RESULTDIR,  'tsccg', 'theta_segments', recording), 'theta');
end

%Loads res and clu files:
res = load(fullfile(DATAPATH, ['viktor' num2str(animal)], [recording '.res.' num2str(shankId1)])); %res (timpoints)
clu = load(fullfile(DATAPATH, ['viktor' num2str(animal)], [recording '.clu.' num2str(shankId1)])); %clu (which activation time)

clu = (clu(2:end)); %clu(1) is the number of cells in the record

% the time point when cell_1 fires:
actTime1 = res(clu == cellId1);
actTime1 = round(actTime1/dr);
actTime1(actTime1 == 0) = 1; %change index if equals to 0 after round

%in case of the same shank:
clu2 = clu; 
res2 = res;

%in case of distinct shanks:
if shankId1 ~= shankId2
    res2 = load(fullfile(DATAPATH, ['viktor' num2str(animal)], [recording '.res.' num2str(shankId2)])); %res (timpoints)
    clu2 = load(fullfile(DATAPATH, ['viktor' num2str(animal)], [recording '.clu.' num2str(shankId2)])); %clu (which activation time)
    
    clu2 = (clu2(2:end));
end

% the time point when cell_2 fires:
actTime2 = res2(clu2 == cellId2);
actTime2 = round(actTime2/dr);
actTime2(actTime2 == 0) = 1; %change index if equals to 0 after round

%create timeseries:
actPattern1 = zeros(size(theta, 2), 1);
actPattern2 = zeros(size(theta, 2), 1);

 % index in the timeseries (1 where cell fires, 0 where not)
actPattern1(actTime1) = 1;
actPattern2(actTime2) = 1;

%if not enough APs recorded return
if sum(actPattern1)<10 || sum(actPattern2)<10
    return
end

% timeserie under theta (1 where cell fires and there is theta, 0 elsewhere)
thetaActPattern1 = actPattern1.*theta';
thetaActPattern2 = actPattern2.*theta';

%create delta vector (0 at dominant theta, 1 at dominant delta APs)
delta = (theta==0);
% timeserie under delta (1 where cell fires and there is delta, 0 elsewhere)
deltaActPattern1 = actPattern1.*delta';
deltaActPattern2 = actPattern2.*delta';

%cross correlation:
f1 = figure;
%cross correlation under theta:
[thetaCcg, thsumccr, clag] = correlation(thetaActPattern1, thetaActPattern2, display, maxlag, xBinS);
hold on
%cross correlation under delta:
[deltaCcg, desumccr, clag] = correlation(deltaActPattern1, deltaActPattern2, display, maxlag, xBinS);
title(['cross correlation cells: ' recording ', ' num2str(cellId1) ' and ' num2str(cellId2) ', shanks: ' num2str(shankId1) ', ' num2str(shankId2) '; '])
xlabel('msec');
legend('under theta', 'under delta');
if issave
    savefig(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'figures', rhtgroup, [recording '_' num2str(cellId1) '_and_' num2str(cellId2) '_shn1_' num2str(shankId1) '_shn2_' num2str(shankId2) '.fig'])); 
end
hold off
close all
if issave
    save(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'ccgs', rhtgroup, [recording '_' num2str(cellId1) '_and_' num2str(cellId2) '_shn1_' num2str(shankId1) '_shn2_' num2str(shankId2) '.mat']), 'thetaCcg', 'thsumccr', 'deltaCcg', 'desumccr', 'clag');
end

end
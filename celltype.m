function inx = celltype(str)
%CELLTYPE   Grouping of MS cell types.
%   CELLTYPE(STR) return indices (in allCell matrix) of cells that belong
%   to the group specified in STR.
%
%   See also MSHCSP, DATA_LOADER, CELL_GROUPS, CELL_FEATUES, LOAD_RANDPOISSON.

%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
resultdir = fullfile(RESULTDIR, 'MSsync');

% Load data table
load(fullfile(resultdir, 'data_loader', 'allCell.mat'));

% Load map for allCell matrix (mO)
load(fullfile(resultdir, 'data_loader', 'mapObj.mat'));

switch str
    case 'CTB'
        inx = find(allCell(:, mO('duringDelta')) == 2 & allCell(:, mO('duringTheta'))==2 & allCell(:, mO('isBurst'))>0); % constitutive theta bursting
    case 'CTT'
        inx = find(allCell(:, mO('duringDelta')) == 2 & allCell(:, mO('duringTheta'))==2 & allCell(:, mO('isBurst'))<0); % constitutive theta tonic
    case 'CDS'
        inx = find(allCell(:, mO('duringDelta')) == 1 & allCell(:, mO('duringTheta'))==1 & allCell(:, mO('isFast'))<0); % constitutive delta slow
    case 'CDF'
        inx = find(allCell(:, mO('duringDelta')) == 1 & allCell(:, mO('duringTheta'))==1 & allCell(:, mO('isFast'))>0); % constitutive delta fast
    case 'TD_'
        inx = find(allCell(:, mO('duringDelta')) == 2 & allCell(:, mO('duringTheta'))==1); %theta-delta cells (theta under delta, delta under theta)
    case 'DT_'
        inx = find(allCell(:, mO('duringDelta')) == 1 & allCell(:, mO('duringTheta'))==2); %delta-theta cells
    case 'NT_'
        inx = find(allCell(:, mO('duringDelta')) == 3 & allCell(:, mO('duringTheta'))==2); %nothing-theta cells
    case 'ND_'
        inx = find(allCell(:, mO('duringDelta')) == 3 & allCell(:, mO('duringTheta'))==1); %nothing-delta cells
    case 'TN_'
        inx = find(allCell(:, mO('duringDelta')) == 2 & allCell(:, mO('duringTheta'))==3); %theta-nothing cells
    case 'DN_'
        inx = find(allCell(:, mO('duringDelta')) == 1 & allCell(:, mO('duringTheta'))==3); %delta-nothing cells
    case 'NN_'
        inx = find(allCell(:, mO('duringDelta')) == 3 & allCell(:, mO('duringTheta'))==3); %nothing-nothing cells
    case 'ABS'
        inx = find(allCell(:, mO('duringDelta')) == 0 & allCell(:, mO('duringTheta'))==2); %not enough datas
end

end
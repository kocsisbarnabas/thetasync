function cell_features_shank(theta, theta_ang, delta_ang, animal, recording, issave, resdir)
%CELL_FEATURES_SHANK(THETA, THETA_ANG, DELTA_ANG, RECORDING, ISSAVE, RESDIR) 
%wrapper function for CELL_FEATURES()
%   The function iterates trough each shank.
% 
%   THETA: timevector (1 during dominant theta, 0 during dominant delta,
%   e.g.: 010001110...), THETA_ANG: phase vector (theta phase during dominant theta)
%   THETA_ANG: phase vector (theta phase during dominant theta)
%   DELTA_ANG: phase vector (delta phase during dominant delta)
%   ANIMAL animalID
%   RECORDING (file name)  
%   ISSAVE: save?
%   RESDIR: target directory to save outputs
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK_CALLER, CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global DATADIR

for shankId =1:4
    if exist (fullfile(DATADIR, animal ,[recording '.clu.' num2str(shankId)]), 'file') == 2
        clu = load(fullfile(DATADIR, animal ,[recording '.clu.' num2str(shankId)]));
        for cellId = 2:clu(1) % clu(1) is the number of cells on the actual shank
            cell_features(theta, theta_ang, delta_ang, animal, recording, shankId, cellId, issave);
            close all;
        end
    else
        [recording '.clu.' num2str(shankId) ' doesnt exist']
    end
end
end
function load_randpoisson(segm, issave)
%LOAD_RANDPOISSON(SEGM, ISSAVE) compute thresholds for theta and delta indices
%(or for any calculated parameters).
%   First sort indices (computed by RANDPOISSON_PROCESS()) of cells, and
%   find the highest 5% (above significance level), and define threshold
%   according to that.
%   SEGM is a string (can be 'theta' or 'delta') controlling which segments
%   should be analysed
%   ISSAVE: save?.
%
%   See also MSHCSP, RANDPOISSON_PROCESS, RANDPOISSON.

%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

% Directories
global RESULTDIR
resultdir = fullfile(RESULTDIR, 'MSsync');

% Load matrix
load(fullfile(resultdir, 'randpoisson_process', 'randpoisson.mat')); % load randpoisson matrix
load(fullfile(resultdir, 'randpoisson_process', 'RandPomapObj.mat')); % load map for matrix (randmO):

% Thresholding parameters
signifLevel = 5; % significance level, in percent
signif = 100 / signifLevel;
Thsumacr = 2000; % Threshold for sum(acr)
ThApfr = 0.0004; % Threshold fo action potential frequency
Desumacr = 2000;
DeApfr = 0.0004;

% Plot thresholds
if strcmp(segm, 'delta')
    %under delta:
    outputVector(outputVector(:, randmO('desumacr'))<Desumacr | outputVector(:,randmO('numApDelta'))./outputVector(:, randmO('delta_length'))<ThApfr, :) = [];
    figure; 
    plot(outputVector(:, randmO('DeAcgThInx')), outputVector(:, randmO('DeAcgDeInx')), '.');
    title({'randpoisson, under delta: TH vs DE inx'; ['criterion: sum(acr)>' num2str(Desumacr)]});
    xlabel('Theta index')
    ylabel('Delta index')
    
    Inxsort1 = sortrows(outputVector, randmO('DeAcgThInx'));
    Inxsort2 = sortrows(outputVector, randmO('DeAcgDeInx'));
    threshold1 = Inxsort1(end-round(length(Inxsort1)/signif), randmO('DeAcgThInx')); % threshold1 (only 5% is above that)
    threshold2 = Inxsort2(end-round(length(Inxsort2)/signif), randmO('DeAcgDeInx')); % threshold2
end

if strcmp(segm, 'theta')
    %under theta:
    outputVector(outputVector(:, randmO('thsumacr'))<Thsumacr | outputVector(:,randmO('numApTheta'))./outputVector(:, randmO('theta_length'))<DeApfr, :) = [];
    figure; 
    plot(outputVector(:, randmO('ThAcgThInx')), outputVector(:, randmO('ThAcgDeInx')), '.');
    title({'randpoisson, under theta: TH vs DE inx'; ['criterion: sum(acr)>' num2str(Thsumacr)]});
    xlabel('Theta index')
    ylabel('Delta index')
    
    Inxsort1 = sortrows(outputVector, randmO('ThAcgThInx'));
    Inxsort2 = sortrows(outputVector, randmO('ThAcgDeInx'));
    threshold1 = Inxsort1(end-round(length(Inxsort1)/signif), randmO('ThAcgThInx')); % threshold1 (only 5% is above that)
    threshold2 = Inxsort2(end-round(length(Inxsort2)/signif), randmO('ThAcgDeInx')); % threshold2
end

hold on
plot([threshold1, threshold1], [-1, 1], 'k'); %display threshold
hold on
plot([-1, 1], [threshold2, threshold2], 'k');
plot([threshold1, 2*threshold1], [threshold2, 2*threshold2], 'k');
text(threshold1, 0.4, num2str(threshold1));
text(0, threshold2, num2str(threshold2));

% Save
if issave
    rdr = fullfile(resultdir, 'load_randpoisson');
    if ~isdir(rdr)
        mkdir(rdr);
    end
end
if strcmp(segm, 'delta') && issave
    save(fullfile(resultdir, 'load_randpoisson', 'duringDeltaParams.mat'), 'threshold1', 'threshold2');
    savefig(fullfile(resultdir, 'load_randpoisson', 'duringDelta.fig'));
end
if strcmp(segm, 'theta') && issave
    save(fullfile(resultdir, 'load_randpoisson', 'duringThetaParams.mat'), 'threshold1', 'threshold2');
    savefig(fullfile(resultdir, 'load_randpoisson', 'duringTheta.fig'));
end

close all
end
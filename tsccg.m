function tsccg(varargin)
%TSCCG(VARARGIN) loads hippocampal field potential data (from anesthetized
%mouse -> slower oscillations), hilbert transform it, handle theta and delta
%segments for correlation calculations.
%   Filters field data for theta (THBAND) and for delta (DEBAND). (For this 
%   purposes bandpass filter is used.) Centralize, normalize 
%   ((feeg-mean(feeg))/standard deviation(eeg)) and hilbert transform 
%   (calculating angle and amplitude) them.
%   Definition: if theta's /delta's amplitude's ratio>1, theta is dominant
%   against delta (their ratio is filtered with moving average)
%   Parameters:
%   RECORD (file name)
%   S (save?)
%   RESDIR: target directory to save outputs
%
%   See also MSHCsp, TSCCG_CALLER, CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global NSR
global THBAND
global DEBAND

% Input arguments
defaultRecord = '201003042';
defaultSave = false;
defaultResdir = fullfile(RESULTDIR,'tsccg');

p = inputParser;
addOptional(p, 'record', defaultRecord, @ischar);
addOptional(p, 's', defaultSave, @islogical);
addOptional(p, 'resdir', defaultResdir, @ischar)
parse(p,varargin{:});

recording = p.Results.record;
s = p.Results.s;
resdir = p.Results.resdir;

% Load data
o = load(fullfile(RESULTDIR, 'MSsync\mspreadeeg_rw', [recording '_radiatum.mat']));
field = o.fieldPot; %radiatum EEG
nyquist_f = NSR / 2; %nyquist frequency
field = double(field);

% Filter in the theta band
thetaFilter = fir1(1024,THBAND/nyquist_f,'bandpass');
thetaFeeg = filtfilt(thetaFilter,1,field);
thetaSFeeg = (thetaFeeg - mean(thetaFeeg)) ./ std(field); % standardize feeg

% Hilbert transform:
thetaTransf = hilbert(thetaSFeeg);
thetaAng = angle(thetaTransf);
thetaAmp = abs(thetaTransf);

% Filter in the delta band
deltaFilter = fir1(1024,DEBAND/nyquist_f,'bandpass');
deltaFeeg = filtfilt(deltaFilter,1,field);
deltaDFeeg = (deltaFeeg - mean(deltaFeeg)) ./ std(field);

% Hilbert transform:
deltaTransf = hilbert(deltaDFeeg);
deltaAng = angle(deltaTransf);
deltaAmp = abs(deltaTransf);
time = 1:length(field);

%common figure:
% figure
% plot(time/NSR, seeg, 'b')
% hold on
% plot(time/NSR, thetaSFeeg, 'r')
% hold on
% plot(time/NSR, deltaDFeeg, 'g')
% hold on
% plot(time/NSR, thetaAmp, 'k')
% hold on
% plot(time/NSR, deltaAmp, 'y')
% hold off
% legend('field', 'theta', 'delta', 'theta amplitude', 'delta amplitude');
% title(['urethane filtered for theta and delta'  file]);
% xlabel('Seconds');

% Theta-delta ratio
ratio_feeg = thetaAmp ./ (deltaAmp);
ratio_feeg(ratio_feeg>10) = 10;

% Smooth ratio with moving average:
wSize = NSR * 5;
coeff = ones(1, wSize)/wSize;
ratioSFeeg = filtfilt(coeff, 1, ratio_feeg);

%not necessary:
%plotting the theta/delta ratio:
% figure
% plot(time/ NSR, ratioSFeeg, 'g');
% t=ones(size(ratioSFeeg, 2), 1);
% hold on
% plot(time/NSR, t, 'r')
% legend('ratio', 'threshold');
% title(['urethane theta per delta ratio' file]);
% xlabel('Seconds');

% Find transition points:
theta = [0 ratioSFeeg>1 0];   % thresholding
le = length(ratioSFeeg);
dtheta = diff(theta);
s1 = find(dtheta==1);  % change from non-theta to theta.
s2 = find(dtheta==-1);  % change from theta to non-theta
theta = unifying(s2, s1, theta, NSR); % unify theta segments interrupted with short deltas

% Detect transition points again (after short delta segments were dropped)
theta = [0 theta 0];
dtheta = diff(theta);
s1 = find(dtheta==1);  % change from non-theta to theta.
s2 = find(dtheta==-1);  % change from theta to non-theta

theta = short_killer(s1, s2, theta, NSR); % Erase short theta segments
theta = theta(3:end-2);
thetaLength = sum(theta);
deltaLength = length(theta) - thetaLength;
dtheta = diff(theta);
s1 = find(dtheta==1);  % change from non-theta to theta.
s2 = find(dtheta==-1);

% Save theta sections
if s
	save(fullfile(resdir, 'theta_segments', recording),'theta');
    save(fullfile(resdir, 'theta_segments', 'segments_length', recording), 'thetaLength', 'deltaLength');
    save(fullfile(resdir, 'theta_angles', recording), 'thetaAng');
    save(fullfile(resdir, 'delta_angles', recording), 'deltaAng');
end

%controll plots:
H = figure;
standardizedField = (field-mean(field))./std(field);
hold on
plot(time/NSR, standardizedField)
plot(time/NSR, thetaSFeeg)
plot(time/NSR, deltaDFeeg)
domTh = theta*2;
domTh(s1) = NaN;
domTh(s2) = NaN;
plot(time/NSR, domTh)
plot(time/NSR, (ratioSFeeg + ones(size(ratioSFeeg)))) %shift the plot to make it visible
hold off
title(['urethane theta and delta ' recording]);
legend('standardized field', 'st. and theta filtered field', 'st. and delta filtered field', 'theta', 'averaged ratio');
xlabel('Seconds');
hold off;
if s
    savefig(fullfile(resdir, 'eeg_figs', recording));
end

close all;
end

% -------------------------------------------------------------------------
function theta=short_killer(s1, s2, theta, nsr)
% short_killer rejects theta segments shorter than t1
time = 1;
t1 = nsr * time;
intervall_1 = abs(s2-s1); %identify theta segments
for i = 1:length(intervall_1)
    if intervall_1(i) < t1
        theta(min(s1(i), s2(i)):max(s1(i), s2(i))) = 0;
    end
end
end

% -------------------------------------------------------------------------
function theta = unifying(s2, s1, theta, nsr)
% keep short non-theta segments, that interrupts theta rytms
% t2 is the short non-theta segment's time, that need to keep
time = 1;
t2 = nsr * time;
intervall_2 = abs(s1-s2);
for i = 1:length(intervall_2)
    if intervall_2(i) < t2
        theta(min(s1(i), s2(i))+1:max(s1(i), s2(i))) = 1; %+1 because matrix indices cant be 0
    end
end
end
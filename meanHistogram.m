function meanHistogram(varargin)
%MEANHISTOGRAM() calculates mean histogram of a specified cell type.
%   meanhistogram(CELLTYPE, WW, CIRCULAR) loads is allCell matrix (contains
%   all computed data of all cells), and creates histograms of a specified
%   CELLTYPE. Than computes resultant histogram of the group. If WW = 1 (logic var),
%   than each cell's histograms normalized with the number of action
%   potentials of that cell (-> equal contribution to the resultant
%   histogram).
%   If circular = 1 (logic var), the function displays circular histogram and first
%   trigonometric moments (2D vectors) of cells.
%
%   See also CELL_FEATURES, MSHCsp.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/05/2017


global RESULTDIR;
global SR;
global NSR;
global CGWINDOW;
global CGBINS;

if nargin == 0
    cellType = 'DT_';
    indices = celltype(cellType); %indices of cells (belongs to CELLTYPE) in allCell matrix
    ww = true; %logic: normalize individual histograms with number of APs or not?
    % if ww = 1, than each cell contributes to the resultant histogram with
    % the same weight (independently from the number of its APs)
    circular = 1;
    issave = false;
else
    cellType = varargin{1};
    indices = celltype(cellType);
    ww = varargin{2}; %logic: normalize individual histograms with number of APs or not?
    circular = varargin{3};
    issave = varargin{4};
end

%create folders:
if issave
    status1 = mkdir(fullfile(RESULTDIR, 'cell_groups'));
    status2 = mkdir(fullfile(RESULTDIR, 'cell_groups', cellType));
end


%load cells matrix:
load(fullfile(RESULTDIR, 'data_loader', 'allCell.mat'));
%load map for allCell matrix (mO):
load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));

%edges for histograms:
edges = -pi:(2*pi/18):pi;
centrs = linspace(edges(1), edges(end), length(edges)-1); %calculate centers

ThHist = []; %matrix for histograms during theta
DeHist = []; %matrix for histograms during delta
ThHangs = []; %mean resultant vector angle matrix during theta
DeHangs = []; %mean resultant vector angle matrix during delta
ThHmvls = []; %mean resultant vector length matrix during theta
DeHmvls = []; %mean resultant vector length matrix during delta

for it = 1:length(indices)
    %load phases of APs:
    load(fullfile(RESULTDIR, 'cell_features', 'correlograms', ['20100' num2str(allCell(indices(it), mO('recordId'))) '_' num2str(allCell(indices(it), mO('cellId'))) '_shn' num2str(allCell(indices(it), mO('shankId'))) '.mat']));
    [deltaCnts, edges] = histcounts(deltaPhase, edges);
    [thetaCnts, edges] = histcounts(thetaPhase, edges);
    if ww %normalize?
        deltaCnts = deltaCnts/numApDelta;
        thetaCnts = thetaCnts/numApTheta;
    end
    %     %Plot histograms:
    %     figure; hold on;
    %     stairs(centrs, thetaCnts);
    %     stairs(centrs, deltaCnts);
    
    ThHist = [ThHist; thetaCnts];
    DeHist = [DeHist; deltaCnts];
    ThHangs = [ThHangs, thetaHang];
    DeHangs = [DeHangs, deltaHang];
    ThHmvls = [ThHmvls, thetaHmvl];
    DeHmvls = [DeHmvls, deltaHmvl];
end
%Create resultant histogram
SumThHist = sum(ThHist);
%     SumThHist = SumThHist/sum(SumThHist); %normalize it
SumDeHist = sum(DeHist);
%     SumDeHist = SumDeHist/sum(SumDeHist); %normalize it

if circular %make circular plot?
    centrs2 = [centrs(2:end), centrs(1)];
    %during theta:
    h1 = figure; hold on;
    x1 = SumThHist.*cos(centrs);
    y1 = SumThHist.*sin(centrs);
    x2 = SumThHist.*cos(centrs2);
    y2 = SumThHist.*sin(centrs2);
    line([x1; x2], [y1; y2], 'Color', [1, 0, 0]);
    line([x1; zeros(size(x1))], [y1; zeros(size(y1))], 'Color', [1, 0, 0])
    line([x2; zeros(size(x2))], [y2; zeros(size(y2))], 'Color', [1, 0, 0])
    %resultant vectors:
    xendp = cos(ThHangs).*ThHmvls; %x coordinate of resultant vector endpoint
    yendp = sin(ThHangs).*ThHmvls; %y coordinate of resultant vector endpoint
    quiver(zeros(size(ThHmvls)), zeros(size(ThHmvls)), xendp, yendp);
    %         text(xendp, yendp, num2str(allCell(indices, mO('ThAcgThInx'))));
    title([cellType ' during theta, num of cells: ' num2str(length(indices))]);
    xlim([-1.5, 1.5]), ylim([-1.5, 1.5]), axis equal
    
    %during delta:
    h2 = figure; hold on;
    x1 = SumDeHist.*cos(centrs);
    y1 = SumDeHist.*sin(centrs);
    x2 = SumDeHist.*cos(centrs2);
    y2 = SumDeHist.*sin(centrs2);
    line([x1; x2], [y1; y2], 'Color', [1, 0, 0]);
    line([x1; zeros(size(x1))], [y1; zeros(size(y1))], 'Color', [1, 0, 0])
    line([x2; zeros(size(x2))], [y2; zeros(size(y2))], 'Color', [1, 0, 0])
    %resultant vectors:
    xendp = cos(DeHangs).*DeHmvls; %x coordinate of resultant vector endpoint
    yendp = sin(DeHangs).*DeHmvls; %y coordinate of resultant vector endpoint
    quiver(zeros(size(DeHmvls)), zeros(size(DeHmvls)), xendp, yendp);
    title([cellType ' during delta, num of cells: ' num2str(length(indices))]);
    xlim([-1.5, 1.5]), ylim([-1.5, 1.5]), axis equal
else
    h1 = figure; hold on;
    stairs(centrs, SumThHist);
    stairs(centrs, SumDeHist);
    title([cellType ' during delta, num of cells: ' num2str(length(indices))]);
end

if issave
    savefig(h1, fullfile(RESULTDIR, 'cell_groups', cellType, 'theta_phase_circular.fig'));
    savefig(h2, fullfile(RESULTDIR, 'cell_groups', cellType, 'delta_phase_circular.fig'));
    close all;
end

end
function cell_features(varargin)
%CELL_FEATURES() calculates auto correlation (and analyse them), phase preference under
%theta, delta.
%   CELL_FEATURES(THETA, thetaAng, deltaAng, ANIMAL, RECORDING, SHANKID, CELLID, ISSAVE, VISIBILITY, AUTO, PHASEP)
%   Loads in res (action potential's timepoints) and clu (which cell fires)
%   files concerning to recording (examined file). THETA is a row vector 
%   (1 at dominant theta times, 0 elsewhere). 
%   thetaAng and deltaAng are a row vectors (angles of the filtered eeg datas).
%   ANIMAL animalId
%   RECORDING recordingId
%   SHANKID shankId
%   CELLID cellId
%   ISSAVE: save?
%   AUTO and PHASEP are optional logical inputs, determining whether do
%   autocorrelation and phase preference calculations.
%   VISIBILITY controlls if plots will displayed or not ('off' or 'on').
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK_CALLER, CELL_FEATURES_SHANK, CORRELATION, THETAINDEX, DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global DATADIR
global SR
global NSR
global CGBINS

% Input arguments
p = inputParser;
defaultAnimal = 'viktor8';
defaultRecording = '201006169';
defaultShankId = 4;
defaultCellId = 5;
defaultSave = false;
defaultDisplay = true;
defaultAuto = true;
defaultPhase_p = true;

addOptional(p, 'theta', @isnumeric);
addOptional(p, 'thetaAng', @isnumeric);
addOptional(p, 'deltaAng', @isnumeric);
addOptional(p, 'animal', defaultAnimal, @ischar);
addOptional(p, 'recording', defaultRecording, @ischar);
addOptional(p, 'shankId', defaultShankId, @isnumeric);
addOptional(p, 'cellId', defaultCellId, @isnumeric);
addOptional(p, 'issave', defaultSave, @islogical);
addOptional(p, 'display', defaultDisplay, @isnumeric);
addParameter(p, 'auto', defaultAuto, @islogical);
addParameter(p, 'phaseP', defaultPhase_p, @islogical);
parse(p,varargin{:});

theta = p.Results.theta;
thetaAng = p.Results.thetaAng;
deltaAng = p.Results.deltaAng;
animal = p.Results.animal;
recording = p.Results.recording;
shankId = p.Results.shankId;
cellId = p.Results.cellId;
issave = p.Results.issave;
display = p.Results.display;
auto = p.Results.auto;
phaseP = p.Results.phaseP;

resdir = fullfile(RESULTDIR, 'MSsync');

% Down-sampling rate
dr = SR / NSR;

if size(varargin)==0
    load(fullfile(resdir, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir, 'MSsync', 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir, 'MSsync', 'tsccg', 'delta_angles', recording), 'deltaAng');
end

% Load res and clu files:
res = load(fullfile(DATADIR, animal, [recording '.res.' num2str(shankId)])); %res (timpoints)
clu = load(fullfile(DATADIR, animal, [recording '.clu.' num2str(shankId)])); %clu (which activation time)
clu = (clu(2:end)); %clu(1) is the number of cells in the recording

% The time point when cell_1 fires:
actTime = res(clu==cellId); %activity pattern
actTime = round(actTime/dr);
actTime(actTime == 0) = 1; %change index if equals to 0 after round
actPattern = zeros(size(theta, 2), 1);

% Index in the actPattern (1 where actTime fires, 0 where not)
actPattern(actTime) = 1;

% If not enough APs recorded return
if sum(actTime)<10
    return
end

% Timeserie under theta (1 where cell fires and there is theta, 0 elsewhere)
thetaActPattern = actPattern.*theta';
numApTheta = sum(thetaActPattern); % number of action potentials under theta of cell

% Create delta vector (0 at dominant theta, 1 at dominant delta)
delta = (theta==0);

% Timeserie under delta (1 where cell fires and there is delta, 0 elsewhere)
deltaActPattern = actPattern.*delta';
numApDelta = sum(deltaActPattern); % number of action potentials under delta of cell

% Autocorrelation
if auto
    f1 = figure;
    
    % During theta
    [thetaAcg, thsumacr, alag] = correlation(thetaActPattern, thetaActPattern, display);
    colour1 = 'k*';
    ThAcgThInx = thetaindex(thetaAcg, alag, display, colour1);
    colour2 = 'y*';
    ThAcgDeInx = deltaindex(thetaAcg, alag, display, colour2);
    [ThAcgThEnergy, ThAcgDeEnergy, ThAcgWiEnergy] = spectralanalysis(thetaAcg(length(thetaAcg)/2:end), NSR/CGBINS, display); % NSR/CGBINS is the sampling rate on correlograms
    title(['Acg spectral profile of ' recording ', cell: ' num2str(cellId) '_shn' num2str(shankId) ', under theta']);
    if issave
        savefig(fullfile(resdir, 'cell_features', 'spectrograms', [recording '_theta_acg_spectrogram_of_' num2str(cellId) '_shn' num2str(shankId)]));
    end
    close
    hold on
    
    % During delta
    [deltaAcg, desumacr, alag] = correlation(deltaActPattern, deltaActPattern, display);
    colour3 = 'g*';
    DeAcgThInx = thetaindex(deltaAcg, alag, display, colour3);
    colour4 = 'c*';
    DeAcgDeInx = deltaindex(deltaAcg, alag, display, colour4);
    [DeAcgThEnergy, DeAcgDeEnergy, DeAcgWiEnergy] = ...
        spectralanalysis(deltaAcg(length(deltaAcg)/2:end), NSR/CGBINS, display); % NSR/CGBINS is the sampling rate on correlograms
    title(['Acg spectral profile of ' recording ', cell: ' num2str(cellId) '_shn' num2str(shankId) ', under delta']);
    if issave
        savefig(fullfile(resdir, 'cell_features', 'spectrograms', [recording '_delta_acg_spectrogram_of_' num2str(cellId) '_shn' num2str(shankId)]));
    end
    close
    ylim = get(gca,'ylim');
    xlim = get(gca,'xlim');
    text(xlim(1)+xlim(2)/10, ylim(2)-(ylim(2)-ylim(1))/10, ...
        {'Under theta:'; ['theta energy: ' num2str(ThAcgThEnergy) ...
        ', delta energy: ' num2str(ThAcgDeEnergy) ', wide energy: ' ...
        num2str(ThAcgWiEnergy)]; ['theta index: ' num2str(ThAcgThInx) ...
        ', delta index: ' num2str(ThAcgDeInx)]; ['APs: ' num2str(numApTheta) ...
        ', theta length:' num2str(sum(theta)) ' (ms), sum(acr): ' num2str(thsumacr)]});
    text(xlim(1)+xlim(2)/10, ylim(1)+(ylim(2)-ylim(1))/10, ...
        {'Under delta:'; ['theta energy: ' num2str(DeAcgThEnergy) ...
        ', delta energy: ' num2str(DeAcgDeEnergy) ', wide energy: ' ...
        num2str(DeAcgWiEnergy)]; ['theta index: ' num2str(DeAcgThInx) ...
        ', delta index: ' num2str(DeAcgDeInx)]; ['APs: ' num2str(numApDelta) ...
        ', delta length:' num2str(sum(delta)) ' (ms), sum(acr): ' num2str(desumacr)]});
    title(['autocorrelation cell:' num2str(cellId), ', shank:' num2str(shankId)]);
    xlabel('msec');
    if issave
        savefig(fullfile(resdir, 'cell_features', 'figures', [recording '_autocorr_of_cell_' num2str(cellId) '_shn' num2str(shankId)]));
    end

    hold off
    close
end

% Phase histogramms
if phaseP
    f4 = figure;
    [thetaPhase, thetaFtm, thetaHang, thetaHmvl, thetaZ, thetaPRayleigh, ...
        thetaU, thetaPRao] = phase_pref(thetaActPattern, thetaAng, display);
    hold on
    [deltaPhase, deltaFtm, deltaHang, deltaHmvl, deltaZ, deltaPRayleigh, ...
        deltaU, deltaPRao] = phase_pref(deltaActPattern, deltaAng, display);
    legend('theta', 'delta');
    title(['phase preference cell:' num2str(cellId), ', shank:' num2str(shankId)]);
    if issave
        savefig(fullfile(resdir, 'cell_features', 'phase_histograms', [recording '_phase_pref_of_cell_' num2str(cellId) '_shn' num2str(shankId)]));
    end
    hold off
    close
end

% Save
if issave
    
    % Save data for a cell (autocorrelation, energies, indexes, phase)
    save(fullfile(resdir, 'cell_features', 'correlograms', ...
        [recording '_' num2str(cellId) '_shn' num2str(shankId)]), ...
        'thetaAcg', 'numApTheta', 'thsumacr', 'ThAcgThEnergy', 'ThAcgDeEnergy', ...
        'ThAcgWiEnergy', 'ThAcgThInx', 'ThAcgDeInx', 'deltaAcg', 'numApDelta', ...
        'desumacr', 'DeAcgThEnergy', 'DeAcgDeEnergy', 'DeAcgWiEnergy', ...
        'DeAcgThInx', 'DeAcgDeInx', 'alag', 'thetaPhase', 'thetaFtm', 'thetaHang', ...
        'thetaHmvl', 'thetaZ', 'thetaPRayleigh', 'thetaU', 'thetaPRao', ...
        'deltaPhase', 'deltaFtm', 'deltaHang', 'deltaHmvl', 'deltaZ', ...
        'deltaPRayleigh', 'deltaU', 'deltaPRao');
end

end

%--------------------------------------------------------------------------
function [phase, ftm, hang, hmvl, Z, pRayleigh, U, pRao] = phase_pref(timeseries, ang, visibility)
% PHASE_PREF(TIMESERIES, ANG, VISIBILITY) calculates phase preference during theta rythm
%   TIMESERIES: activity pattern of cell
%   ANG: phases (of hilbert transformed eeg)
%   VISIBILITY: whether to display results

%AP times:
index = timeseries~=0;

% Phase of APs
phase = ang(index);

% Phase histogram
edges = -pi:(2*pi/18):pi;
histogram(phase, edges, 'Normalization', 'probability', 'DisplayStyle', 'stairs');

n = length(phase);
if n < 4
    ftm = NaN;
    hang = NaN;
    hmvl = NaN;
    Z = NaN;
    pRayleigh = NaN;
    U = NaN;
    pRao = [NaN, NaN];
    return
end

% Testing for non-uniform phase distribution
ftm = sum(exp(1).^(1i*phase)) / n;    % first trigonometric moment
hang = angle(ftm);   % mean angle
hmvl = abs(ftm);     % mean resultant length
[Z,pRayleigh,U,pRao] = b_rao(phase(:)');

end
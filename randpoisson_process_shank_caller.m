function randpoisson_process_shank_caller()
%RANDPOISSON_PROCESS_SHANK_CALLER() wrapper for
%RANDPOISSON_PROCESS_SHANK
%
%   See also MSHCsp, RANDPOISSON_PROCESS_SHANK, RANDPOISSON_PROCESS, RANDPOISSON, LOAD_RANDPOISSON, CELL_FEATURES_SHANK_CALLER, CORRELATION, THETAINDEX? DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global NSR
global CGWINDOW
global CGBINS

% Directories
resdir = fullfile(RESULTDIR,'MSsync\randpoisson_process');
if ~exist(resdir,'dir')
    mkdir(resdir);
end

outputVector = [];

% Call 'randpoisson_process_shank'
for it1 = 1:3 %iterate trough all of the animals 3 times
    
    %viktor3
    animal = 3;
    for it = 1:3
        recording = ['20100304' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor4
    animal = 4;
    for it = 1:4
        recording = ['20100317' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor5
    animal = 5;
    for it = 1:4
        recording = ['20100329' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor7
    animal = 7;
    for it = 1:10
        recording = ['20100602' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor8
    animal = 8;
    for it = 1:10
        recording = ['20100616' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor9
    animal = 9;
    for it = 1:10
        recording = ['20100728' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
    
    %viktor10
    animal = 10;
    for it = 1:6
        recording = ['201008050' num2str(it)];
        output = randpoisson_process_shank(animal, recording);
        if ~isnan(output(1))
            outputVector = [outputVector; output];
        end
    end
end

save(fullfile(resdir, 'randpoisson.mat'), 'outputVector');

% Create map for columns interpretation
keySet = {'animalId', 'recordId', 'cellId', 'shankId', 'ThAcgThEnergy', 'ThAcgDeEnergy', 'ThAcgWiEnergy', 'DeAcgThEnergy', 'DeAcgDeEnergy', 'DeAcgWiEnergy', ...
    'ThAcgThInx', 'ThAcgDeInx', 'DeAcgThInx', 'DeAcgDeInx', 'numApTheta', 'numApDelta', ...
    'theta_length', 'delta_length', 'thsumacr', 'desumacr', ...
    'theta_acg', 'delta_acg'};
acgLe = CGWINDOW*NSR*2/CGBINS; %length of acgs
valueSet = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 [21, (acgLe + 20)], [acgLe + 21, (acgLe*2 + 20)]};
randmO = containers.Map(keySet,valueSet); %mapObject

% Save
save(fullfile(resdir, 'RandPomapObj.mat'), 'randmO');

end
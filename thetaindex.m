function thinx = thetaindex(cor,lags, display, colour)
%THETAINDEX(COR, LAGS, DISPLAY, COLOUR) calculate theta index for provided
%COR correlation vector.
%   COR correlation vector.
%   LAGS is the lag vector (time shifts) for COR
%   DISPLAY: whether to display or not
%   COLOUR: colour of asterisk on plot.
%
%   Theta index (thinx):
%         1. find peak in theta band (THBAND) on acg.
%         2. average around (mthp = peak location +- 20 msec) it
%         3. calculate baseline (mtht = peak location/2 and peak location*1.5) and
%         average it.
%         4. (mthp-mtht)/max(mtht, mthp) -> normalize into [-1, 1]
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK_CALLER, CELL_FEATURES_SHANK, CELL_FEATURES, CORRELATION, DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global NSR
global CGWINDOW
global CGBINS
global THBAND

% Theta peak
corres = CGBINS / NSR;   % COR resolution in seconds
thpeak = cor(lags>=(NSR/THBAND(2))&lags<=(NSR/THBAND(1)));   % COR first theta peak
[peakvalue pl] = max(thpeak);
ploc = round((CGWINDOW+1/THBAND(2))/corres + pl);   % peak loc. index to cor
flank = 0.02 / corres;   % 20 ms flanks around the peak, in cor data points
mthp = mean(cor(ploc-flank:ploc+flank)); %mean in a ~50 msec timewindowsize around peak (20msec
peaklag = lags(ploc);   % lag for the theta peak

% Troughs
tloc1 = round(CGWINDOW/corres+(ploc-CGWINDOW/corres)*0.5);   % pre-trough loc index to cor
tloc2 = round(CGWINDOW/corres+(ploc-CGWINDOW/corres)*1.5);   % post-trough loc index to cor
troughlag1 = lags(tloc1);   % lag for theta pre-trough
troughlag2 = lags(tloc2);   % lag for theta post-trough
mtht1 = mean(cor(tloc1-flank:tloc1+flank));
mtht2 = mean(cor(tloc2-flank:tloc2+flank));
mtht = (mtht1+mtht2)/2;   % mean in 50 ms time window around the troughs

% Theta index
% thinx = (mthp - mtht) / max(mthp,mtht);   % theta index
thinx = diff([mtht, mthp]);   % theta index

% Plot
if display
    hold on
    plot(peaklag, mthp, colour);
    plot(lags(tloc1), mtht1, colour);
    plot(lags(tloc2), mtht2, colour);
end
end
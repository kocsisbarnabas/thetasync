function mspreadeeg_rw(varargin)
%MSPREADEEG_RW(VARARGIN) (called by MSPREADEEG_CALLER()) imports hippocampal
%field data of the MSHCsp project and exports resampled (pyramidal-0.4 mm) 
%layer field potential to .mat files.
%   After loads in the data, choose the provided channel (based on
%   CHANNEL_SELECTOR), resamples it, and creates a field potential vector.
%   Parameters:
%   ANIMAL (animal folder path)
%   RECORD (file name)
%   CHOOSEN_CHANNEL: channel number (in the pyramidal layer)
%   S (save?)
%   RESDIR: target directory to save outputs
%
%   See also MSHCsp, MSPREADEEG_CALLER, CHANNEL_SELECTOR, TSCCG.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

% Input arguments
global DATADIR
global RESULTDIR
global SR
global NSR

defaultanimal = 'viktor3\';
defaultRecord = '201003042';
defaultChannel = 64;
defaultSave = false;
defaultResdir = fullfile(RESULTDIR,'mspreadeeg_rw');

p = inputParser;
addOptional(p, 'animal', defaultanimal, @ischar);
addOptional(p, 'record', defaultRecord, @ischar);
addOptional(p, 'channel', defaultChannel, @isnumeric);
addOptional(p, 's', defaultSave, @islogical);
addOptional(p, 'resdir', defaultResdir, @ischar)
parse(p,varargin{:});

animal = p.Results.animal;
recording = p.Results.record;
channel = p.Results.channel;
s = p.Results.s;
resdir = p.Results.resdir;

% Load parameters
fname = fullfile(DATADIR,animal,recording);
pars = LoadPar([fname '.par']); %Load parameter file
pars.nChannels; % 65
pars.nBits;
pars.nElecGps;
pars.ElecGp;

% Create channel list
pars.ElecGp = cellfun(@(x) x',pars.ElecGp,'UniformOutput',false);
chOrder = cell2mat(pars.ElecGp);   % channel order
chOrder = chOrder + 1;   % because of matrix indices can't be 0

fileinfo = dir([fname '.dat']);
nRecordings = fileinfo.bytes/2/pars.nChannels; %gives the length of the recording for one channel

datafile = fopen([fname '.dat'],'r');
fieldPot = zeros(1, nRecordings/(SR/NSR)); %Allocate resampled field potential

%we will read data by section and than concatenate them
%(reading immediately the whole data would crash the memory)
bufferseconds = 10;  %read 10 second (10*sampling rate)

%create resampled data matrix:
for x = 1:(nRecordings/(bufferseconds*SR))
    data = fread(datafile,[pars.nChannels,SR*bufferseconds],'int16');
    fieldPot(((x-1)*NSR*bufferseconds+1):x*NSR*bufferseconds) = data(chOrder(channel), 1:(SR/NSR):end);
end

fclose(datafile);

if s
    save(fullfile(resdir,[recording '_radiatum.mat']),'fieldPot');
end

end
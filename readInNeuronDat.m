function readInNeuronDat()
%READINNEURONDAT() handles Neuron software generated .dat files.
%   Reads in neuonral activity and time, than plot them.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 19/10/2017

clear all

voltage = dlmread('C:\Users\kocsis.barnabas\Google Drive\KOKI\models\pacemaker\F�T\GolombModell\volt_6Hz.dat','r');
time = dlmread('C:\Users\kocsis.barnabas\Google Drive\KOKI\models\pacemaker\F�T\GolombModell\soma_time_6Hz.dat','r');

timev = linspace(0,3,length(voltage));
figure;plot(timev, voltage)
xlabel('time (s)')
ylabel('voltage (mV)')
ylim([-100, 50])
set(gca, 'xtick',[0, 1, 2, 3])
setmyplot_balazs
end
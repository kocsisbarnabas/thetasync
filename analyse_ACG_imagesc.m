function analyse_ACG_imagesc()
%ANALYSE_ACG_IMAGESC() help to analyse thresholds for indices (theta and delta).
%   analyse_ACG_imagesc() opens all cell's ACG's image (during theta or
%   delta-> HIPSTATE). Each row (ACG) can be obtained on the 2D plane, by
%   clicking on 'ChooseAcg' button, and then click on the desired row. Use
%   left button on the left subfigure (sorted based on theta index) to
%   click, and right button on the right subfigure (sorted based on delta
%   index) to click!
%
%   See also MSHCsp, COMPARETECHNIQUES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 01/06/2017

global RESULTDIR;
global SR;
global NSR;
global CGWINDOW;
global CGBINS;

hipState = 'theta';

%load ACG image:
openfig(fullfile(RESULTDIR, 'compareTechniques', [hipState '.fig']));

%load map for allCell matrix (mO):
l1 = load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));
    
btn = uicontrol('Style', 'pushbutton', 'String', 'ChooseAcg',...
        'Position', [1 1 100 20],...
        'Callback', @chooseAcg_callback);

    function chooseAcg_callback(src,event)
        %make a click:
        [~, clicky, button] = ginput(1);
        clicky = round(clicky);
        if button == 1 % clicked with left button (hopefully on the l�ft side)-> thetainx sorted cell will be l1
            l2 = load(fullfile(RESULTDIR, 'compareTechniques', [hipState '_ThsortedAllCell.mat']));
            animal = l2.sortedCell1(clicky, l1.mO('animalId'));
            recording = l2.sortedCell1(clicky, l1.mO('recordId'));
            SHANKNO = l2.sortedCell1(clicky, l1.mO('shankId'));
            EXAMINED_CELL = l2.sortedCell1(clicky, l1.mO('cellId'));
            openfig(fullfile(RESULTDIR, 'cell_features', 'figures', [num2str(animal) num2str(recording) '_autocorr_of_cell_' num2str(EXAMINED_CELL) '_shn' num2str(SHANKNO) '.fig']));
            %adjust y axis:
            %ylim([0, 0.01]);
            %transition point to te recording:
            tP = troughs(recording)*NSR;
            %make raster plot:
            raster_plottozas_II(num2str(animal), num2str(recording), num2str(SHANKNO), EXAMINED_CELL, tP, 50*NSR)
        elseif button == 3 % clicked with right button (hopefully on the right side)-> deltinx sorted cell will be l1
            l2 = load(fullfile(RESULTDIR, 'compareTechniques', [hipState '_DesortedAllCell.mat']));
            animal = l2.sortedCell2(clicky, l1.mO('animalId'));
            recording = l2.sortedCell2(clicky, l1.mO('recordId'));
            SHANKNO = l2.sortedCell2(clicky, l1.mO('shankId'));
            EXAMINED_CELL = l2.sortedCell2(clicky, l1.mO('cellId'));
            openfig(fullfile(RESULTDIR, 'cell_features', 'figures', [num2str(animal) num2str(recording) '_autocorr_of_cell_' num2str(EXAMINED_CELL) '_shn' num2str(SHANKNO) '.fig']));
            %transition point to te recording:
            tP = troughs(recording)*NSR;
            %make raster plot:
            raster_plottozas_II(num2str(animal), num2str(recording), num2str(SHANKNO), EXAMINED_CELL, tP, 50*NSR)
        end        
    end
end
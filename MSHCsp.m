% function MSHCSp
%MSHCSp   Main for thetasync project.
% 
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

% Add code folder to path
codepath = 'C:\Users\kocsis.barnabas\Desktop\thetasync';
codepath = 'd:\Dropbox\_code\BitBucket\Thetasync\';
addpath(genpath(codepath));

% Directories
global ROOTDIR
% ROOTDIR = 'C:\Users\kocsis.barnabas\Desktop\MSSync\';
ROOTDIR = 'E:\MSHCsp';
% addpath(genpath(ROOTDIR));

global WORKFOLDER
WORKFOLDER = 'october17\';
% global RESULTDIR
% RESULTDIR = fullfile(DATAPATH,'MSsync','proba1');   % result files
% RESULTDIR = fullfile(ROOTDIR,WORKFOLDER);
% if ~exist(RESULTDIR,'dir')
%     status = mkdir(RESULTDIR);   % create folder for this run
% end

global DATADIR
% DATADIR = 'C:\Users\kocsis.barnabas\Desktop\MSSync\';
DATADIR = 'F:\MSHCsp\';

% Global parameters
global SR   % sampling rate
SR = 20000;
global NSR  % new sampling rate
NSR = 1000;
global CGWINDOW   % CGWINDOW for acg
CGWINDOW = 3;
global CGBINS  % binsize for acg
CGBINS = 20;
global THBAND   % theta frequency band
THBAND = [3, 8];
global DEBAND   % delta frequency band
DEBAND = [0.5, 3];

% % Create EEG figures of electrode channels for appropriate channel
% % selection (of pyramidal layer or radiatum)
% % run CHANNEL_SELECTOR() function for all folders (animal):
% s1 = true;   % logical varriable: save figures and data?
% channel_selector_caller(s1)

% % Downsample EEG data of the defined electrode channels, than save them
% % run MSPREADEEG_RW() function for all folders:
% s2 = true;
% mspreadeeg_caller(s2)

% % Determine delta and theta segments of EEG, compute angles, create figure:
% % run TSCCG() function for all records:
% s3 = true;
% tsccg_caller(s3);

% % Create acgs, phase histograms of septal cells, and analyze them:
% % run CELL_FEATURES_SHANK() function which calls CELL_FEATURES() for all
% % cells
% s4 = true;
% cell_features_shank_caller(s4);

% % Create randpoisson process:
% randpoisson_process_shank_caller()

% % Calculate threshold of indices 
% s5 = true;
% load_randpoisson('theta', s5);   % during theta
% load_randpoisson('delta', s5);   % during delta

% % Load cell acgs and parameters in a common matrix:
% s51 = true;
% data_loader(s51)

% Not prepared yet: randpermutation(NSR) (another technique for threshold computation)

% % Create image of all autocorrelogramms with computed threshold:
% s6 = true;
% cmprRhtmIndices('theta', s6)
% cmprRhtmIndices('delta', s6)

% Create folders for groups:
s7 = true;
cell_groups('CTB', s7);   % constitutive theta bursting
cell_groups('CTT', s7);   % constitutive theta tonic
cell_groups('CDS', s7);   % constitutive delta slow
cell_groups('CDF', s7);   % constitutive delta fast
% cell_groups('TD_', s7));
cell_groups('DT_', s7);   % constitutive delta-theta
% cell_groups('NT_', s7);
% cell_groups('ND_', s7);
% cell_groups('TN_', s7);
% cell_groups('DN_', s7);
% cell_groups('NN_', s7);
% cell_groups('ABS', s7);

% % calculate INTRAgroup ccgs:
% s8 = true;
% maxlag = 3*NSR; %3000 msec
% xBinS = 20; %1 msec
% ccg_intragroups('CTB', maxlag, xBinS, s8);
% ccg_intragroups('CTT', maxlag, xBinS, s8);
% ccg_intragroups('CDS', maxlag, xBinS, s8);
% ccg_intragroups('CDF', maxlag, xBinS, s8);
% ccg_intragroups('DT_', maxlag, xBinS, s8);

% % calculate INTRAgroup (small) ccgs:
% s9 = true;
% maxlag = 50; % 50 msec
% xBinS = 1; %1 msec
% ccg_intragroups('CTB', maxlag, xBinS, s9);
% ccg_intragroups('CTT', maxlag, xBinS, s9);
% ccg_intragroups('CDS', maxlag, xBinS, s9);
% ccg_intragroups('CDF', maxlag, xBinS, s9);
% ccg_intragroups('DT_', maxlag, xBinS, s9);

% % calculate INTERgroup ccgs:
% s10 = true;
% maxlag = 3*NSR; % 3 sec
% xBins = 20; %20 msec
% ccg_intergroups(maxlag, xBins, s10);

function [th_energy, de_energy, wide_energy] = spectralanalysis(cg, Fs, display)
%SPECTRALANALYSIS(CG, FS, DISPLAY) calculates spectral profile of CG
%   CG correlogram
%   FS sampling frequency
%   DISPLAY: whether to display results or not
%
%   See also MSHCsp, CELL_FEATURES, CORRELATION, THETAINDEX, DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 09/10/2017

global THBAND
global DEBAND

% Fourier-transform
xdft = (1/length(cg))*fft(cg);
%freq = -(Fs/2):(Fs/length(xdft)):(Fs/2)-Fs/length(xdft);
freq = linspace(-(Fs/2), (Fs/2), length(xdft)-1);
xdft(1) = [];
sxdft = abs(fftshift(xdft)); %shift zero frequency to the center

% Energy in theta band
th_energy = max(sxdft(freq>THBAND(1) & freq<THBAND(2)))-mean(sxdft(freq>THBAND(1) & freq<THBAND(2)));
% Energy in delta band
de_energy = max(sxdft(freq>DEBAND(1) & freq<DEBAND(2)))-mean(sxdft(freq>DEBAND(1) & freq<DEBAND(2)));
% Energy in wide band
wide_energy = mean([sxdft(freq>=0 & freq<DEBAND(1)); sxdft(freq>THBAND(2))]); %energy from delta band to beta band

% Plot
if display
    figure; 
    plot(freq,sxdft);
    ylim = get(gca,'ylim');
    xlim = get(gca,'xlim');
    text(xlim(1)+xlim(2)/10, ylim(2)-ylim(2)/10, {['theta/wide energy: ' num2str(th_energy/wide_energy)]; ['delta/wide energy: ' num2str(de_energy/wide_energy)]; ['wide energy: ' num2str(wide_energy)]});
end
end
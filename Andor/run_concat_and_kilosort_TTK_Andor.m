%TTK konkaten�lni �s lefuttatni a Kilosortot

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TTK concat

try
    
    rootDir = 'E:\TTKmouse\b';
    
    addpath(genpath(rootDir));
    animals = dir(rootDir);
    animals = animals([animals.isdir]);
    names1 = {animals.name};
    for it1 = 3:length(animals)
        if isempty(findstr(names1{it1}, 'not_concat'))
            records = dir(fullfile(rootDir, names1{it1}));
            records = records([records.isdir]);
            names2 = {records.name};
            for it2 = 3:length(records)
                cd(fullfile(rootDir, names1{it1}, names2{it2}));
                if length(names2{it2})>1
                    files = dir(fullfile(rootDir, names1{it1}, names2{it2}));
                    names3 = {files.name};
                    filelist = {};
                    for it3 = 1:length(names3)
                        if ~isempty(findstr(names3{it3}, '128ch.dat')) && ~files(it3).isdir
                            filelist{end+1} = names3{it3};
                        end
                    end
                    if length(filelist)>1
                        destination = fullfile(rootDir, names1{it1}, names2{it2}, [names1{it1}, names2{it2}, '128ch.dat']);
                        [success, borders] = andor_combiFile(filelist, destination);
                        for it3 = 1:length(filelist)
                            delete(filelist{it3});
                        end
                    end
                    
                    filelist = {};
                    for it3 = 1:length(names3)
                        if ~isempty(findstr(names3{it3}, '32ch.dat')) && ~files(it3).isdir
                            filelist{end+1} = names3{it3};
                        end
                    end
                    if length(filelist)>1
                        destination = fullfile(rootDir, names1{it1}, names2{it2}, [names1{it1}, names2{it2}, '32ch.dat']);
                        [success, borders] = andor_combiFile(filelist, destination);
                        for it3 = 1:length(filelist)
                            delete(filelist{it3});
                        end
                    end
                end
            end
        end
    end
catch ME
    
end


clear all;
close all;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TTK Kilosort:
try
    
    TTKtotCh = 128;
    TTKnCh = 128;
    TTKnClu = 128;
    TTKrootDir = 'E:\TTKmouse\b';
    TTK_folders1 = dir(TTKrootDir);
    TTK_folders1 = TTK_folders1([TTK_folders1.isdir]);
    for it1 = 5:length(TTK_folders1)
        TTKanimal = TTK_folders1(it1).name;
        TTK_folders2 = dir(fullfile(TTKrootDir, TTKanimal));
        TTK_folders2 = TTK_folders2([TTK_folders2.isdir]);
        for it2 = 3:3 %length(TTK_folders2)
            TTKrecord = TTK_folders2(it2).name;
            TTKfbinary = [TTKanimal, TTKrecord, '_128ch.dat'];
            run('C:\Users\kocsis.barnabas\Documents\MATLAB\Kilosort\master_file_example_MOVEME.m')
            
            outputFiles = dir(fullfile(TTKrootDir, TTKanimal));
            names = {outputFiles.name};
            dates = {outputFiles.date};
            isdirs = {outputFiles.isdir};
            for it3 = 1:length(outputFiles)
                if ~isempty(findstr(dates{it3}, 'Sep-2017')) & ~isdirs{it3} & isempty(findstr(names{it3}, 'chanMap.mat'))
                    source = fullfile(TTKrootDir, TTKanimal, outputFiles(it3).name);
                    destination = fullfile(TTKrootDir, TTKanimal, TTKrecord, outputFiles(it3).name);
                    movefile(source,destination)
                end
            end
            
            close all;
        end
    end
catch ME
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Andor linear track and concatenated Kilosort:
% try
%     
%     TTKtotCh = 67;
%     TTKnCh = 32;
%     TTKnClu = 64;
%     TTKrootDir = 'E:\Andor';
%     TTKanimal = '1988';
%     TTK_folders1 = dir(fullfile(TTKrootDir, TTKanimal));
%     TTK_folders1 = TTK_folders1([TTK_folders1.isdir]);
%     for it1 = 3:length(TTK_folders1)
%         TTKrecord = TTK_folders1(it1).name;
%         TTKfbinary = [TTKanimal, TTKrecord, '.dat'];
%         run('C:\Users\kocsis.barnabas\Documents\MATLAB\Kilosort\master_file_example_MOVEME.m');
%         
%         outputFiles = dir(fullfile(TTKrootDir, TTKanimal));
%         names = {outputFiles.name};
%         dates = {outputFiles.date};
%         isdirs = {outputFiles.isdir};
%         for it3 = 1:length(outputFiles)
%             if ~isempty(findstr(dates{it3}, 'Sep-2017')) & ~isdirs{it3} & isempty(findstr(names{it3}, 'chanMap.mat'))
%                 source = fullfile(TTKrootDir, TTKanimal, outputFiles(it3).name);
%                 destination = fullfile(TTKrootDir, TTKanimal, TTKrecord, outputFiles(it3).name);
%                 movefile(source,destination)
%             end
%         end
%         
%         close all;
%     end
% catch ME
%     
% end
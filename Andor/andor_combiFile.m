function [success, borders] = andor_combiFile(filelist, destination)

success = false;
fileFormat = 'int16';

if ~iscell(filelist)
    filelist = cellstr(filelist);
end

borders = zeros(size(filelist));

dataSize = zeros(size(filelist));
for k = 1:numel(filelist)
    if isequal(exist(filelist{k}),2)
        temp = dir(filelist{k});
        dataSize(k) = temp.bytes;
        if strcmp(fileFormat,'int16')
            borders(k) = dataSize(k)/2;
        end
    else
        disp(['File ' filelist{k} ' does not found. Cancelled.'])
        return
    end
end
requiredSize = sum(dataSize(:));
[destFolder,~,~] = fileparts(destination);
freeDestSpace = java.io.File(destFolder).getFreeSpace();
if requiredSize > freeDestSpace
    disp(['Available space in destination folder ' num2str(freeDestSpace) ' bytes. Required ' num2str(requiredSize) ' bytes. Cancelled.'])
    return
end


status = ones(size(filelist));
if isunix
    for k = 1:numel(filelist)
    unixcommand = ['cat ' filelist{k} ' >> ' destination];
    status(k) = unix(unixcommand);
    end
elseif ispc
    for k = 1:numel(filelist)
        if k == 1
            doscommand = ['type ' filelist{k} ' > ' destination];
            status(k) = dos(doscommand);
        else
            doscommand = ['type ' filelist{k} ' >> ' destination];
            status(k) = dos(doscommand);
        end
    end
end
success = all(~status);
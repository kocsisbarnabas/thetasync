function ChNum = andor_metafile(filename, char, varargin)
% 3rd input: output will be text, otherwise numeric

fileID = fopen(filename,'r');

data = textscan(fileID, '%[^\n]', 'Delimiter', ' ', 'MultipleDelimsAsOne', true, 'ReturnOnError', false);
data = data{1};

c = cellfun(@(x) regexpi(x, char), data, 'UniformOutput', false);

d = find(~cellfun(@isempty, c, 'UniformOutput', true));

if nargin == 3
    ChNum = textscan(data{d}, '%*s%s', 'Delimiter', '=');
else
    ChNum = (cell2mat(textscan(data{d}, '%*s%n', 'Delimiter', '='))); 
end

fclose(fileID);


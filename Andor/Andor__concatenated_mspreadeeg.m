function Andor__concatenated_mspreadeeg()
rootDir = 'E:\Andor\concatenated';
animalId = '1988';
recordingId = '38_43';
sr = 20000;
nsr = 1000;
chNo = 67; %number of channels
channelId = 10;


chFile = fullfile(rootDir, animalId, [animalId, 'linchOrder.mat']);
load(chFile); %load channel map

fileinfo = dir(fullfile(rootDir, animalId, recordingId, [animalId, recordingId, '.dat']));
nRecordings = fileinfo.bytes/2/chNo; %gives the length of the recording for one channel

datafile = fopen(fullfile(rootDir, animalId, recordingId, [animalId, recordingId, '.dat']),'r');
fieldPot = zeros(1, nRecordings/(sr/nsr)); %Allocate resampled EEG

bufferseconds = 100;  %read 100 second (100*sampling rate)

%create resampled data matrix:
for x = 1:(nRecordings/(bufferseconds*sr))
    data = fread(datafile,[chNo,sr*bufferseconds],'int16');
    fieldPot(((x-1)*nsr*bufferseconds+1):x*nsr*bufferseconds) = data(linChOrder(channelId), 1:(sr/nsr):end);
end



% %wavelet:
% [pow,phase,f] = eegwavelet(field,nsr);
% 
% timev = linspace(1, size(pow, 2)/nsr, size(pow, 2));
% figure, imagesc(timev,1:size(pow, 1),pow);
% b_rescaleaxis('Y',f(1:size(pow, 1)));
% set(gca,'clim',[0, 100]);
% colormap('jet');
% xlabel('s')

% %filter:
% theta_flt = fir1(1024,[3 8]/(nsr/2),'bandpass');
% theta_feeg = filtfilt(theta_flt,1,data);
% theta_sfeeg = (theta_feeg - mean(theta_feeg)) ./ std(data); % standardize feeg

%plot
% figure;
% plot(data(:, channelId));
% save('E:\Andor\11november\198838_43.mat','fieldPot');
end
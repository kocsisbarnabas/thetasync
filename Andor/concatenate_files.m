function concatenate_files()

addpath(genpath('C:\Users\kocsis.barnabas\Desktop\thetasync'));

rootDir = 'E:\Andor\concatenated';
animalId = '1988';
recordingGroup = '20_33';

recordings = dir(fullfile(rootDir, animalId, recordingGroup));
recordings = recordings(3:end);
filelist = {recordings.name};

cd(fullfile(rootDir, animalId, recordingGroup));

destination = fullfile(rootDir, animalId, recordingGroup, [animalId, recordingGroup, '.dat']);
[success, borders] = andor_combiFile(filelist, destination);

end
% Freely Moving Mouse Project (FMMproject), global varriable declarations

% Add code folder to path
codepath = 'C:\Users\kocsis.barnabas\Desktop\thetasync';
addpath(genpath(codepath));

% Directories
global ROOTDIR
% ROOTDIR = 'C:\Users\kocsis.barnabas\Desktop\MSSync\';
ROOTDIR = 'E:\Andor';
addpath(genpath(ROOTDIR));

global WORKFOLDER
WORKFOLDER = 'november11\';
global RESULTDIR
RESULTDIR = fullfile(ROOTDIR,WORKFOLDER);

if ~exist(RESULTDIR,'dir')
    status = mkdir(RESULTDIR);   % create folder for this run
end

global DATAPATH
% DATAPATH = 'C:\Users\kocsis.barnabas\Desktop\MSSync\';
DATAPATH = 'E:\Andor\concatenated';


% Global parameters
global SR;   % sampling rate
SR = 20000;

global NSR;  % new sampling rate
NSR = 1000;

global CGWINDOW;   % CGWINDOW for acg
CGWINDOW = 3;

global CGBINS;  % binsize for acg
CGBINS = 20;

global THBAND;   % theta frequency band
THBAND = [4, 12];

global DEBAND;   % delta frequency band
DEBAND = [1, 4];
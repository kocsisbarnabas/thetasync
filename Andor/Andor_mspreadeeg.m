function Andor_mspreadeeg()
% metaFile = 'E:\Barni\freely_moving\1988\1\1988_20.meta';
datFile = 'E:\Barni\freely_moving\1988\1\1988.dat';
sr = 20000;
channelId = 10;

% chNo = andor_metafile(metaFile, 'recorded channel'); % number of channels
chNo = 67;
data = memmapfile(datFile,'Format', 'int16');%load data file
data = reshape(data.Data, chNo, []).';

% chFile = 'E:\Barni\freely_moving\1988\1\chOrder.mat';
chFile = 'C:\Users\kocsis.barnabas\Documents\1988\1\chOrder.mat';
load(chFile); %load channel map

data = data(:, linChorder); %order channels

%resample data:
nsr = 1000;
data = double(data(1:sr/nsr:end,:));

%wavelet:
[pow,phase,f] = eegwavelet(data(:, channelId),nsr);

timev = linspace(1, size(pow, 2)/nsr, size(pow, 2));
figure, imagesc(timev,1:size(pow, 1),pow);
% b_rescaleaxis('Y',f(1:size(pow, 1)));
b_rescaleaxis('Y',f)
setappdata(gca,'scaley',f)
b_zoomset_for_wavelet

set(gca,'clim',[0, 100]);
colormap('jet');
xlabel('s')

% %filter:
% theta_flt = fir1(1024,[3 8]/(nsr/2),'bandpass');
% theta_feeg = filtfilt(theta_flt,1,data);
% theta_sfeeg = (theta_feeg - mean(theta_feeg)) ./ std(data); % standardize feeg

%plot
% figure;
% plot(data(:, channelId));

GUI_bandFilter(data(:, channelId), nsr);

pause;
end
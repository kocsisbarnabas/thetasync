function Andor_channel_selector()

rootDir = 'E:\Andor\concatenated';
animalId = '1988';
recordingId = '38_43';
sr = 20000;
startTime = 0*sr+1;
endTime = startTime + 60*sr-1;
%metFile = fullfile(rootDir, animalId, 'ampliplex', recordingId)
chNo = 67; %andor_metafile(metaFile, 'recorded channel'); % number of channels

datFile = fullfile(rootDir, animalId, 'ampliplex', recordingId, [animalId, '_', recordingId, '.dat']);



data = memmapfile(datFile,'Format', 'int16');%load data file
data = reshape(data.Data, chNo, []).';
% data = reshape(data.Data, [], chNo).';

chFile = 'E:\Barni\freely_moving\1988\1\chOrder.mat';
load(chFile); %load channel map

data = data(startTime:endTime, [linChorder BuzsChorder 65 66 67]); %order channels
% data = data(startTime:endTime, linChorder); %order channels

data = double(data);

%plot
Shift = repmat(((1:size(data, 2))*1800), endTime-startTime+1, 1);
timeVector = linspace(startTime, endTime, size(data, 1));
figure;
plot(data(startTime:endTime, :)+Shift)
% pause;
end
function cut_dat_file()
sourceFile = 'E:\Barni\freely_moving\1988_20\1\1988_20.dat';
destFile = 'E:\Barni\freely_moving\1988_20\1\1988_20_60sec.dat';
datafile = fopen(sourceFile,'r');

sr = 20000;
secsToRead = 60;
nChanTot = 67;

% parcel = zeros(nChanTot, sr*secsToRead);

for it = 1:secsToRead
    buff = fread(datafile, [nChanTot sr], '*int16');
    parcel(:, ((it-1)*sr+1):it*sr) = buff;
end

fclose(datafile);

fid = fopen(destFile, 'w');
fwrite(fid, parcel, '*int16');
fclose(fid);

end
% rootDir = 'E:\Barni\freely_moving\1988\1';
% cd 'C:\Users\kocsis.barnabas\Documents\1988\3';
% rootDir = 'C:\Users\kocsis.barnabas\Documents\1988\3';
% cd 'E:\Barni\freely_moving\1988\4';
% rootDir = 'E:\Barni\freely_moving\1988\4';

cd 'D:\TTKmouse\b\20170607\23_128';
rootDir = 'D:\TTKmouse\b\20170607\23_128';

addpath(genpath(rootDir));
files = dir(rootDir);
names = {files.name};
isdirs = {files.isdir};
filelist = {};
for it = 1:length(names)
    if ~isdirs{it} & ~isempty(findstr(names{it}, '.dat'))
        filelist{end+1} = names{it};
    end
end
destination = [rootDir, '\23_128.dat'];
[success, borders] = andor_combiFile(filelist, destination);


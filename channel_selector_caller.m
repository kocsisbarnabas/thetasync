function channel_selector_caller(issave)
%CHANNEL_SELECTOR_CALLER   Wrapper function for CHANNEL_SELECTOR
%   CHANNEL_SELECTOR_CALLER(ISSAVE) calls CHANNEL_SELECTOR for each animal once,
%   to find pyramidal layer, based on displaying theta phase change and
%   ripple change. (Recordings provided are 'nice' recordings -> theta and
%   delta segments are seperated.) Saves figures and data if S == 1.
%
%   See also MSHCSP, LOADPAR, CHANNEL_SELECTOR_CALLER, MSPREADEEG_RW
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA 
%   Date: 18/04/2017

global RESULTDIR

% Create folder for figures and data
resdir = fullfile(RESULTDIR,'MSsync','channel_selector');
if ~exist(resdir,'dir')
    mkdir(resdir);
end

animal = 'viktor3\';
record = '201003042';
thetaTime = [690, 695]; % dominant theta segment in seconds (for theta phase shift)
deltaTime = [660, 670]; % dominant delta segment in seconds (for ripple phase shift)
dispChannels = [9, 25]; % channels to display
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor4\';
record = '201003171';
thetaTime = [1825, 1830];
deltaTime = [910, 920];
dispChannels = [13, 29];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor5\';
record = '201003292';
thetaTime = [545, 550];
deltaTime = [290, 300];
dispChannels = [14, 30];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor7\';
record = '201006022';
thetaTime = [735, 740];
deltaTime = [55, 65];
dispChannels = [1, 32];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor8\';
record = '201006163';
thetaTime = [1000, 1005];
deltaTime = [120, 130];
dispChannels = [16, 32];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor9\';
record = '201007282';
thetaTime = [805, 815];
deltaTime = [330, 340];
dispChannels = [16, 32];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

animal = 'viktor10\';
record = '2010080502';
thetaTime = [610, 615];
deltaTime = [460, 470];
dispChannels = [1, 32];
channel_selector(animal, record, thetaTime, deltaTime, dispChannels, issave, resdir);

end
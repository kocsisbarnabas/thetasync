function cell_features_shank_caller(issave)
%CELL_FEATURES_SHANK_CALLER(ISSAVE) wrapper functions for CELL_FEATURES_SHANK
%  ISSAVE: save?
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK, CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR

% Directories
resdir0 = fullfile(RESULTDIR,'MSsync');
resdir = fullfile(RESULTDIR,'MSsync','cell_features');
if ~exist(resdir,'dir')
    mkdir(resdir);
    
    % Create subfolders:
    mkdir(fullfile(resdir, 'correlograms')); %for cells autocorrelograms (vectors)
    mkdir(fullfile(resdir, 'figures')); %for autocorrelograms figures
    mkdir(fullfile(resdir, 'phase_histograms')); %for cells phase histograms
    mkdir(fullfile(resdir, 'spectrograms')); %for autocorrelograms spectrograms
end

%viktor3
animal = 'viktor3';
for it = 1:3
    recording = ['20100304' num2str(it)];
    %load theta timevector (example: 0100111...)
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    %load phase vectors:
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    %iterate trough each shank:
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor4
animal = 'viktor4';
for it = 1:4
    recording = ['20100317' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor5
animal = 'viktor5';
for it = 1:4
    recording = ['20100329' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor7
animal = 'viktor7';
for it = 1:10
    recording = ['20100602' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor8
animal = 'viktor8';
for it = 1:10
    recording =['20100616' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor9
animal = 'viktor9';
for it = 1:10
    recording = ['20100728' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

%viktor10
animal = 'viktor10';
for it = 1:6
    recording = ['201008050' num2str(it)];
    load(fullfile(resdir0, 'tsccg', 'theta_segments', recording), 'theta');
    load(fullfile(resdir0, 'tsccg', 'theta_angles', recording), 'thetaAng');
    load(fullfile(resdir0, 'tsccg', 'delta_angles', recording), 'deltaAng');
    
    cell_features_shank(theta, thetaAng, deltaAng, animal, recording, issave, resdir);
end

end
function deinx = deltaindex(cor,lags, display, colour)
%DELTAINDEX(COR, LAGS, DISPLAY, COLOUR) calculate delta index for provided
%COR correlation vector.
%   COR correlation vector.
%   LAGS is the lag vector (time shifts) for COR
%   DISPLAY: whether to display or not
%   COLOUR: colour of asterisk on plot.
%
%   Delta index (thinx):
%         1. find peak in delta band (DEBAND) on acg.
%         2. average around (mdep = peak location +- 20 msec) it
%         3. calculate baseline (mdet = peak location/2 and peak location*1.5) and
%         average it.
%         4. (mdep-mdet)/max(mdep, mdet) -> normalize into [-1, 1]
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK_CALLER, CELL_FEATURES_SHANK, CELL_FEATURES, CORRELATION,, THETAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global NSR
global CGWINDOW
global CGBINS
global DEBAND

% Delta peak
corres = CGBINS / NSR;   % cor resolution in seconds
depeak = cor(lags>=(NSR/DEBAND(2))&lags<=(NSR/DEBAND(1)));   % cor first delta peak
[peakvalue pl] = max(depeak);
ploc = round((CGWINDOW+1/DEBAND(2))/corres + pl);   % peak loc. index to cor
flank = 0.02 / corres;   % 20 ms flanks around the peak, in cor data points
mdep = mean(cor(ploc-flank:ploc+flank)); %mean in a ~50 msec timewindowsize around peak (20msec
peaklag = lags(ploc);   % lag for the delta peak

% Troughs
tloc1 = round(CGWINDOW/corres+(ploc-CGWINDOW/corres)*0.5);   % pre-trough loc index to cor
tloc2 = round(CGWINDOW/corres+(ploc-CGWINDOW/corres)*1.5);   % post-trough loc index to cor

mdet1 = mean(cor(tloc1-flank:tloc1+flank));
if tloc2+flank>length(cor) % if we cant average, because baseline "hang out" (cor is too short)
    mdet2 = mean(cor(length(cor)-flank*2:length(cor)));
else
    mdet2 = mean(cor(tloc2-flank:tloc2+flank));
end
mdet = (mdet1 + mdet2)/2;   % mean in 50 ms time CGWINDOW around the troughs

% Delta index
% deinx = (mdep - mdet) / max(mdep,mdet);   % delta index
deinx = diff([mdet, mdep]);   % delta index

% Plot
if display
    hold on
    plot(peaklag, mdep, colour);
    plot(lags(tloc1), mdet1, colour);
    plot(lags(tloc2), mdet2, colour);
end
end
% TTK project:
% 
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 24/07/2017

% % Add code folder to path
% codepath = 'C:\Users\kocsis.barnabas\Desktop\thetasync';
% <<<<<<< HEAD
% % codepath = 'd:\Dropbox\_code\BitBucket\Thetasync\';
% addpath(genpath(codepath));

% % Directories
global rootDir
rootDir = 'C:\Users\kocsis.barnabas\Desktop\TTKmouse\';
% addpath(genpath(rootDir));

global workfolder
% workfolder = 'div_with_std\';
workfolder = 'july\';
resdir = fullfile(rootDir,workfolder);

% Directories
% global rootDir
% rootDir = 'C:\Users\kocsis.barnabas\Desktop\MSHCsp\';
% global DATAPATH
% global RESULTDIR
% RESULTDIR = fullfile(DATAPATH,'MSsync','proba1');   % result files
% if ~exist(resdir,'dir')
%     status = mkdir(resdir);   % create folder for this run
% end
% global DATADIR
% DATADIR = 'F:\MSHCsp\';   % data files

% Global parameters
global sr   % sampling rate
sr = 20000;

global nsr   % new sampling rate
nsr = 1000;

global windowsize   % windowsize for acg
windowsize = 3;

global bins   % binsize for acg
bins = 20;

global THBAND   % theta frequency band
THBAND = [2.5, 8];

global DEBAND   % delta frequency band
DEBAND = [0.5, 2.5];

% Create EEG figures of electrode channels for appropriate channel
% selection (of pyramidal layer or radiatum)
% run CHANNEL_SELECTOR() function for all folders (animal):
s1 = true;   % logical varriable: save figures and data?
channel_selector_caller(s1)
% 
% % Downsample EEG data of the defined electrode channels, than save them
% % run MSPREADEEG_RW() function for all folders:
% s2 = 1;
% mspreadeeg_caller(s2)
% 
% % Determine delta and theta segments of EEG, compute angles, create figure:
% % run TSCCG() function for all records:
% s3 = 1;
% tsccg_caller(s3);

% Create acgs, phase histograms of septal cells, and analys them:
% run CELL_FEATURES_SHANK() function which calls CELL_FEATURES() for all
% cells
s4 = 1;
TTKcell_features_caller(s4);

% Create randpoisson process:
randpoisson_process_shank_caller()

% Calculate threshold of indices 
s5 = 1;
load_randpoisson('theta', s5);   % during theta
load_randpoisson('delta', s5);   % during delta

% Load cell acgs and parameters in a common matrix:
data_loader()

% Not prepared yet: randpermutation(nsr) (another technique for threshold computation)

% Create image of all autocorrelogramms with computed threshold:
s9 = 1;
compareTechniques('theta', s9)
compareTechniques('delta', s9)

% Create folders for groups:
s6 = 1;
cell_groups('CTB', s6);   % constitutive theta bursting
cell_groups('CTT', s6);   % constitutive theta tonic
cell_groups('CDS', s6);   % constitutive delta slow
cell_groups('CDF', s6);   % constitutive delta fast
% cell_groups('TD_', s6));
cell_groups('DT_', s6);   % constitutive delta-theta
% cell_groups('NT_', s6);
% cell_groups('ND_', s6);
% cell_groups('TN_', s6);
% cell_groups('DN_', s6);
% cell_groups('NN_', s6);
% cell_groups('ABS', s6);

% calculate INTRAgroup ccgs:
s7 = 1;
maxlag = 3*nsr; %3000 msec
xBinS = 20; %1 msec
ccg_intragroups('CTB', maxlag, xBinS, s7);
ccg_intragroups('CTT', maxlag, xBinS, s7);
ccg_intragroups('CDS', maxlag, xBinS, s7);
ccg_intragroups('CDF', maxlag, xBinS, s7);
ccg_intragroups('DT_', maxlag, xBinS, s7);

% calculate INTRAgroup (small) ccgs:
s8 = 1;
maxlag = 50; % 50 msec
xBinS = 1; %1 msec
ccg_intragroups('CTB', maxlag, xBinS, s8);
ccg_intragroups('CTT', maxlag, xBinS, s8);
ccg_intragroups('CDS', maxlag, xBinS, s8);
ccg_intragroups('CDF', maxlag, xBinS, s8);
ccg_intragroups('DT_', maxlag, xBinS, s8);

% calculate INTERgroup ccgs:
s9 = 1;
maxlag = 3*nsr; % 3 sec
xBins = 20; %20 msec
ccg_intergroups(maxlag, xBins, s9);

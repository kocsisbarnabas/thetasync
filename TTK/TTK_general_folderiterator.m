function TTK_general_folderiterator()

DATAPATH = 'E:\TTKmouse\DATA';

animals = dir(DATAPATH);
animals = animals(3:end);

for it1 = 1:length(animals)
    if animals(it1).isdir
    animalId = animals(it1).name;
    recordings = dir(fullfile(DATAPATH, animalId));
    recordings = recordings(3:end);
    for it2 = 1:length(recordings)
        if recordings(it2).isdir
        recordingId = recordings(it2).name;
        operation(DATAPATH, animalId, recordingId);
        end
    end
    end
end


end

function operation(DATAPATH, animalId, recordingId)
    createEventFile(animalId, recordingId)
end
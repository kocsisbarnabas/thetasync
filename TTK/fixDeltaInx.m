% soutputVector = sortrows(allCell, mO('DeAcgDeInx'));
% nSignif = ceil(size(soutputVector, 1)/20);
% deInx = mO('deltaAcg');
% for it1 = 1:nSignif
% figure; plot(soutputVector(end-it1, deInx(1):deInx(2)));
% title(num2str(soutputVector(end-it1,randmO('DeAcgDeInx'))));
% end

nsr = 1000;

folder1 = dir('E:\TTKmouse\november13\cell_features\figures');
suspInx = [9, 10, 13, 21, 27, 71, 76, 77, 87, 97, 106, 107, 501, 514];
for it = suspInx
    openfig(['E:\TTKmouse\november13\cell_features\figures\', folder1(it).name])
    title(it)
    s = strfind(folder1(it).name, '_');
    Id = [str2num(folder1(it).name(1:8)), str2num(folder1(it).name(1:s(1)-1)), str2num(folder1(it).name(end-4)), str2num(folder1(it).name(s(4)+1:s(5)-1))];
    TTK_raster_plottozas_II(num2str(Id(1)), num2str(Id(2)), num2str(Id(3)), Id(4), TTK_troughs(Id(2))*nsr, 100*nsr),
    close all
end
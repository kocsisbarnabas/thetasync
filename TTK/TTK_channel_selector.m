function TTK_channel_selector(varargin)
%under development!!!!!!!!
%TTK_CHANNEL_SELECTOR   Select hippocampal theta reference channel.
%   TTK_CHANNEL_SELECTOR(VARARGIN) called by CHANNEL_SELECTOR_CALLER(NSR, S)
%   Visualizes theta phase change to find pyramidal layer. 
%   Parameters:
%       ANIMAL: animal folder path,
%       RECORD: file name, 
%       THETATIME: time of dominant theta (2x1 vector: start and end point
%           in seconds), 
%       DELTATIME: time of dominant delta (2x1 vector, same as above), 
%       DISPCHANNELS: channel numbers that needs to be displayed (2x1 
%           vector, from channel... to channel...), 
%       NSR: new sampling rate
%       S: save?, logic varriable
%
%   Reads EEG data. Makes matrices (e.g.: 65 channel * (10*sampling rate)
%   -> 10 sec long data for each channel) for dominant theta and dominant
%   delta. (Their time intervalls provided by THETATIME and DELTATIME
%   vectors). Filter to ripple ([90, 220] Hz), and  filter to theta ([3, 8]
%   Hz), then plot data and save figure.
%
%   See also MSHCSP, CHANNEL_SELECTOR.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

% Global variables
sr = 20000;
nsr = 1000;
rootDir = 'D:\TTKmouse\b\';

% Default variables
defaultAnimal = '20170608\5\';
defaultRecord = '201706085_32ch';
defaultnChannels = 32;
defaultThetaTime = [300, 400];
defaultDeltaTime = [50, 100];
defaultDispChannels = [1, 32];
% defaultNsr = 1000;
defaultSave = 0;

% Input arguments
p = inputParser;
addOptional(p, 'animal', defaultAnimal, @ischar);
addOptional(p, 'record', defaultRecord, @ischar);
addOptional(p, 'nChannels', defaultnChannels, @isnumeric);
addOptional(p, 'thetaTime', defaultThetaTime, @isnumeric)
addOptional(p, 'deltaTime', defaultDeltaTime, @isnumeric)
addOptional(p, 'dispChannels', defaultDispChannels, @isnumeric)
% addOptional(p, 'nsr', defaultNsr, @isnumeric)
addOptional(p, 's', defaultSave, @isnumeric)
parse(p,varargin{:});

animal = p.Results.animal;
record = p.Results.record;
nChannels = p.Results.nChannels;
thetaTime = p.Results.thetaTime;
deltaTime = p.Results.deltaTime;
dispChannels = p.Results.dispChannels;
% nsr = p.Results.nsr;
s = p.Results.s;



fname = [rootDir animal record];

fileinfo = dir([fname '.dat']);
%fileinfo.bytes/2/nChannels/sr gives the length of the recording in seconds

%open data file:
datafile = fopen([fname '.dat'],'r');

%allocate data matrices:
% ripple_eeg = zeros(nChannels, (deltaTime(2)-deltaTime(1))*sr);
theta_eeg = zeros(nChannels, (thetaTime(2)-thetaTime(1))*sr);

%we will read data by section
%(reading immediately the whole data would crash the memory)

l = max(thetaTime(2), deltaTime(2));
for x = 1:l
    data = fread(datafile,[nChannels,sr],'int16'); %read one second (1*sampling rate) with int16 precision
%     if x>deltaTime(1) & x<deltaTime(2)+1 % push back resampled data to matrix if it's between the provided timepoints (dominant delta)
%         ripple_eeg(1:nChannels, ((x-deltaTime(1)-1)*sr+1):((x-deltaTime(1))*sr)) =  data; 
%     end
    if x>thetaTime(1) & x<thetaTime(2)+1 % push back resampled data to matrix if it's between the provided timepoints (dominant theta)
        theta_eeg(1:nChannels, ((x-thetaTime(1)-1)*sr+1):((x-thetaTime(1))*sr)) =  data; 
    end
end

fclose(datafile);

%Order channels, based on channel sequence:
% oRippleEeg = ripple_eeg;
oThetaEeg = theta_eeg;


oThetaEeg = oThetaEeg(dispChannels(1):dispChannels(2), :);
% oRippleEeg = oRippleEeg(dispChannels(1):dispChannels(2), :);

%time vector (x axis):
thetaLength = thetaTime(2)-thetaTime(1);
timev = linspace(thetaTime(1),thetaTime(2), thetaLength*sr);

%orignal (simply resampled) data:
EEGShift = repmat(((1:size(oThetaEeg, 1))*1000)', 1, (thetaTime(2)-thetaTime(1))*sr);
figure; plot(timev, (EEGShift'+oThetaEeg'))
set(gca, 'YTick', [dispChannels(1):dispChannels(2)]'.*1000, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
title(['MultiUnit, ' record ', from: ' num2str(thetaTime(1)) ' to ' num2str(thetaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
xlabel('s');

if s
    savefig([rootDir workfolder mfilename '\' record '_EEGShift.fig']);
end
% close;


%filter data:
    
thetaFilter = fir1(1024,[2.5 8]/(sr/2),'bandpass');
thetaData = filtfilt(thetaFilter,1,oThetaEeg');
thetaShift = repmat(((1:size(oThetaEeg, 1))*1800)', 1, (thetaTime(2)-thetaTime(1))*sr);
figure; plot(timev, (thetaShift'+thetaData))
set(gca, 'YTick', [dispChannels(1):dispChannels(2)]'.*1800, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
title(['Theta phase change, ' record ', from: ' num2str(thetaTime(1)) ' to ' num2str(thetaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
xlabel('s');

%analying phase change:
channel_difference(thetaData, thetaTime);

if s
    savefig([rootDir workfolder mfilename '\' record '_thetaShift.fig']);
end
close;

% rippleFilter = fir1(1024,[90 220]/(sr/2),'bandpass');
% rippleData = filtfilt(rippleFilter,1,oRippleEeg');
% rippleShift = repmat(((1:size(oRippleEeg, 1))*500)', 1, (deltaTime(2)-deltaTime(1))*sr);
% figure; plot(timev, (rippleShift'+rippleData))
% set(gca, 'YTick', [dispChannels(1):dispChannels(2)]'.*500, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
% title(['Ripples, ' record ', from: ' num2str(deltaTime(1)) ' to ' num2str(deltaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
% xlabel('s');
% 
% if s
%     savefig([rootDir workfolder mfilename '\' record '_ripples.fig']);
% end
% close;

%calculate energies for each channel:
% energy = sum(abs(fdata), 2);

% figure;
% plot(energy)
% for it = 1:33
% text(it, energy(it), num2str(it));
% end
% savefig(['C:\Users\Barnabás\Desktop\EEG\default_folder\Balazs grantjába\' file1 ' ripples']);
% figure
% imagesc(oeeg)

% [value, max_E_channel] = max(energy);
% value
% max_E_channel = max_E_channel + 32; % because we cut the first 32 elements previously

end
function TTK_same_clusters_shifted_II()
%identifying same clusters from different recordings, shifted
%deepth. Reads in CREATE_AVERAGE_WAVEFORMS() generated average waveforms.
%(Also contains post-modification (spliting, merging clusters -> new
%clusters) clusters.

s = 0; %save figure and similarity to text file?

rootDir = 'E:\TTKmouse\DATA';
animalId = '20170216';

%recordings to compare:
recording1 = '2_3';
recording2 = '4_5'; %DEEPER LEVEL

dShift = 500; %downward shift (um)
chSep = 22.5; %channel separation (distance between adjacent channels bottoms)
nchRow = 4; %number of channels in a row
nChannels = 128; %number of channels
nChShift = round(dShift/chSep); %downshift on channel scale


load(fullfile(rootDir, animalId, recording1, [animalId, recording1, '_avr_waveforms.mat']));

templates1 = allAvgWaveforms; clear allAvgWaveforms;
[cids1, cgs1] = readClusterGroupsCSV(fullfile(rootDir, animalId, recording1, 'cluster_groups.csv'));
goodClus1 = cids1(cgs1 == 2);%identified as 'good'
load(fullfile(rootDir, animalId, recording2, [animalId, recording2, '_avr_waveforms.mat']));

templates2 = allAvgWaveforms; clear allAvgWaveforms;
[cids2, cgs2] = readClusterGroupsCSV(fullfile(rootDir, animalId, recording2, 'cluster_groups.csv'));
goodClus2 = cids2(cgs2 == 2);%identified as 'good'

numCh = 16; %number of channels to compare waveforms on

%sort channels, based on amplitudes (biggest waveforms at the top):
spikeEnergy1 = squeeze(sum(abs(templates1), 3));
[oSpikeEnergy1, inx1] = sort(spikeEnergy1.', 'descend');
% for it1 = 1:size(templates1, 1)
%     templates1(it1, :, :) = templates1(it1, inx1(:, it1), :);
% end

spikeEnergy2 = squeeze(sum(abs(templates2), 3));
[oSpikeEnergy2, inx2] = sort(spikeEnergy2.', 'descend');
% for it1 = 1:size(templates2, 1)
%     templates2(it1, :, :) = templates2(it1, inx2(:, it1), :);
% end

similarity = zeros(size(templates1, 1), size(templates2, 1));
c = 0;
for it1 = 1:size(templates1, 1)
    shiftInx1 = inx1(:,it1)-nChShift*nchRow;
    for it2 = 1:size(templates2, 1)
        [inters, aind, bind] = intersect(shiftInx1(1:numCh), inx2(1:numCh, it2)); %compare template1 and template2, for the same channels
        if length(inters)>8
            weight1 = spikeEnergy1(it1, inters+nChShift*nchRow).';
            weight2 = spikeEnergy2(it2, inters).';
            %difference on channels:
            differ = abs(squeeze(templates1(it1, inters+nChShift*nchRow, :))).*repmat(weight1, 1, size(templates1, 3))-abs(squeeze(templates2(it2, inters, :))).*repmat(weight2, 1, size(templates2, 3));
            similarity(it1, it2) = 1/sum(sum(abs(differ)))*length(inters);
                        figure('Position',[700,400,500,400]); plot(squeeze(templates1(it1, inters+nChShift*nchRow, :)).', 'b')
                        hold on, plot(squeeze(templates2(it2, inters, :)).', 'r')
                        title(num2str(similarity(it1, it2)));
                        close all;
        c = c+1;
        end
    end
end
[rowInx, colInx] = find(similarity ~= 0);
figure; imagesc(similarity), text(colInx-0.5, rowInx-0.3, num2str(goodClus1(rowInx).'))
text(colInx-0.5, rowInx+0.3, num2str(goodClus2(colInx).')), title([animalId, ': ', recording1, ' ', recording2])

end


function TTK_same_clusters_pattern(animalId, recording1, recording2, cellId1, cellId2)
%TTK_SAME_CLUSTERS_PATTERN() plots autocorrelation (and activity pattern) of two similar
%clusters.

if nargin == 0
    
end

rootDir = 'E:\TTKmouse\DATA';

clu1 = load(fullfile(rootDir, animalId, recording1, [animalId, recording1, '.clu.1']));
res1 = load(fullfile(rootDir, animalId, recording1, [animalId, recording1, '.res.1']));
clu2 = load(fullfile(rootDir, animalId, recording2, [animalId, recording2, '.clu.1']));
res2 = load(fullfile(rootDir, animalId, recording2, [animalId, recording2, '.res.1']));

sr = 20000;
nsr = 1000;
dr = sr/nsr;


Aps1 = res1(clu1 == cellId1)/sr;
Aps2 = res2(clu2 == cellId2)/sr;


% figure('Position',[200,400,500,400]); line([sAps1/sr, sAps1/sr].', repmat([0; 1], 1,length(sAps1)), 'Color', [1, 0, 0]);
% hold on, line([sAps2/sr, sAps2/sr].', repmat([1; 2], 1,length(sAps2)), 'Color', [1, 0, 0]);
% set(gca, 'ytick',[0.5, 1.5]);set(gca,'yticklabel',{recording1, recording2});


end
function create_average_waveforms(varargin)
%CREATE_AVERAGE_WAVEFORMS() plots average waveform of a cluster's spikes.
%   Calculates average waveform for all cluster on all channels. (Indices
%   starts from 0, python consistent).  Data is directly read from .dat file.

if nargin == 0
    rootDir = 'E:\TTKmouse\DATA';
    animalId = '20170608';
    recordingId = '201706086';
else
    rootDir = varargin{1};
    animalId = varargin{2};
    recordingId = varargin{3};
end

nChannels = 128; %number of channels on the electrode
SR = 20000; %sampling frequency
recPoints = 82; %number of recording points (82*1/20000 = 4.1 usec)

datafile = fopen(fullfile(rootDir, animalId, recordingId, [recordingId, '_128ch.dat']), 'r');
st = readNPY(fullfile(rootDir, animalId, recordingId, 'spike_times.npy')); %spike times
clu = readNPY(fullfile(rootDir, animalId, recordingId, 'spike_clusters.npy')); %spike clusters (corresponding to st)

[cids, cgs] = readClusterGroupsCSV(fullfile(rootDir, animalId, recordingId, 'cluster_groups.csv'));
goodClus = cids(cgs == 2);%identified as 'good'

allAvgWaveforms = zeros(length(goodClus), nChannels, recPoints); %all clusters average waveforms for all channels
for it1 = 1:length(goodClus)%iterate trough all clusters
    cluId = goodClus(it1);
    frewind(datafile);  %move back position indicator
    ST = st(clu==cluId); %spike times corresponding to cluId
    
    waveforms = zeros(length(ST), nChannels, recPoints);
    
    % Read data
    cntr = 1;
    for buffered = 1:ST(end)/SR
        data = fread(datafile,[nChannels,SR],'int16');
        bufferedST = ST((buffered-1)*SR + 1<ST & ST<buffered*SR);
        for it3 = 1:length(bufferedST)
            tPData = (bufferedST(it3)-SR*(buffered-1)); %timepoint in buffered data
            if tPData + 40 <SR & tPData - 40 > 0 %if sticking out the waveform, renounce it
                
                try
                    waveforms(cntr, :, :) = data(:, tPData-40:tPData+41);
                catch ME
                end
                cntr = cntr + 1;
            end
        end
    end
    
    %average waveform:
    avrgWave = zeros(recPoints, nChannels);
    for it2 = 1:nChannels
        avrgWave(:, it2) = mean(squeeze(waveforms(:, it2, :))).'; %average waveform on one channel
        avrgWave(:, it2) = avrgWave(:, it2)-mean(avrgWave(:, it2)); %align around zero
        avrgWave(:, it2) = avrgWave(:, it2) + abs(min(avrgWave(:, it2))); %shift above zero
    end
    %     figure; plot(avrgWave), text(repmat(recPoints, nChannels, 1), avrgWave(end, :), num2str([1:nChannels].'))
    allAvgWaveforms(it1, :, :) = avrgWave.';
    
end

% %sort channels, based on amplitudes (biggest waveforms at the top):
% spikeEnergy = squeeze(sum(abs(allAvgWaveforms), 3));
% [~, inx] = sort(spikeEnergy.', 'descend');
% for it1 = 1:length(inx)
%     allAvgWaveforms(it1, :, :) = templates1 = (it1, inx(:, it1), :);
% end

fclose(datafile);
save(fullfile(rootDir, animalId, recordingId, [recordingId, '_avr_waveforms.mat']), 'allAvgWaveforms');

end
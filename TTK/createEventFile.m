function createEventFile(animalId, recordingId)
%Creates event files (.clu: cluster ID of the AP, hapenning at .res time;
%the rows are corresponding) from automatically sorted, than manually validated experimental
%data.

global DATAPATH;

if nargin == 0
    animalId = '20170216';
    recordingId = '2017021645';
    if ~exist(DATAPATH)
        DATAPATH = 'E:\TTKmouse\DATA';
    end
end

dataDirectory = fullfile(DATAPATH, animalId, recordingId);

spikeClustersPath = fullfile(dataDirectory,'spike_clusters.npy');
spikeTimesPath= fullfile(dataDirectory,'spike_times.npy');
clusterGroupsPath = fullfile(dataDirectory,'cluster_groups.csv');


fid = fopen(clusterGroupsPath);
clusterGroups = textscan(fid, '%d %s', 'Delimiter', '\t', 'HeaderLines', 1);
fclose(fid);

spike_clusters = readNPY(spikeClustersPath);
spike_times = readNPY(spikeTimesPath);
clusterIDs = unique(spike_clusters);
clusterSpikeTimes = [];

goodClusterIDs = clusterIDs;
indexes = [];
for i = 1: numel(clusterIDs)
    
    if isequal(char(clusterGroups{2}(i)),'mua') || isequal(char(clusterGroups{2}(i)),'noise') || isequal(char(clusterGroups{2}(i)),'unsorted')
        indexes = [indexes i];
    end
end
goodClusterIDs(indexes,:) = [];


spike_times(~ismember(spike_clusters, goodClusterIDs))= [];
spike_clusters(~ismember(spike_clusters, goodClusterIDs))= [];

save(fullfile(dataDirectory, [recordingId '_goodClusterIDs.mat']), 'goodClusterIDs');

fileID = fopen(fullfile(dataDirectory, [recordingId '.clu.1']), 'w');
fprintf(fileID, '%d\n', spike_clusters);
fclose(fileID);

fileID = fopen(fullfile(dataDirectory, [recordingId '.res.1']), 'w');
fprintf(fileID, '%d\n', spike_times);
fclose(fileID);

end
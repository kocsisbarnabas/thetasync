function TTK_tsccg_wrapper(issave)
% TTK_TSCCG_WRAPPER(ISSAVE) Wrapper function for TTK_TSCCG
%   issave: save?, logic varriable
%
%   See also TTKproject, TTK_TSCCG, TTK_MSPREADEEG.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 21/10/2017

global RESULTDIR;

% Create folder for figures and data
if issave
    resdir = fullfile(RESULTDIR,'tsccg');
    if ~exist(resdir,'dir')
         mkdir(resdir);
         %create subfolders:
         mkdir(fullfile(resdir, 'theta_segments')); %for theta segments (logic vector)
         mkdir(fullfile(resdir, 'theta_segments', 'segments_length')); %for segments length
         mkdir(fullfile(resdir, 'theta_angles')); %for theta segments angles
         mkdir(fullfile(resdir, 'delta_angles')); %for delta segments angles
         mkdir(fullfile(resdir, 'eeg_figs')); %for field potential figures
    end
end

recordings = dir(fullfile(RESULTDIR, 'mspreadeeg'));
recordings = recordings(3:end);

for it1 = 1:length(recordings)
        inx = strfind(recordings(it1).name, '.');
        recordingId = recordings(it1).name(1:inx(1)-1);
        TTK_tsccg(recordingId, issave, resdir)
end

end
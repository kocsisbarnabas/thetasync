function TTK_load_randpoisson(segm, issave)
%TTK_LOAD_RANDPOISSON(SEGM, ISSAVE) compute thresholds for theta and delta indices
%(or for any calculated parameters).
%   First sort indices (computed by RANDPOISSON_PROCESS()) of cells, and
%   find the highest 5% (above significance level), and define threshold
%   according to that.
%   SEGM is a string (can be 'theta' or 'delta') controlling which segments
%   should be analysed
%   ISSAVE: save?.
%
%   See also TTKproject, RANDPOISSON_PROCESS, RANDPOISSON.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 11/11/2017

global RESULTDIR;

%load matrix:
load(fullfile(RESULTDIR, 'randpoisson_process', 'randpoisson.mat')); %load randpoisson matrix
%load map for matrix (randmO):
load(fullfile(RESULTDIR, 'randpoisson_process', 'RandPomapObj.mat')); 

signifLevel = 5; %significance level, in percent
signif = 100/signifLevel;
Thsumacr = 2000; %Threshold for sum(acr)
ThApfr = 0.0004; %Threshold fo action potential frequency
Desumacr = 2000;
DeApfr = 0.0004;

if strcmp(segm, 'delta')
    %under delta:
    outputVector(outputVector(:, randmO('desumacr'))<Desumacr | outputVector(:,randmO('numApDelta'))./outputVector(:, randmO('delta_length'))<ThApfr, :) = [];
    figure; plot(outputVector(:, randmO('DeAcgThInx')), outputVector(:, randmO('DeAcgDeInx')), '.');
    title({'randpoisson, under delta: TH vs DE inx'; ['criterion: sum(acr)>' num2str(Desumacr)]});
    
    Inxsort1 = sortrows(outputVector, randmO('DeAcgThInx'));
    Inxsort2 = sortrows(outputVector, randmO('DeAcgDeInx'));
    threshold1 = Inxsort1(end-round(length(Inxsort1)/signif), randmO('DeAcgThInx')); % threshold1 (only 5% is above that)
    threshold2 = Inxsort2(end-round(length(Inxsort2)/signif), randmO('DeAcgDeInx')); %threshold2
end

if strcmp(segm, 'theta')
    %under theta:
    outputVector(outputVector(:, randmO('thsumacr'))<Thsumacr | outputVector(:,randmO('numApTheta'))./outputVector(:, randmO('theta_length'))<DeApfr, :) = [];
    figure; plot(outputVector(:, randmO('ThAcgThInx')), outputVector(:, randmO('ThAcgDeInx')), '.');
    title({'randpoisson, under theta: TH vs DE inx'; ['criterion: sum(acr)>' num2str(Thsumacr)]});
    
    Inxsort1 = sortrows(outputVector, randmO('ThAcgThInx'));
    Inxsort2 = sortrows(outputVector, randmO('ThAcgDeInx'));
    threshold1 = Inxsort1(end-round(length(Inxsort1)/signif), randmO('ThAcgThInx')); % threshold1 (only 5% is above that)
    threshold2 = Inxsort2(end-round(length(Inxsort2)/signif), randmO('ThAcgDeInx')); %threshold2
end

hold on; plot([threshold1, threshold1], [-0.25, 1], 'k'); %display threshold
hold on; plot([-1, 1], [threshold2, threshold2], 'k');
plot([threshold1, 10*threshold1], [threshold2, 10*threshold2], 'k');
xlim([-1, 1]);
ylim([-0.25, 1]);
text(threshold1, 0.4, num2str(threshold1));
text(0, threshold2, num2str(threshold2));

if strcmp(segm, 'delta') & issave
    status1 = mkdir(fullfile(RESULTDIR, 'load_randpoisson'));
    save(fullfile(RESULTDIR, 'load_randpoisson', 'duringDeltaParams.mat'), 'threshold1', 'threshold2');
    savefig(fullfile(RESULTDIR, 'load_randpoisson', 'duringDelta.fig'));
end

if strcmp(segm, 'theta') & issave
    status1 = mkdir(fullfile(RESULTDIR, 'load_randpoisson'));
    save(fullfile(RESULTDIR, 'load_randpoisson', 'duringThetaParams.mat'), 'threshold1', 'threshold2');
    savefig(fullfile(RESULTDIR, 'load_randpoisson', 'duringTheta.fig'));
end

close all
end
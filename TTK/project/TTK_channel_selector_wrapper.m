function TTK_channel_selector_wrapper(issave)
%TTK_CHANNEL_SELECTOR_WRAPPER wrapper function for TTK_CHANNEL_SELECTOR
%   TTK_CHANNEL_SELECTOR_WRAPPER(ISSAVE) calls TTK_CHANNEL_SELECTOR for each animal once,
%   to find pyramidal layer, based on displaying theta phase change and
%   ripple change. (Recordings provided are 'nice' recordings -> theta and
%   delta segments are seperated.) Saves figures and data if S == 1.
%
%   See also
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/10/2017

global RESULTDIR
global DATAPATH

% Create folder for figures and data
if issave
    resdir = fullfile(RESULTDIR,'channel_selector');
    if ~exist(resdir,'dir')
        mkdir(resdir);
    end
end

animals = dir(DATAPATH);
animals = animals(3:end);

for it1 = 1:length(animals)
    if animals(it1).isdir
        animalId = animals(it1).name;
        recordings = dir(fullfile(DATAPATH, animalId));
        recordings = recordings(3:end);
        for it2 = 1:length(recordings)
            recordingId = recordings(it1).name;
            TTK_channel_selector(animalId, recordingId, [320, 370], issave, resdir);
            break; %one recording is enough
        end
    end
end

end
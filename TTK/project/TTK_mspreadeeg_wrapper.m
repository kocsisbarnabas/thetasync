function TTK_mspreadeeg_wrapper(issave)
% TTK_MSPREADEEG_WRAPPER(ISSAVE) Wrapper function for TTK_MSPREADEEG
%   S: save?, logic varriable
%
%   See also TTKPROJECT, TTK_CHANNEL_SELECTOR, TTK_MSPREADEEG.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/10/2017

global RESULTDIR;
global DATAPATH;

% Create folder for figures and data
if issave
    resdir = fullfile(RESULTDIR,'mspreadeeg');
    if ~exist(resdir,'dir')
         mkdir(resdir);
    end
end

animals = dir(DATAPATH);
animals = animals(3:end);

%hippocampal radiatu channels (400 um below pyramidal layer (phase change))
chosenChannels = [16, 25, 16, 20]; %[16, 25, 16, 20];

for it1 = 1:length(animals)
    if animals(it1).isdir
    animalId = animals(it1).name;
    recordings = dir(fullfile(DATAPATH, animalId));
    recordings = recordings(3:end);
    for it2 = 1:length(recordings)
        if recordings(it2).isdir
        recordingId = recordings(it2).name;
        TTK_mspreadeeg(animalId, recordingId, chosenChannels(it1), issave, resdir);
        end
    end
    end
end

end
function TTK_channel_selector(varargin)
%TTK_CHANNEL_SELECTOR   Select hippocampal theta reference channel.
%   TTK_CHANNEL_SELECTOR(VARARGIN) called by TTK_CHANNEL_SELECTOR_WRAPPER(issave)
%   Visualizes theta phase change to find pyramidal layer. 
%   Parameters:
%       ANIMALID: animal folder path,
%       RECORDINGID: file name, 
%       THETATIME: time of dominant theta (2x1 vector: start and end point
%           in seconds), 
%       ISSAVE: save?, logic varriable
%
%   Reads field data. Makes matrices (e.g.: 32 channel * (10*sampling rate)
%   -> 10 sec long data for each channel) for dominant theta. (Their time 
%   intervall provided by THETATIME vector). Filters to theta ([THBAND] Hz),
%   then plots data and save figure.
%
%   See also.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 10/20/2017

% Global variables
global DATAPATH
global RESULTDIR
global SR
global THBAND


% Default variables
defaultAnimalId = '20170608';
defaultRecordingId = '2017060845';
defaultThetaTime = [380, 480];
defaultSave = false;
defaultResdir = fullfile(RESULTDIR,'channel_selector');

% Input arguments
p = inputParser;
addOptional(p, 'animalId', defaultAnimalId, @ischar);
addOptional(p, 'recordingId', defaultRecordingId, @ischar);
addOptional(p, 'thetaTime', defaultThetaTime, @isnumeric)
addOptional(p, 'issave', defaultSave, @islogical)
addOptional(p, 'resdir', defaultResdir, @ischar)
parse(p,varargin{:});

animal = p.Results.animalId;
recording = p.Results.recordingId;
thetaTime = p.Results.thetaTime;
issave = p.Results.issave;
resdir = p.Results.resdir;

nChannels = 32;

% Open data file
datafile = fopen(fullfile(DATAPATH, animal, recording, [recording, '_32ch.dat']), 'r');

% Allocate data matrices
thetaField = zeros(nChannels, (thetaTime(2)-thetaTime(1))*SR);

% Read data
for buffered = 1:thetaTime(2)
    data = fread(datafile,[nChannels,SR],'int16');  % read one second (1*sampling rate) with int16 precision (reading immediately the whole data would crash the memory)
    if buffered>thetaTime(1) && buffered<thetaTime(2)+1  % store resampled data if it'issave between the provided timepoints (dominant theta)
        thetaField(1:nChannels, ((buffered-thetaTime(1)-1)*SR+1):((buffered-thetaTime(1))*SR)) =  data; 
    end
end
fclose(datafile);


%time vector (buffered axis):
thetaLength = thetaTime(2)-thetaTime(1);
timev = linspace(thetaTime(1),thetaTime(2), thetaLength*SR);

% Orignal (resampled) data
shft = 1800; %shift channels (just for visualization)
signalShift = repmat(((1:size(thetaField, 1))*shft)', 1, (thetaTime(2)-thetaTime(1))*SR);
H = figure;
plot(timev, (signalShift'+thetaField'))
title(['Field, ' recording ', from: ' num2str(thetaTime(1)) ' to ' num2str(thetaTime(2)) ', for channels (top-bottom): ventral-dorsal']);
xlabel('s');
set(gca, 'YTick', [shft, shft*16, shft*32], 'YTickLabel', {'1', '16', '32'})

if issave
    savefig(fullfile(resdir,[recording '_field.fig']));
end

% Filter data to theta band
thetaFilter = fir1(1024,THBAND/(SR/2),'bandpass');   % theta band
thetaData = filtfilt(thetaFilter,1,thetaField');
thetaShift = repmat(((1:size(thetaField, 1))*shft)', 1, (thetaTime(2)-thetaTime(1))*SR);
hold on, plot(timev, (thetaShift'+thetaData), 'Color', [0, 0, 0], 'LineWidth', 0.01);
if issave
    savefig(fullfile(resdir,[recording '_thetaShift.fig']));
    close(H);
end
end
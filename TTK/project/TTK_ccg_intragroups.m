function TTK_ccg_intragroups(str, maxlag, xBinS, issave)
%TTK_CCG_INTRAGROUPS(STR, MAXLAG, XBINS, ISSAVE) calculates cross correlations of cell pairs 
%belonging to the same group if it is possible (both of them in the same
%recording) by calling CCG_CALCULATIONS().
%   STR: 3 letter ID of cell group (string, e.g.: 'CTB' -> constitutive bursting theta cells), 
%   MAXLAG: window size for cross correlation
%   XBINS: size of binns to smooth ccg-s
%   ISSAVE: save?. 
%
%   See also TTKproject, CCG_CALCULATIONS, CELLTYPE, IMAGECCGS, TTK_CELL_GROUPS, TTK_CCG_INTERGROUPS.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 11/11/2017

global RESULTDIR;

% if nargin == 0
%     str = 'CTB';
%     maxlag = windowsize;
%     xBins = bins;
%     s = 0;
% end

%create folders and subfolders:
if issave
    status1 = mkdir(fullfile(RESULTDIR, 'ccg_calculations'));
    status2 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'ccgs'));
    status3 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'ccgs', str));
    status4 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'figures'));
    status5 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'figures', str));
end

%load cells matrix:
load(fullfile(RESULTDIR, 'data_loader', 'allCell.mat'));
%load map for allCell matrix (mO):
load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));

%choose one group (find indices (in allCell matrix) of cells belonging to STR group):
ex = celltype(str);

[recordID, inx] = unique(allCell(:, mO('animalId'):mO('recordId')), 'rows', 'stable');

% cellTypes contains recording IDs and corresponding cellIDs belonging to
% 'str' group:
cellTypes = num2cell(recordID);

%inx vector: 1st recording: from inx(1) to inx(2) cellId, 2nd recording: from
%inx(2) to inx(3) cellId, ...
inx = [inx; length(allCell(:, mO('recordId')))+1]; % length(allCell(:, 2))+1 is the endpoint

for it = 1:length(inx)-1
    cellTypes{it, 3} = ex(ismember(ex, inx(it):inx(it+1)-1));
end

for it = 1:size(cellTypes, 1) %records
    animal = cellTypes{it, 1};
    recording = [num2str(allCell(ex(it), mO('animalId'))) num2str(cellTypes{it, 2})];
    load(fullfile(RESULTDIR, 'tsccg', 'theta_segments', recording), 'theta');
    
    if length(cellTypes{it, 3})>1 %more than one cell in that recording in the group
        pairs = nchoosek(cellTypes{it, 3}, 2); %all possible pair selection for cross correlation
        for it2 = 1:size(pairs, 1)
            shankId1 = allCell(pairs(it2, 1), mO('shankId'));
            shankId2 = allCell(pairs(it2, 2), mO('shankId'));
            cellId1 = allCell(pairs(it2, 1), mO('cellId'));
            cellId2 = allCell(pairs(it2, 2), mO('cellId'));
            ccg_calculations(theta, animal, recording, shankId1, shankId2, cellId1, cellId2, maxlag, xBinS, issave, str);
            close all;
        end
    end
end

end

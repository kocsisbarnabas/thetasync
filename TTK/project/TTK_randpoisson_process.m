function TTK_randpoisson_process_wrapper()
%TTK_RANDPOISSON_PROCESS_WRAPPER()
%
%   See also TTKproject, TTK_RANDPOISSON_PROCESS.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 26/10/2017

global RESULTDIR;
global DATAPATH;
global SR;
global NSR;
global CGWINDOW;
global CGBINS;

resdir = fullfile(RESULTDIR,'randpoisson_process');
if ~exist(resdir,'dir')
    mkdir(resdir);
end

animals = dir(DATAPATH);
animals = animals(3:end);

outputVector = [];

for it1 = 1:length(animals) %it1: animalId
    if animals(it1).isdir
        animalId = animals(it1).name;
        recordings = dir(fullfile(DATAPATH, animalId));
        recordings = recordings(3:end);
        for it2 = 1:length(recordings) % it2:recordingId
            if recordings(it2).isdir
                recordingId = recordings(it2).name;
                %load theta timevector (example: 0100111...)
                load(fullfile(RESULTDIR, 'tsccg', 'theta_segments', recordingId), 'theta');
                %create delta vector (0 at dominant theta, 1 at dominant delta)
                delta = (theta==0);
                fileId = fullfile(DATAPATH, animalId, recordingId, recordingId);
                if exist ([fileId, '_goodClusterIDs.mat'], 'file') == 2
                    load([fileId, '_goodClusterIDs.mat']);
                    res = load([fileId '.res.1']); %res (timpoints)
                    clu = load([fileId '.clu.1']); %clu (which activation time)
                    for it3 = 1:length(goodClusterIDs) % clu(1) is the number of cells on the actual shank
                        cellId = double(goodClusterIDs(it3));
                        
                        actTime = round(res(clu==cellId)/(SR/NSR)); %activity pattern
                        actTime(actTime == 0) = 1; %change index if equals to 0 after round (downsampling can cause this)
                        actPattern = zeros(size(theta, 2), 1);
                        % index in the actPattern (1 where actTime fires, 0 where not)
                        actPattern(actTime) = 1;
                        if sum(actTime)<10
                            continue
                        end
                        
                        % timeserie under theta (1 where cell fires and there is theta, 0 elsewhere)
                        thetaActPattern = actPattern.*theta';
                        % timeserie under delta (1 where cell fires and there is delta, 0 elsewhere)
                        deltaActPattern = actPattern.*delta';
                        
                        [outputs, chosen_cell1] = randpoisson_process(thetaActPattern, deltaActPattern, str2num(animalId), recordingId, 1, cellId);
                        %'1' stands for shank1 (the probe only consist of one shank)
                        if ~isnan(outputs(1))
                            outputVector = [outputVector; [chosen_cell1, outputs]];
                        end
                    end
                end
            end
        end
    end
end

save(fullfile(resdir, 'randpoisson.mat'), 'outputVector');

%create map for columns interpretation:
keySet = {'animalId', 'recordId', 'cellId', 'shankId', 'ThAcgThEnergy', 'ThAcgDeEnergy', 'ThAcgWiEnergy', 'DeAcgThEnergy', 'DeAcgDeEnergy', 'DeAcgWiEnergy', ...
    'ThAcgThInx', 'ThAcgDeInx', 'DeAcgThInx', 'DeAcgDeInx', 'numApTheta', 'numApDelta', ...
    'theta_length', 'delta_length', 'thsumacr', 'desumacr', ...
    'thetaAcg', 'deltaAcg'};
acgLe = CGWINDOW*NSR*2/CGBINS; %length of acgs
valueSet = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 [21, (acgLe + 20)], [acgLe + 21, (acgLe*2 + 20)]};
randmO = containers.Map(keySet,valueSet); %mapObject

save(fullfile(resdir, 'RandPomapObj.mat'), 'randmO');

end

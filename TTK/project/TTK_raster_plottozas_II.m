function TTK_raster_plottozas_II(varargin)
%TTK_RASTER_PLOTTOZAS_II() Creates raster plot of specified cell (just for one cell).
%   TTK_raster_plottozas_II() reads in hippocampal unit activity and cut out
%   WINDOWS*2 window around the tP (transition point: delta to theta) from
%   it. Than cut out the same period from the specified cell's activity.
%   Additionally depicts the angles of hippocampal oscillation.
%
%   See also TTKproject.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 11/11/2017

global RESULTDIR
global DATAPATH
global SR
global NSR
global THBAND
global DEBAND

dr = SR/NSR;

if nargin == 0
    %Cell to examine:
    animal = '20170608';
    recording = '201706086';
    shankId = '1';
    cellId = 24;
    tP = TTK_troughs(str2num(recording))*NSR; %transition point
    windowS = NSR*10000; %Plot tP +/- windowS (tP defined below)
else
    animal = varargin{1};
    recording = varargin{2};
    shankId = varargin{3};
    cellId = varargin{4};
    tP = varargin{5};
    windowS = varargin{6};
end

%Load hippocampal unit activity:
o = load(fullfile(RESULTDIR, 'mspreadeeg', [recording '.mat']));
unitAct = double(o.fieldPot); %radiatum 

%Loads res and clu files of cell:
record_res = fullfile(DATAPATH, animal, recording, [recording '.res.' num2str(shankId)]); %res (timpoints)
record_clu = fullfile(DATAPATH, animal, recording, [recording '.clu.' num2str(shankId)]); %clu (which cell)

res = load(record_res);
clu = load(record_clu);

if tP-windowS>1
    startTime = tP-windowS;
else
    startTime = 1;
end
if tP+windowS<length(unitAct)
    endTime = tP+windowS;
else
    endTime = length(unitAct);
end

% the time points when cell fires:
cell = res(clu==cellId);
cell = round(cell/dr);
cell(cell == 0) = 1; %change index if equals to 0 after round

unitAct = unitAct(startTime:endTime); %cut out section
unitAct = (unitAct-mean(unitAct))./std(unitAct); %standardize

delta_flt = fir1(1024,[DEBAND(1) DEBAND(2)]/(NSR/2),'bandpass');
delta_feeg = filtfilt(delta_flt,1,unitAct);

theta_flt = fir1(1024,[THBAND(1), THBAND(2)]/(NSR/2),'bandpass');
theta_feeg = filtfilt(theta_flt,1,unitAct);

THtransf = hilbert(theta_feeg);
DEtransf = hilbert(delta_feeg);

figure;
timeVec = (startTime)/NSR:windowS/NSR*2/(windowS*2):(endTime)/NSR;
plot(timeVec, unitAct+(max(unitAct)+abs(min(unitAct)))/2 + 3, 'Color', [0, 0, 1])
hold on;
plot(timeVec, delta_feeg+(max(unitAct)+abs(min(unitAct)))/2 + 3, 'Color', [0, 1, 1])
plot(timeVec, theta_feeg+(max(unitAct)+abs(min(unitAct)))/2 + 3, 'Color', [0, 0, 0])
% plot(windowS+1:windowS*2, angle(THtransf(1:windowS))/pi);
% plot(angle(DEtransf(1:windowS))/pi);
plot(timeVec, angle(THtransf)/pi/2+1.5);
plot(timeVec, angle(DEtransf)/pi/2+2.5);

cell = cell(cell>startTime & cell<endTime);
cell = cell/NSR;
line([cell'; cell'], [repmat(0, 1, length(cell));repmat(1, 1, length(cell))], 'Color', [0, 0, 0])

legend('EEG', 'delta', 'theta', 'theta angle', 'delta angle', 'APs')
xlabel('s')
title({[recording ', sh: ' shankId ',  cell:' num2str(cellId)];...
    ['transition at ' num2str(tP/NSR) ', +/- ' num2str(windowS/NSR) ' (s)']})

end
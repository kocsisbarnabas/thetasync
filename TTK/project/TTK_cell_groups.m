function TTK_cell_groups(str, issave)
%TTK_CELL_GROUPS(STR, ISSAVE) loads allCell matrix from DATA_LOADER() output, and
%create cell groups according to their firing rhytmicity.
%   STR: 3 letter ID of cell group (string, e.g.: 'CTB' -> constitutive bursting theta cells)
%   ISSAVE: save?
%
%   See also TTKproject, TTK_DATA_LOADER, CELLTYPE, IMAGECCGS, CELL_FEATUES, TTK_LOAD_RADPOISSON, TTK_RANDPOISSON_PROCESS.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 11/11/2017

global RESULTDIR;
global NSR;
global CGWINDOW;
global CGBINS;

if nargin == 0
    str = 'CTB';
    issave = 0;
end

%find indices (in allCell matrix) of cells belonging to STR group
ex = celltype(str);

%create folders:
if issave
    status1 = mkdir(fullfile(RESULTDIR, 'cell_groups'));
    status2 = mkdir(fullfile(RESULTDIR, 'cell_groups', str));
end

%load cells matrix:
load(fullfile(RESULTDIR, 'data_loader', 'allCell.mat'));
%load map for allCell matrix (mO):
load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));

%open and save individual plots of acgs:
for it = 1 :  size(ex, 1)
    openfig(fullfile(RESULTDIR, 'cell_features', 'figures', [num2str(allCell(ex(it), mO('animalId'))) num2str(allCell(ex(it), mO('recordId'))) '_autocorr_of_cell_' num2str(allCell(ex(it), mO('cellId'))) '_shn' num2str(allCell(ex(it), mO('shankId'))) '.fig']));
    if issave
        savefig(fullfile(RESULTDIR, 'cell_groups', str, [num2str(allCell(ex(it), mO('animalId'))) num2str(allCell(ex(it), mO('recordId'))) '_autocorr_of_cell_' num2str(allCell(ex(it), mO('cellId'))) '_shn' num2str(allCell(ex(it), mO('shankId'))) '.fig']));
        close all;
    end
end

%Make image of all cells acgs in the group:
%Ticks and labels for displaying:
le = CGWINDOW*NSR*2/CGBINS; %acg length
xticks = [1, le/6, 2*le/6, 5*le/12, 3*le/6, 7*le/12, 4*le/6, 5*le/6, le];
windowms = NSR*CGWINDOW; %window in msec
xlabels = {-windowms -windowms*2/3 -windowms/3 -windowms/6 0 windowms/6 windowms/3 windowms*2/3 windowms};
imageccgs(sortrows(allCell(ex, :), mO('ThAcgThInx')), mO('thetaAcg'), mO('deltaAcg'), xticks, xlabels);

%cummulative image:
if issave
    savefig(fullfile(RESULTDIR, 'cell_groups', str, 'image'));
    close all;
end

end
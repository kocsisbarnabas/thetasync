function tp = TTK_troughs(record)
% TTK_TROUGHS() : Table for delta-theta transitions in hippocampal 
%   troughs() output is a delta-theta transition point in the specified
%   recording.
%
%   See also TTK_raster_plottozas, TTKproject.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 11/11/2017


%TABLE I.: strictly one transition point to one record
transitions = {2017021623, 1660; 2017021645 220; 2017021667 715; ...
    201704112345 2082; 201704116 306; 201704117 340; 201704118 310; ...
    2017060723 1305; 2017060745 1300; ...
    2017060845 314; 201706086 295};

index = find([transitions{:}] == record);
tp = transitions{index, 2}; %transition point in the specified record

% status1 = mkdir([rootDir workfolder mfilename]);
% save([rootDir, workfolder, mfilename '\troughs.mat'], 'transitions');
end
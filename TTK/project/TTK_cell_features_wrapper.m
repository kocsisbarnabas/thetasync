function TTK_cell_features_wrapper(issave)
%TTK_CELL_FEATURES_WRAPPER(ISSAVE) wrapper functions for TTK_CELL_FEATURES
%  ISSAVE: save?
%
%   See also TTKproject, TTK_TSCCG, TTK_CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 21/10/2017

global RESULTDIR;
global DATAPATH;
global SAVEOUTS;
global SR;
global NSR;

if ~exist('issave', 'var')
    issave = SAVEOUTS;
end

if issave
    resdir = fullfile(RESULTDIR,'cell_features');
    if ~exist(resdir,'dir')
        mkdir(resdir);
        %create subfolders:
        mkdir(fullfile(resdir, 'correlograms')); %for cells autocorrelograms (vectors)
        mkdir(fullfile(resdir, 'figures')); %for autocorrelograms figures
        mkdir(fullfile(resdir, 'phase_histograms')); %for cells phase histograms
        mkdir(fullfile(resdir, 'spectrograms')); %for autocorrelograms spectrograms
    end
end

animals = dir(DATAPATH);
animals = animals(3:end);

for it1 = 1:length(animals) %it1: animalId
    if animals(it1).isdir
        animalId = animals(it1).name;
        recordings = dir(fullfile(DATAPATH, animalId));
        recordings = recordings(3:end);
        for it2 = 1:length(recordings) % it2:recordingId
            if recordings(it2).isdir
                recordingId = recordings(it2).name;
                %load theta timevector (example: 0100111...)
                load(fullfile(RESULTDIR, 'tsccg', 'theta_segments', recordingId), 'theta');
                %create delta vector (0 at dominant theta, 1 at dominant delta)
                delta = (theta==0);
                %load phase vectors:
                load(fullfile(RESULTDIR, 'tsccg', 'theta_angles', recordingId), 'thetaAng');
                load(fullfile(RESULTDIR, 'tsccg', 'delta_angles', recordingId), 'deltaAng');
                fileId = fullfile(DATAPATH, animalId, recordingId, recordingId);
                if exist ([fileId, '_goodClusterIDs.mat'], 'file') == 2
                    load([fileId, '_goodClusterIDs.mat']);
                    res = load([fileId '.res.1']); %res (timpoints)
                    clu = load([fileId '.clu.1']); %clu (which activation time)
                    for it3 = 1:length(goodClusterIDs) % clu(1) is the number of cells on the actual shank
                        cellId = double(goodClusterIDs(it3));
                        
                        actTime = round(res(clu==cellId)/(SR/NSR)); %activity pattern
                        actTime(actTime == 0) = 1; %change index if equals to 0 after round (downsampling can cause this)
                        actPattern = zeros(size(theta, 2), 1);
                        % index in the actPattern (1 where actTime fires, 0 where not)
                        actPattern(actTime) = 1;
                        if sum(actTime)<10
                            continue
                        end
                        
                        % timeserie under theta (1 where cell fires and there is theta, 0 elsewhere)
                        thetaActPattern = actPattern.*theta';
                        % timeserie under delta (1 where cell fires and there is delta, 0 elsewhere)
                        deltaActPattern = actPattern.*delta';
                        
                        cell_features(thetaActPattern, deltaActPattern, thetaAng, deltaAng, animalId, recordingId, 1, cellId, issave);
                    end
                end
            end
        end
    end
end

end

% TTK anasthetized mouse project:
% 
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/10/2017

clear all;
close all;

% Add code folder to path
codepath = 'C:\Users\kocsis.barnabas\Desktop\thetasync\TTK\project';
addpath(genpath(codepath));

% Directories
global ROOTDIR
ROOTDIR = 'E:\TTKmouse';
% addpath(genpath(ROOTDIR));

global WORKFOLDER
WORKFOLDER = 'divideWithSumacr\';
global RESULTDIR
RESULTDIR = fullfile(ROOTDIR,WORKFOLDER);

if ~exist(RESULTDIR,'dir')
    status = mkdir(RESULTDIR);   % create folder for this run
end

global DATAPATH
DATAPATH = 'E:\TTKmouse\DATA';

global SAVEOUTS;
SAVEOUTS = false;


% Global parameters
global SR;   % sampling rate
SR = 20000;

global NSR;  % new sampling rate
NSR = 1000;

global CGWINDOW;   % CGWINDOW for acg
CGWINDOW = 3;

global CGBINS;  % binsize for acg
CGBINS = 20;

global THBAND;   % theta frequency band
THBAND = [2, 6];

global DEBAND;   % delta frequency band
DEBAND = [0.5, 2];

% % Create EEG figures of electrode channels for appropriate channel
% % selection (of pyramidal layer or radiatum)
% % run CHANNEL_SELECTOR() function for all folders (animal):
% s1 = true;   % logical varriable: save figures and data?
% TTK_channel_selector_wrapper(s1)

% % Downsample EEG data of the defined electrode channels, than save them
% % run MSPREADEEG_RW() function for all folders:
% s2 = true;
% TTK_mspreadeeg_wrapper(s2)
% 
% % Determine delta and theta segments of EEG, compute angles, create figure:
% % run TSCCG() function for all records:
% s3 = true;
% TTK_tsccg_wrapper(s3);
% 
% % Create acgs, phase histograms of septal cells, and analys them:
% % run CELL_FEATURES_SHANK() function which calls CELL_FEATURES() for all
% % cells
% s4 = true;
% TTK_cell_features_wrapper(s4);
% 
% % Create randpoisson process:
% TTK_randpoisson_process()
% 
% % Calculate threshold of indices 
% s5 = true;
% TTK_load_randpoisson('theta', s5);   % during theta
% TTK_load_randpoisson('delta', s5);   % during delta
% 
% % Load cell acgs and parameters in a common matrix:
% s51 = true;
% TTK_data_loader(s51)
% 
% % Not prepared yet: randpermutation(NSR) (another technique for threshold computation)
% 
% % Create image of all autocorrelogramms with computed threshold:
% s6 = true;
% cmprRhtmIndices('theta', s6)
% cmprRhtmIndices('delta', s6)

% Create folders for groups:
s7 = true;
TTK_cell_groups('CTB', s7);   % constitutive theta bursting
TTK_cell_groups('CTT', s7);   % constitutive theta tonic
TTK_cell_groups('CDS', s7);   % constitutive delta slow
TTK_cell_groups('CDF', s7);   % constitutive delta fast
% cell_groups('TD_', s7));
TTK_cell_groups('DT_', s7);   % constitutive delta-theta
% TTK_cell_groups('NT_', s7);
% TTK_cell_groups('ND_', s7);
% TTK_cell_groups('TN_', s7);
% TTK_cell_groups('DN_', s7);
% TTK_cell_groups('NN_', s7);
% TTK_cell_groups('ABS', s7);

% Create phase histograms of groups:
s71 = true;
TTK_meanHistogram('CTB', true, true, s71);   % constitutive theta bursting
TTK_meanHistogram('CTT', true, true, s71);   % constitutive theta tonic
TTK_meanHistogram('CDS', true, true, s71);   % constitutive delta slow
TTK_meanHistogram('CDF', true, true, s71);   % constitutive delta fast
% TTK_meanHistogram('TD_', true, true, s71);
TTK_meanHistogram('DT_', true, true, s71);   % constitutive delta-theta
% TTK_meanHistogram('NT_', true, true, s71);
% TTK_meanHistogram('ND_', true, true, s71);
% TTK_meanHistogram('TN_', true, true, s71);
% TTK_meanHistogram('DN_', true, true, s71);
% TTK_meanHistogram('NN_', true, true, s71);
% TTK_meanHistogram('ABS', true, true, s71);

% calculate INTRAgroup ccgs:
s8 = true;
maxlag = 3*NSR; %3000 msec
xBinS = 20; %1 msec
TTK_ccg_intragroups('CTB', maxlag, xBinS, s8);
TTK_ccg_intragroups('CTT', maxlag, xBinS, s8);
TTK_ccg_intragroups('CDS', maxlag, xBinS, s8);
TTK_ccg_intragroups('CDF', maxlag, xBinS, s8);
TTK_ccg_intragroups('DT_', maxlag, xBinS, s8);

% calculate INTRAgroup (small) ccgs:
s9 = true;
maxlag = 50; % 50 msec
xBinS = 1; %1 msec
TTK_ccg_intragroups('CTB', maxlag, xBinS, s9);
TTK_ccg_intragroups('CTT', maxlag, xBinS, s9);
TTK_ccg_intragroups('CDS', maxlag, xBinS, s9);
TTK_ccg_intragroups('CDF', maxlag, xBinS, s9);
TTK_ccg_intragroups('DT_', maxlag, xBinS, s9);

% calculate INTERgroup ccgs:
s10 = true;
maxlag = 3*NSR; % 3 sec
xBins = 20; %20 msec
TTK_ccg_intergroups(maxlag, xBins, s10);

function TTK_mspreadeeg_rw(varargin)
%TTK_MSPREADEEG(VARARGIN) (called by TTK_MSPREADEEG_WRAPPER()) imports EEG 
%   data of the TTK mouse project and exports hippocampal pyramidal
%   layer field potential to .mat files.
%   After load in the data, choose the provided channel (based on
%   TTK_CHANNEL_SELECTOR), resample it, and create an eeg vector.
%   Parameters:
%   ANIMAL (animal folder path)
%   RECORD (file name)
%   CHOOSEN_CHANNEL: channel number (in the pyramidal layer)
%   issave (save?)
%   RESDIR: results directory
%
%   See also TTKPROJECT, TTK_MSPREADEEG_WRAPPER, TTK_CHANNEL_SELECTOR.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 20/10/2017

global DATAPATH;
global RESULTDIR;
global SR;
global NSR;

% Default variables
defaultAnimalId = '20170607';
defaultRecordingId = '2017060723';
defaultChannelId = 16;
defaultSave = false;
defaultResdir = fullfile(RESULTDIR,'mspreadeeg');

% Input arguments
p = inputParser;
addOptional(p, 'animalId', defaultAnimalId, @ischar);
addOptional(p, 'recordingId', defaultRecordingId, @ischar);
addOptional(p, 'channelId', defaultChannelId, @isnumeric)
addOptional(p, 'issave', defaultSave, @islogical)
addOptional(p, 'resdir', defaultResdir, @ischar)
parse(p,varargin{:});

animal = p.Results.animalId;
recording = p.Results.recordingId;
channelId = p.Results.channelId;
issave = p.Results.issave;
resdir = p.Results.resdir;

nChannels = 32;

% Open data file
datafile = fopen(fullfile(DATAPATH, animal, recording, [recording, '_32ch.dat']), 'r');
fileinfo = dir(fullfile(DATAPATH, animal, recording, [recording, '_32ch.dat']));
nRecordings = fileinfo.bytes/2/nChannels; %gives the length of the recording for one channel

% Allocate data matrices
fieldPot = zeros(1, nRecordings/(SR/NSR));


%we will read data by section and than concatenate them
%(reading immediately the whole data would crash the memory)
bufferseconds = 10;  %read 10 second (10*sampling rate)
% Read data
for buffered = 1:(nRecordings/(bufferseconds*SR))
    data = fread(datafile,[nChannels,SR*bufferseconds],'int16');
    fieldPot(((buffered-1)*NSR*bufferseconds+1):buffered*NSR*bufferseconds) = data(channelId, 1:(SR/NSR):end);
end
fclose(datafile);

% %wavelet:
% [pow,phase,f] = eegwavelet(fieldPot,NSR);
% 
% timev = linspace(1, size(pow, 2)/NSR, size(pow, 2));
% figure, imagesc(timev,1:size(pow, 1),pow);
% b_rescaleaxis('Y',f(1:size(pow, 1)));
% set(gca,'clim',[0, 100]);
% colormap('jet');
% xlabel('s')


if issave
    save(fullfile(resdir,[recording '.mat']),'fieldPot');
%     saveas(gcf, fullfile(RESULTDIR, 'others', 'phase_change', 'radiatum_channel_wavelet', [recording, '_ch' num2str(channelId) '.jpeg'])); %save wavelet as jpeg
end

end
function TTK_same_clusters()
%identifying same clusters from different recordings, with a same
%deepth.

s = 0; %save figure and similarity to text file?

% rootDir = 'D:\TTKmouse\b';
% animal = '20170608';
rootDir = 'E:\Andor\';
animal = '2';

recording1 = '0';
recording2 = '34'; %DEEPER LEVEL

templates1 = readNPY(fullfile(rootDir, animal, recording1, 'templates.npy'));
templates2 = readNPY(fullfile(rootDir, animal, recording2, 'templates.npy'));

numCh = 16; %number of channels to compare waveforms on

%powers on first numCh channels of waveforms (sorted), for each cluster:
cluToCh1 = find_depth(templates1, numCh); %1st recording
cluToCh2 = find_depth(templates2, numCh); %2nd recording

similarity = zeros(size(templates1, 1), size(templates2, 1));
interChs = zeros(size(templates1, 1), size(templates2, 1), numCh);

%find good clusters only:
clus1 = load(fullfile(rootDir, animal, recording1, [animal recording1 '.clu.1']));
goodClus1 = unique(clus1)+1;
clus2 = load(fullfile(rootDir, animal, recording2, [animal recording2 '.clu.1']));
goodClus2 = unique(clus2)+1;

for it1 = 1:size(cluToCh1, 2)
    for it2 = 1:size(cluToCh2, 2)
        [inters, aind, bind] = intersect(cluToCh1{it1}(2, :), cluToCh2{it2}(2, :)); %compare template1 and template2, for the same channels
        %If the two clusters occupy almost the same channels, and both of
        %them are good clusters:
        if  size(inters, 2)>numCh-4 && any(it1==goodClus1) && any(it2==goodClus2)
            interChs(it1, it2, 1:length(inters)) = inters;
            
            weight1 = cluToCh1{it1}(1, :);
            weight1 = repmat(weight1(aind), size(templates1, 2), 1); %sort weight vector (to be consequent with intersect vector)
            weight2 = cluToCh2{it2}(1, :);
            weight2 = repmat(weight2(bind), size(templates2, 2), 1);
            %difference on channels:
            differ = abs(squeeze(templates1(it1, :, inters))).*weight1-abs(squeeze(templates2(it2, :, inters))).*weight2;
            similarity(it1, it2) = 1/sum(sum(abs(differ)));
            %             if it1 == 41 && it2 == 28
            %             %plot overlaping waveform pairs:
            %             wShift = repmat((1:size(inters, 2))*0.01, size(differ, 1), 1);
            %             figure; plot(squeeze(templates1(it1, :, inters))+wShift, 'b');
            %             hold on; plot(squeeze(templates2(it2, :, inters))+wShift, 'r');
            %             text(zeros(size(inters, 2), 1), wShift(1, :), num2str(inters'));
            %             end
        end
    end
end

figure; imagesc(similarity), set(gca,'clim',[0, 1.5]);

close;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%threshold:
[maxs, inx] = max(similarity');
th = min(maxs(maxs>0));

%clusters from the second recording matched to multiple clusters from the
%first rec.
unique(inx(maxs>th))

clu1 = load(fullfile(rootDir, animal, recording1, [animal, recording1, '.clu.1']));
res1 = load(fullfile(rootDir, animal, recording1, [animal, recording1, '.res.1']));
clu2 = load(fullfile(rootDir, animal, recording2, [animal, recording2, '.clu.1']));
res2 = load(fullfile(rootDir, animal, recording2, [animal, recording2, '.res.1']));

sr = 20000;
nsr = 1000;
dr = sr/nsr;
stP = 500*sr;
eP = 1000*sr;

for it1 = 1:length(maxs)
    if maxs(it1)>=th
        Aps1 = res1(clu1 == it1-1);
        sAps1 = Aps1(Aps1>stP & Aps1<eP);
        Aps2 = res2(clu2 == inx(it1)-1);
        sAps2 = Aps2(Aps2>stP & Aps2<eP);
        figure('Position',[600,400,500,400]); line([sAps1/sr, sAps1/sr].', repmat([0; 1], 1,length(sAps1)), 'Color', [1, 0, 0]);
        hold on, line([sAps2/sr, sAps2/sr].', repmat([1; 2], 1,length(sAps2)), 'Color', [1, 0, 0]);
        set(gca, 'ytick',[0.5, 1.5]);set(gca,'yticklabel',{recording1, recording2});
        title([num2str(it1-1), ' : ', num2str(inx(it1)-1), ', ', num2str(maxs(it1))]);
        
        inters = interChs(it1, inx(it1), :);
        inters(inters==0)= [];
        wShift = repmat((1:length(inters))*0.3, size(templates1, 2), 1);
        figure('Position',[1200,400,200,500]); plot(squeeze(templates1(it1, :, inters))+wShift, 'b');
        hold on; plot(squeeze(templates2(inx(it1), :, inters))+wShift, 'r');
        
        %cut recordings to the same size (assuming the shorter one was aligned to the beegining of the longer)
        len = min(length(Aps1), length(Aps2));
        Aps1 = Aps1(1:len);
        Aps2 = Aps2(1:len);
        
        timeserie_1 = zeros(ceil(max(Aps1(end), Aps2(end))/dr), 1);
        timeserie_1(ceil(Aps1/dr)) = 1;
        timeserie_2 = zeros(ceil(max(Aps1(end), Aps2(end))/dr), 1);
        timeserie_2(ceil(Aps2/dr)) = 1;
        
        [acor1, suma, alag] = correlation(timeserie_1, timeserie_1, 0, 0.5*nsr, 5);
        figure('Position',[300,400,300,200]); plot(alag, acor1);
        
        [acor2, suma, alag] = correlation(timeserie_2, timeserie_2, 0, 0.5*nsr, 5);
        hold on; plot(alag, acor2);
        
        close all;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if s
    %     savefig(fullfile(rootDir, animal, 'others\same_recording_similarity', [animal recording1 '_and_' animal recording2 '.fig']));
    fileID = fopen(fullfile(rootDir, animal, 'others', [animal recording1 '_and_' animal recording2 '.txt'] ),'w');
    for it1 = 1:size(templates1, 1)
        ind = find(similarity(it1, :)>0);
        str = [num2str(it1), ' -> '];
        for it2= 1:length(ind)
            str = [str,  num2str(ind(it2)), ':' num2str(similarity(it1, ind(it2))), ','];
        end
        str = [str, '\n'];
        fprintf(fileID, str);
    end
    fclose(fileID);
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cluToCh = find_depth(templates, numCh)
for it1 = 1:size(templates, 1)
    waveforms = squeeze(templates(it1, :, :));
    wavePower = [sum(waveforms.^2); 1:size(waveforms, 2)]'; %power on channels
    wavePower = flipud(sortrows(wavePower)); %sort channels according to wave power
    cluToCh{it1} = double(wavePower(1:numCh, :)');
end

end

% chMap = reshape(1:size(cluToCh1), 4, []);
% chMap(cluToCh1(1, :)) = 0;
% figure; plot(chMap, '*')
%
%     figure; plot(squeeze(templates1(1, :, 14)));
%     hold on; plot(squeeze(templates2(1, :,14)));






% [X,Y] = meshgrid(1:128, 1:82);
% Z = squeeze(templates1(1, :, :));
% figure; surf(X,Y,Z)
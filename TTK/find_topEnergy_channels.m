function wavePower = find_topEnergy_channels(templates, numCh)
%WAVEPOWER_TOPENERGY_CHANNELS() finds the highest energy channels for a
%specified cluster waveforms.

for it1 = 1:size(templates, 1)
    waveforms = squeeze(templates(it1, :, :));
    wavePower = [sum(waveforms.^2); 1:size(waveforms, 2)]'; %power on channels
    wavePower = flipud(sortrows(wavePower)); %sort channels according to wave power
    wavePower = wavePower(1:16, :);
end

end
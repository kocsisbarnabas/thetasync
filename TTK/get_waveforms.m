function get_waveforms(varargin)
%GET_WAVEFORMS() plots average waveform of a cluster's spikes.
%   Calculates average waveform for CLUID cluster on CHS channels. (Indices
%   starts from 0, python consistent).  Data is directly read from .dat file.

if nargin == 0
    rootDir = 'E:\TTKmouse\DATA';
    animalId = '20170608';
    recordingId = '6';
    cluId = 91; %cluster Id (to analyze, indexing starts from 0!)
    chs = [10; 15]; %observe waveform on specific channels, (phython indexing, starts from 0!!)
else
    rootDir = varargin{1};
    animalId = varargin{2};
    recordingId = varargin{3};
    cluId = varargin{4};
    chs = varargin{5};
end

nChannels = 128; %number of channels on the electrode
SR = 20000; %sampling frequency

datafile = fopen(fullfile(rootDir, animalId, recordingId, [animalId, recordingId, '_128ch.dat']), 'r');

st = readNPY(fullfile(rootDir, animalId, recordingId, 'spike_times.npy')); %spike times
clu = readNPY(fullfile(rootDir, animalId, recordingId, 'spike_clusters.npy')); %spike clusters (corresponding to st)
ST = st(clu==cluId); %spike times corresponding to cluId

recPoints = 82; %number of recording points (82*1/20000 = 4.1 usec)
waveforms = zeros(length(ST), nChannels, recPoints);

% Read data
cntr = 1;
for buffered = 1:ST(end)/SR
    data = fread(datafile,[nChannels,SR],'int16');
    bufferedST = ST((buffered-1)*SR + 1<ST & ST<buffered*SR);
    for it1 = 1:length(bufferedST)
        tPData = (bufferedST(it1)-SR*(buffered-1)); %timepoint in buffered data
        if tPData + 40 <SR & tPData - 40 > 0 %if sticking out the waveform, renounce it
            waveforms(cntr, :, :) = data(:, tPData-40:tPData+41);
            cntr = cntr + 1;
        end
    end
end
fclose(datafile);

%average waveform:
avrgWave = zeros(recPoints, length(chs));
for it1 = 1:length(chs)
    avrgWave(:, it1) = mean(squeeze(waveforms(:, chs(it1)+1, :))).'; %average waveform on one channel
    avrgWave(:, it1) = avrgWave(:, it1)-mean(avrgWave(:, it1)); %align around zero
    avrgWave(:, it1) = avrgWave(:, it1) + abs(min(avrgWave(:, it1))); %shift above zero
end

figure; plot(avrgWave)
legend(num2str(chs))

%Find biggest amplitude spikes:
numSpikes = 100;
plotBigWaves(waveforms, chs, numSpikes)

end
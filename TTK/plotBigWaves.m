function plotBigWaves(waveforms, chs, numSpikes)
%PLOTBIGWAVES(WAVEFORMS, CHS, NUMSPIKES) plots spikeforms corresponding to a cluster.
%   Plots NUMSPIKES highest energy spikes on CHS channels. WAVEFORMS
%   (numberofspikes x numberofchannels x numberofrecordingpoints matrix).

numSpikes = min(numSpikes, size(waveforms, 1)); %number of biggest amplitude spikes


for it1 = 1:length(chs)
    spikeEnergy = sum(abs(squeeze(waveforms(:, chs(it1)+1, :))), 2);
    [~, inx] = sort(spikeEnergy, 'descend');
    figure; plot(squeeze(waveforms(inx(1:numSpikes), chs(it1)+1, :)).', 'b'), title(num2str(chs(it1)))
end
end
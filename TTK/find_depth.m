function cluToCh = find_depth(templates, numCh)
for it1 = 1:size(templates, 1)
    waveforms = squeeze(templates(it1, :, :));
    wavePower = [sum(waveforms.^2); 1:size(waveforms, 2)]'; %power on channels
    wavePower = flipud(sortrows(wavePower)); %sort channels according to wave power
    cluToCh{it1} = double(wavePower(1:numCh, :)');
end

end
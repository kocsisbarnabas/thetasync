function TTK_preAnalys()
%TTK_PREANALYS()
%Indices are shifted (not equal with phy GUI indices) with one (Matlab can't
%interpret 0 index.

sr = 20000;
nsr = 1000;

%loading rez.mat (Kilosort output file):
load('C:\Users\kocsis.barnabas\Desktop\TTKmouse\AMouse_2017_06_08\4\rez.mat');
thetaTime = [340, 510];
deltaTime = [700, 800];

sr = 20000;
le = ceil(max(rez.st3(:, 1))/(sr/nsr)); %length of recording

clustId = 47; %cluster Id
timePoints = round(rez.st3(rez.st3(:, 2)==clustId, 1)/(sr/nsr));

thTimePoints = timePoints(find(timePoints>thetaTime(1)*nsr,1):find(timePoints<thetaTime(2)*nsr,1, 'last'));
deTimePoints = timePoints(find(timePoints>deltaTime(1)*nsr,1):find(timePoints<deltaTime(2)*nsr,1, 'last'));

thTimeserie = zeros(le, 1);
thTimeserie(thTimePoints) = 1;

deTimeserie = zeros(le, 1);
deTimeserie(deTimePoints) = 1;

acgWindowS = 2; %autocorrelation window size
maxlag = acgWindowS*nsr;

% %during theta:
% [Thcor,lag] = xcorr(thTimeserie, thTimeserie, maxlag);
% Thcor(lag == 0) = [];
% lag(lag == 0) = [];
% 
% %during delta:
% [Decor,lag] = xcorr(deTimeserie, deTimeserie, maxlag);
% Decor(lag == 0) = [];
% lag(lag == 0) = [];

figure;
correlation(thTimeserie, thTimeserie, 1, maxlag, 20); hold on;
correlation(deTimeserie, deTimeserie, 1, maxlag, 20);
xlabel('ms')

end
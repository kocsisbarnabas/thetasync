function plot_avr_waveform()
%PLOT_AVR_WAVEFORM() plots averaged cluster waveform for specified cluster.

rootDir = 'E:\TTKmouse\DATA';
animalId = '20170608';
recordingId = '201706086';
cellId = 12;
shankId = 1;

load(fullfile(rootDir, animalId, recordingId, [recordingId, '_avr_waveforms.mat']));
wavePower = find_topEnergy_channels(allAvgWaveforms, 16);
figure; plot(squeeze(allAvgWaveforms(cellId, wavePower(:, 2), :)).');

end
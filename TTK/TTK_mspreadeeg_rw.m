function TTK_mspreadeeg_rw(varargin)
%under development!!!!!!!!!!!!
%   MSPREADEEG(VARARGIN) (called by MSPREADEEG_CALLER()) imports EEG data of the MSHCsp project and exports pyramidal
%   layer EEG to .mat files.
%   After load in the data, choose the provided channel (based on
%   CHANNEL_SELECTOR), resample it, and create an eeg vector.
%   Parameters:
%   ANIMAL (animal folder path), RECORD (file name), CHOOSEN_CHANNEL:
%   channel number (in the pyramidal layer), NSR (new sampling rate), S (save?)
%
%   See also MSHCsp, MSPREADEEG_CALLER, CHANNEL_SELECTOR, TSCCG.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

sr = 20000;
nsr = 1000;
rootDir = 'E:\TTKmouse\b\';


defaultAnimal = '20170608\4_5\'; %'20170607/3/'; %pyramidal layer: 13
defaultRecord = '201706084_5_32ch'; %'201706085_32ch';
defaultnChannels = 32;
defaultChannel = 20; %21;
% defaultNsr = 1000;
defaultSave = 0;

p = inputParser;
addOptional(p, 'animal', defaultAnimal, @ischar);
addOptional(p, 'record', defaultRecord, @ischar);
addOptional(p, 'nChannels', defaultnChannels, @isnumeric);
addOptional(p, 'channel', defaultChannel, @isnumeric);
addOptional(p, 's', defaultSave, @isnumeric);
parse(p,varargin{:});

animal = p.Results.animal;
record = p.Results.record;
nChannels = p.Results.nChannels;
channel = p.Results.channel;
s = p.Results.s;



fname = [rootDir animal record];

fileinfo = dir([fname '.dat']);
nRecordings = fileinfo.bytes/2/nChannels; %gives the length of the recording for one channel


datafile = fopen([fname '.dat'],'r');
eeg = zeros(1, nRecordings/(sr/nsr)); %Allocate resampled EEG

%we will read data by section and than concatenate them
%(reading immediately the whole data would crash the memory)

bufferseconds = 10;  %read 10 second (10*sampling rate)

%create resampled data matrix:
for x = 1:(nRecordings/(bufferseconds*sr))
    data = fread(datafile,[nChannels,sr*bufferseconds],'int16');
    eeg(((x-1)*nsr*bufferseconds+1):x*nsr*bufferseconds) = data(channel, 1:(sr/nsr):end);
end

fclose(datafile);

% preFilter = fir1(1024,1/(nsr/2),'high');
% feeg = filtfilt(preFilter,1,eeg);


GUI_bandFilter(eeg, nsr);

[pow,phase,f] = eegwavelet(eeg,nsr);

timev = linspace(1, size(pow, 2)/nsr, size(pow, 2));
figure, imagesc(timev,1:size(pow, 1),pow);
b_rescaleaxis('Y',f(1:size(pow, 1)));
set(gca,'clim',[0, 100]);
colormap('jet');
xlabel('s')


if s
    save([rootDir workfolder mfilename '\' record '_radiatum.mat'],'eeg');
end

end
function TTK_same_clusters()
%identifying same clusters from different recordings, shifted
%deepth. Reads in python generated average waveforms (not contains
%post-modification clusters (after merging, spliting -> new cluster).

s = 0; %save figure and similarity to text file?

rootDir = 'E:\TTKmouse\DATA';
animalId = '20170608';

%recordings to compare:
recording1 = '4_5';
recording2 = '6'; %DEEPER LEVEL

dShift = 500; %downward shift (um)
chSep = 22.5; %channel separation (distance between adjacent channels bottoms)
nchRow = 4; %number of channels in a row


templates1 = readNPY(fullfile(rootDir, animalId, recording1, 'templates.npy'));
templates2 = readNPY(fullfile(rootDir, animalId, recording2, 'templates.npy'));

numCh = 16; %number of channels to compare waveforms on

%powers on first numCh channels of waveforms (sorted), for each cluster:
cluToCh1 = find_depth(templates1, numCh); %1st recording
cluToCh2 = find_depth(templates2, numCh); %2nd recording

similarity = zeros(size(templates1, 1), size(templates2, 1));

[cids1, cgs1] = readClusterGroupsCSV(fullfile(rootDir, animalId, recording1, 'cluster_groups.csv'));
goodClus1 = cids1(cgs1 == 2);
[cids2, cgs2] = readClusterGroupsCSV(fullfile(rootDir, animalId, recording2, 'cluster_groups.csv'));
goodClus2 = cids2(cgs2 == 2);

% %find good clusters only:
% clus1 = load(fullfile(ROOTDIR, animalId, recording1, [animalId recording1 '.clu.1']));
% goodClus1 = unique(clus1)+1;
% clus2 = load(fullfile(ROOTDIR, animalId, recording2, [animalId recording2 '.clu.1']));
% goodClus2 = unique(clus2)+1;

for it1 = 1:size(cluToCh1, 2)
    ampCh1 = cluToCh1{it1}; %[amplutude...; on channel]
    for it2 = 1:size(cluToCh2, 2)
        ampCh2 = cluToCh2{it2}; %[amplutude...; on channel]
        ampCh2(2, :) = max(0, ampCh2(2, :) - round(dShift/chSep)*nchRow); %shift channels
        [inters, aind, bind] = intersect(ampCh1(2, :), ampCh2(2, :)); %compare template1 and template2, for the same channels
        %If the two clusters occupy almost the same channels, and both of
        %them are good clusters:
        if  size(inters, 2)>nchRow
            if any(it1==goodClus1) && any(it2==goodClus2)
                weight1 = ampCh1(1, :);
                weight1 = repmat(weight1(aind), size(templates1, 2), 1); %sort weight vector (to be consequent with intersect vector)
                weight2 = ampCh2(1, :);
                weight2 = repmat(weight2(bind), size(templates2, 2), 1);
                %difference on channels:
                differ = abs(squeeze(templates1(it1, :, inters))).*weight1-abs(squeeze(templates2(it2, :, inters))).*weight2;
                similarity(it1, it2) = 1/sum(sum(abs(differ)));
                %             if it1 == 41 && it2 == 28
                %             %plot overlaping waveform pairs:
                %             wShift = repmat((1:size(inters, 2))*0.01, size(differ, 1), 1);
                %             figure; plot(squeeze(templates1(it1, :, inters))+wShift, 'b');
                %             hold on; plot(squeeze(templates2(it2, :, inters))+wShift, 'r');
                %             text(zeros(size(inters, 2), 1), wShift(1, :), num2str(inters'));
                %             end
            end
        end
    end
end

figure; imagesc(similarity), set(gca,'clim',[0, 1.5]);

if s
    savefig(fullfile(rootDir, animalId, 'others\same_recording_similarity', [animalId recording1 '_and_' animalId recording2 '.fig']));
    fileID = fopen(fullfile(rootDir, animalId, 'others\same_recording_similarity', [animalId recording1 '_and_' animalId recording2 '.txt'] ),'w');
    for it1 = 1:size(templates1, 1)
        ind = find(similarity(it1, :)>0);
        str = [num2str(it1), ' -> '];
        for it2= 1:length(ind)
            str = [str,  num2str(ind(it2)), ':' num2str(similarity(it1, ind(it2))), ','];
        end
        str = [str, '\n'];
        fprintf(fileID, str);
    end
    fclose(fileID);
end

end


% chMap = reshape(1:size(cluToCh1), 4, []);
% chMap(cluToCh1(1, :)) = 0;
% figure; plot(chMap, '*')
%
%     figure; plot(squeeze(templates1(1, :, 14)));
%     hold on; plot(squeeze(templates2(1, :,14)));


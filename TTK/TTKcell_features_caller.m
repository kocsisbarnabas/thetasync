function TTKcell_features_caller(s)
%TTKCELL_FEATURES_CALLER(S) calls
%CELL_FEATURES sequentially (for each record)
%  S: save?
%
%   See also MSHCsp, TSCCG, CELL_FEATURES.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 24/07/2017

global rootDir;
global workfolder;
global sr;
global nsr;
global windowsize;
global bins;

%create folder for outputs:
status1 = mkdir([rootDir workfolder 'cell_features']);

%create subfolders:
status2 = mkdir([rootDir workfolder 'cell_features\correlograms']); %for cells autocorrelograms (vectors)
status3 = mkdir([rootDir workfolder 'cell_features\figures']); %for autocorrelograms figures
status4 = mkdir([rootDir workfolder 'cell_features\phase_histograms']); %for cells phase histograms
status5 = mkdir([rootDir workfolder 'cell_features\spectrograms']); %for autocorrelograms spectrograms

animalFolders = dir(rootDir);
for it = 4:4
    recordFolders = dir(fullfile(rootDir, animalFolders(it).name));
    for it2 = 3:3
        record = [num2str(animalFolders(it).name) num2str(recordFolders(it2).name)];
%         %load theta timevector (example: 0100111...)
%         load([rootDir workfolder 'tsccg\theta_segments\' record], 'theta');
%         %load phase vectors:
%         load([rootDir workfolder 'tsccg\theta_angles\' record], 'theta_ang');
%         load([rootDir workfolder 'tsccg\delta_angles\' record], 'delta_ang');
theta = [zeros(300000, 1); ones(300000, 1); zeros(301000, 1)].';
theta_ang = theta;
delta_ang = theta;
        %iterate trough each cells:
        load(fullfile(rootDir, animalFolders(it).name, recordFolders(it2).name, [record '_goodClusterIDs.mat']));
        addpath(fullfile(rootDir, animalFolders(it).name, recordFolders(it2).name));
        for it3 = 1:length(goodClusterIDs)
            datas = [1, goodClusterIDs(it3)];
            cell_features(theta, theta_ang, delta_ang, record, datas, 0);
        end
    end
end


end
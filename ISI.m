function ISI(varargin)
% ISI() calculates interspike intervalls, and plots histograms during theta
% and delta

global RESULTDIR;
global SR;
global NSR;

dr = SR/NSR;
stepSize = 0.01; %step size for histograms

if nargin == 0
    %Cell to examine:
    animal = '4';
    recording = '201007285';
    SHANKNO = '4';
    EXAMINED_CELL = 3;
else
    animal = varargin{1};
    recording = varargin{2};
    SHANKNO = varargin{3};
    EXAMINED_CELL = varargin{4};
end

%Load unit activity:
load(fullfile(RESULTDIR, 'tsccg', 'theta_segments', recording), 'theta');
delta = (theta==0); %create delta vector (theta's complementer)

%Loads res and clu files of cell:
record_res = [recording '.res.' num2str(SHANKNO)]; %res (timpoints)
record_clu = [recording '.clu.' num2str(SHANKNO)]; %clu (which cell)

res = load(record_res);
clu = load(record_clu);

clu = (clu(2:end)); %clu(1) is the number of cells in the recording


% the time points when cell fires:
cell = res(clu==EXAMINED_CELL);
cell = round(cell/dr);
cell(cell == 0) = 1; %change index if equals to 0 after round

%erase action potentials under delta:
theta_cell = cell;
theta_cell(theta(theta_cell)==0) = [];

%erase action potentials under theta:
delta_cell = cell;
delta_cell(delta(delta_cell)==0) = [];

%Create interspike intervall histograms:
figure;
edges = [0:stepSize*NSR:NSR*1.5]; %bins
%under theta:
histogram(diff(theta_cell), edges, 'Normalization', 'probability');
title([recording ', sh: ' SHANKNO ',  cell:' num2str(EXAMINED_CELL) ', during theta'])

%under delta:
figure;
histogram(diff(delta_cell), edges, 'Normalization', 'probability');
title([recording ', sh: ' SHANKNO ',  cell:' num2str(EXAMINED_CELL) ', during delta'])
end
function ccg_intergroups(maxlag, xBinS, issave)
%CCG_INTERGROUPS(MAXLAG, XBINS, S) calculates cross correlations of cell pairs
%belonging to distinct group if it is possible (both of them in the same
%recording) by calling CCG_CALCULATIONS().
%   MAXLAG: window size for cross correlation
%   XBINS: size of binns to smooth ccg-s
%   ISSAVE: save?.
%
%   See also MSHCSP, CCG_CALCULATIONS, CELLTYPE, IMAGECCGS, CELL_GROUPS, CCG_INTERGROUPS.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR;

if nargin < 3
    maxlag = 3000;
    xBinS = 20;
    issave = 0;
end

%create folders and subfolders:
if issave
    status1 = mkdir(fullfile(RESULTDIR, 'ccg_calculations'));
    status2 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'ccgs'));
    status3 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'figures'));
end

%load cells matrix:
load(fullfile(RESULTDIR, 'data_loader', 'allCell.mat'));
%load map for allCell matrix (mO):
load(fullfile(RESULTDIR, 'data_loader', 'mapObj.mat'));

[recordID, inx] = unique(allCell(:, mO('recordId')), 'stable');

%cellTypes: 1st column are records ID, 2nd: vector of IDs of consthburst cells in the record, 3rd: vector of IDs of consthtonic cells in the record ...
cellTypes = num2cell([(allCell(inx, mO('animalId'))), recordID]);
inx = [inx; length(allCell(:, mO('recordId')))+1]; % length(allCell(:, 2))+1 is the endpoint

groupTable = {'CTB', 'CTT', 'CDS', 'CDF', 'DT_'}; %ConstThetaBurst, ConstThetaTonic, ConsDeltaSlow, ConsDeltaFast, DeltaTheta

%indexes:
CTB = celltype('CTB');
CTT = celltype('CTT');
CDS = celltype('CDS');
CDF = celltype('CDF');
DT_ = celltype('DT_');

for it = 1:length(inx)-1
    cellTypes{it, 3} = CTB(ismember(CTB, inx(it):inx(it+1)-1));
    cellTypes{it, 4} = CTT(ismember(CTT, inx(it):inx(it+1)-1));
    cellTypes{it, 5} = CDS(ismember(CDS, inx(it):inx(it+1)-1));
    cellTypes{it, 6} = CDF(ismember(CDF, inx(it):inx(it+1)-1));
    cellTypes{it, 7} = DT_(ismember(DT_, inx(it):inx(it+1)-1));
end

% make every possible intergroup pairs:
grouppairs = nchoosek([3, 4, 5, 6, 7], 2);

for it0 = 1 : size(grouppairs, 1)
    for it = 1:size(cellTypes, 1) %records
        animal = cellTypes{it, 1};
        recording = ['20100' num2str(cellTypes{it, 2})];
        load(fullfile(RESULTDIR, 'tsccg', 'theta_segments', recording), 'theta');
        if length(cellTypes{it, grouppairs(it0, 1)})>0 & length(cellTypes{it, grouppairs(it0, 2)})%more than one cell in that record in that group
            for it3 = 1:length(cellTypes{it, grouppairs(it0, 1)})
                for it4 = 1:length(cellTypes{it, grouppairs(it0, 2)})
                    shankId1 = allCell(cellTypes{it, grouppairs(it0, 1)}(it3), mO('shankId'));
                    shankId2 = allCell(cellTypes{it, grouppairs(it0, 2)}(it4), mO('shankId'));
                    cellId1 = allCell(cellTypes{it, grouppairs(it0, 1)}(it3), mO('cellId'));
                    cellId2 = allCell(cellTypes{it, grouppairs(it0, 2)}(it4), mO('cellId'));
                    str = [char(strcat(groupTable(grouppairs(it0, 1)-2), groupTable(grouppairs(it0, 2)-2)))];
                    status4 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'figures', str));
                    status5 = mkdir(fullfile(RESULTDIR, 'ccg_calculations', [num2str(maxlag) 'ms'], 'ccgs', str));
                    ccg_calculations(theta, animal, recording, shankId1, shankId2, cellId1, cellId2, maxlag, xBinS, issave, str);
                    %legend(groupTable{grouppairs(it0, 1)-2}, groupTable{grouppairs(it0, 2)-2});
                    close all;
                end
            end
        end
    end
end

end
function cmprRhtmIndices(segm, issave)
%CMPRRHTMINDICES(SEGM, ISSAVE) compares all cell's rhythmicities during
%delta and delta.
%   SEGM: which acgs to display ('theta' or 'delta')
%   ISSAVE: save?. 
%
%   See also MSHCsp, DATA_LOADER, LOAD_RANDPOISSON.

%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global NSR
global CGWINDOW
resultdir = fullfile(RESULTDIR, 'MSsync');

% Input arguments
if nargin == 0
    segm = 'theta';
    issave = 0;
end

% Load data table
load(fullfile(resultdir, 'data_loader', 'allCell.mat'));

% Load map for allCell matrix (mO):
load(fullfile(resultdir, 'data_loader', 'mapObj.mat'));

% Thresholds
if strcmp(segm, 'delta')
    load(fullfile(resultdir, 'load_randpoisson', 'duringDeltaParams.mat'));
    th1 = threshold1;
    th2 = threshold2;
    ThParCol = mO('DeAcgThInx'); % column of theta index
    DeParCol = mO('DeAcgDeInx'); % column of delta index
    sumacrcol = mO('desumacr'); % column of ACG integral
    numAPcol = mO('numApDelta'); % column of number of APs
    ThLengthcol = mO('deltaLength'); % column of segment length
    acgInx = mO('deltaAcg'); %starting column and last column of ACG
end
if strcmp(segm, 'theta')
    load(fullfile(resultdir, 'load_randpoisson', 'duringThetaParams.mat'));
    th1 = threshold1;
    th2 = threshold2;
    ThParCol = mO('ThAcgThInx'); % column of theta index
    DeParCol = mO('ThAcgDeInx'); % column of delta index
    sumacrcol = mO('thsumacr'); % column of ACG integral
    numAPcol = mO('numApTheta'); % column of number of APs
    ThLengthcol = mO('thetaLength'); % column of segment length
    acgInx = mO('thetaAcg'); % starting column and last column of ACG
end
le = acgInx(2)-acgInx(1)+1; % ACG length

%(USE THE SAME AS USED IN LOAD_RANDPOISSON()!!!)
frtresh = 0.0004; % mean frequency threshold
Sumacr = 2000; % ACG integral threshold

% Plot
sortedCell1 = allCell;
sortedCell1(sortedCell1(:, sumacrcol)<Sumacr | sortedCell1(:, numAPcol)./sortedCell1(:, ThLengthcol)<frtresh,:) = [];
[sortedCell1, sortinxes1] = sortrows(sortedCell1, ThParCol);

figure;
subplot(1, 3, 1);
a1 = gca;
imagesc(sortedCell1(:,acgInx(1):acgInx(2)));
set(gca,'clim',[-8 4]);
% title(['sorted, based on ThAcgThEnergy, thresh: ' num2str(ThEnth)])
dist = sortedCell1(:, ThParCol) - th1;
ix = length(dist(dist<=0));
text(zeros(1, size(sortedCell1, 1)), [1:size(sortedCell1, 1)], num2str(sortedCell1(:, 2:4)));
text(ones(1, size(sortedCell1, 1))*(le-100), [1:size(sortedCell1, 1)], num2str(sortedCell1(:, ThParCol)));
title('Theta index');
% text(ones(1, size(names, 1))*650, [1:size(names, 1)], num2str(sorted_numAp_s), 'Color', 'red');
hold on; plot([0, le], [ix+0.5,ix+0.5], 'r');
xticks = [1, le/6, 2*le/6, 5*le/12, 3*le/6, 7*le/12, 4*le/6, 5*le/6, le];
windowms = NSR*CGWINDOW;  % window in msec
xlabels = {-windowms -windowms*2/3 -windowms/3 -windowms/6 0 windowms/6 windowms/3 windowms*2/3 windowms};
set(gca,'xtick',xticks);
set(gca,'xticklabel',xlabels)
xlabel('msec')

[sortedCell2, sortinxes2] = sortrows(sortedCell1, DeParCol);

subplot(1, 3, 3);
a2 = gca;
imagesc(sortedCell2(:,acgInx(1):acgInx(2)));
set(gca,'clim',[-8 4]);
% title(['sorted, based on ThAcgDeEnergy, thresh: ' num2str(DeEnth)])
dist = sortedCell2(:, DeParCol)-th2;
ix = length(dist(dist<=0));
text(zeros(1, size(sortedCell2, 1)), [1:size(sortedCell2, 1)], num2str(sortedCell2(:, 2:4)));
text(ones(1, size(sortedCell2, 1))*(le-100), [1:size(sortedCell2, 1)], num2str(sortedCell2(:, DeParCol)));
title('Delta index');
% text(ones(1, size(names, 1))*450, [1:size(names, 1)], num2str(sorted_numAp_s), 'Color', 'red');
hold on; plot([0, le], [ix+0.5,ix+0.5], 'r');
xticks = [1, le/6, 2*le/6, 5*le/12, 3*le/6, 7*le/12, 4*le/6, 5*le/6, le];
windowms = NSR*CGWINDOW;  % window in msec
xlabels = {-windowms -windowms*2/3 -windowms/3 -windowms/6 0 windowms/6 windowms/3 windowms*2/3 windowms};
set(gca,'xtick',xticks);
set(gca,'xticklabel',xlabels)
xlabel('msec')

x1 = ones(1, length(sortinxes1(:, 1)));
x2 = ones(1, length(sortinxes2(:, 1)))*le;
subplot(1, 3, 2);
a3 = gca;
set(a3,'Ydir','reverse');
line([x1; x2], [sortinxes2'; [1:length(sortinxes1)]]);

text(ones(1, size(sortinxes1, 1))*2, sortinxes2, num2str(abs(flipud([1:length(sortinxes1)])-sortinxes2')'));
linkaxes([a1, a2, a3]);

% Save
if issave
    rdr = fullfile(resultdir, 'compareTechniques');
    if ~isdir(rdr)
        mkdir(rdr);
    end
    savefig(fullfile(resultdir, 'compareTechniques', segm));
    
    % Save sorted allCell matrices
    save(fullfile(resultdir, 'compareTechniques', [segm, '_ThsortedAllCell']), 'sortedCell1');
    save(fullfile(resultdir, 'compareTechniques', [segm, '_DesortedAllCell']), 'sortedCell2');
    close all
end

end
function output = randpoisson_process_shank(animal, recording)
%RANDPOISSON_PROCESS_SHANK(ANIMAL, RECORDING) wrapper for RANDPOISSON_PROCESS()
%   The function iterate trough each shank.
%   RECORD (file name)
%   ANIMAL (animal Id)
%
%   See also MSHCsp, RANDPOISSON_PROCESS_SHANK_CALLER, CELL_FEATURES_SHANK, RANDPOISSON_PROCESS, RANDPOISSON, LOAD_RANDPOISSON.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global DATADIR
output = [];

for shankId =1:4
    if exist (fullfile(DATADIR, ['viktor'  num2str(animal)], [recording '.clu.' num2str(shankId)]), 'file') == 2
        clu = load(fullfile(DATADIR, ['viktor'  num2str(animal)],[recording '.clu.' num2str(shankId)]));
        for cellId = 2:clu(1) % clu(1) is the number of cells on the actual shank
            [outputs, chosen_cell1] = randpoisson_process(animal, recording, shankId, cellId);
            if ~isnan(outputs(1))
                output = [output; [chosen_cell1, outputs]];
            end
        end
    end
end
end
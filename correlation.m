function [normLrCor, sumacr, lrLag] = correlation(timeseries1, timeseries2, display, maxlag, binsize)
% CORRELATION(timeseries1, timeseries2, DISPLAY, MAXLAG, BINSIZE) calculating crosscorrelation of timeseries1 and timeseries2,
% autocorrelation (if timeseries1 and timeseries2 are the same). 
%   MAXLAG is the CGWINDOW
%   BINSIZE: bin size, DISPLAY: controlls wheter to plot ccgs or
%   not.
%   Smooth, normalize and lowpass filter it.
%   TIMESERIEs are timeseries of cells firing patterns.
%   timeseries1 contains 1 where cell fires, 0
%   where not
%   DISPLAY: whether to display results or not
%
%   See also MSHCsp, TSCCG, CELL_FEATURES_SHANK_CALLER, CELL_FEATURES_SHANK, CELL_FEATURES, THETAINDEX, DELTAINDEX.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global NSR
global CGWINDOW
global CGBINS

% Input arguments
if nargin < 4
    maxlag = CGWINDOW*NSR;
    binsize = CGBINS;
end

% Cross-correlation
[cor,lag] = xcorr(timeseries1, timeseries2, maxlag);

% Discard zero lag (avoiding very big peak at zero lag) if autocorrelation
if isequal(timeseries1,timeseries2)
    cor(lag==0) = [];
    lag(lag==0) = [];
end

% Make lower resolution correlograms (smoothing by binning):
if mod(length(cor)/binsize, 1) == 0
    binInx = reshape(repmat((1:1:length(cor)/binsize)', [1, binsize])', [1, length(cor)])';
else
    binInx = reshape(repmat((1:1:length(cor)/binsize)', [1, binsize])', [1, length(cor)-1])';
    binInx = [binInx(1:length(binInx)/2); binInx(length(binInx)/2-1); binInx(length(binInx)/2+1:end)];
end
lrCor = accumarray(binInx, cor);
lrLag = lag((round(binsize/2):binsize:end));

% Return if no correlation found
if sum(lrCor)==0
    disp('sum(lrCor)=0, so return')
    normLrCor = lrCor;
    sumacr = 0;
    return
end

% Normalize CCGs (Z-score)
sumacr = sum(lrCor); %integrate
normLrCor = zscore(lrCor); % / sumacr; %we could normalize with sumacr

% % Lowpass filter acgs under 20 Hz
% if binsize < 20
%     coeff1 = fir1(32, 0.2, 'low');
%     normLrCor = filtfilt(coeff1, 1, normLrCor);
% end

% Plot
if display
    % bar(lrLag,normLrCor,'FaceColor','black')
    plot(lrLag, normLrCor);
end

% % Smooth by moving average
% normLrCor = smooth(normLrCor,'linear',3);
% 
% % Plot
% if display
%     hold on
%     plot(lrLag, normLrCor,'LineWidth',3);
% end

end
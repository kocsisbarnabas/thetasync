function msz_tren
%MSZ_TREN   Calculates Z-shif, MI, transfer entropy for the total length of
%longest theta and nontheta segments, and for 1 second windows.
%
%   See also MSPREADEEG, B_ZSHIFTRUN, TRENRUN, TRTRENRUN.

% Input argument check
error(nargchk(0,0,nargin))

% Directories
global DATAPATH
global DATADIR
ratno = '4';
inpdir = [DATADIR 'MSHCsp\Viktor' ratno '\'];
thetadir = [DATAPATH 'MSHCsp\Wavelet\theta_segments\'];
nonthetadir = [DATAPATH 'MSHCsp\Wavelet\nontheta_segments\'];
tedir = [DATAPATH 'MSHCsp\TE\Viktor' ratno '\'];
resdir = [DATAPATH 'MSHCsp\Zshift\Viktor' ratno '\'];
resdir2 = [DATAPATH 'MSHCsp\trTE\Viktor' ratno '\'];
mm = pwd;

% Filelist
switch  ratno
    case '4'
        rid = '1234';
        flist = {'201003171' '201003172' '201003173' '201003174'};
    case '5'
        rid = '1237'; %Viktor5's files
        flist = {'201003291' '201003292' '201003293' '201003294'};
end
sf = length(flist);

% Main
sr = 20000;    % sampling rate
dsr = 1000;    % resample at 1000 Hz
const = sr / dsr;
Inx = [33:65];
for o = 3:sf

    % Load EEG
    fname = flist{o};
    ff = [thetadir 'THETA_SEGMENTS_' rid '_' fname '.mat'];     % load theta segments
    load(ff)
    ThetaSegments = uniteseg(ThetaSegments,sr);     % drop gaps < 0.5 s
    ThetaSegments = short_killer(ThetaSegments);    % drop segments < 3 s
    ThetaSegments =  round(ThetaSegments/const);    % downsample on 'dsr'
    lent = ThetaSegments(2,:) - ThetaSegments(1,:);     % theta segment length
    minx = find(lent==max(lent));   % find the longest theta segment
    th1 = ThetaSegments(1,minx);
    th2 = ThetaSegments(2,minx);

    ff = [nonthetadir 'NONTHETA_SEGMENTS_' rid '_' fname '.mat'];     % load non-theta segments
    load(ff)
    NonThetaSegments = short_killer(NonThetaSegments);    % drop segments < 3 s
    NonThetaSegments =  round(NonThetaSegments/const);    % downsample on 'dsr'
    lenn = NonThetaSegments(2,:) - NonThetaSegments(1,:);     % length of non-theta segments
    minx = find(lenn==max(lenn));   % find the longest non-theta segment
    nth1 = NonThetaSegments(1,minx);
    nth2 = NonThetaSegments(2,minx);

    ff = [inpdir flist{o} '.par'];  % load channel settings
    pars = LoadPar(ff);
    pchs = cellfun(@(x) x',pars.ElecGp,'UniformOutput',0);
    chs = cell2mat(pchs);

    fnam = [inpdir flist{o} '.dat'];    % load EEG
    numch = pars.nChannels;
    buffersize = 2^14;
    fileinfo = dir(fnam);  % get file size and calculate the number of samples per channel
    flen = ceil(fileinfo(1).bytes/2/numch);
    numelmax = flen;
    %     numelmin = flen - 1000000;
    numelmin = 0;
    datafile = fopen(fnam,'r');
    eeg = zeros(numelmax-numelmin+1,1);

    ff = [inpdir 'EEG\' rid '_' flist{o} '_eeg.mat'];
    load(ff)
%     for ch = Inx % get the eeg channel
% 
%         numel = 0;
%         fseek(datafile,numelmin*numch*2,'bof');
%         while numelmin + numel < numelmax
%             [data,count] = fread(datafile,[numch,buffersize],'int16');
%             numelm = count / numch;
%             chselect = chs(ch) + 1;
%             eeg(numel+1:numel+numelm) = (data(chselect,:))';
% 
%             numel = numel + numelm;
%         end

            ch = 60;

        % Resample & filter EEG
        eeg = eeg(1:const:size(eeg,1));     % downsample at 'dsr'
        flt = fir1(1024, [2/dsr*2 8/dsr*2], 'bandpass'); % filter at theta 2-8 Hz
        feeg = filtfilt(flt,1,eeg);
        pheeg = angle(hilbert(feeg));
        % figure; plot(eeg)

        % Load unit
        for shankno = 1:4
            ff = [inpdir fname '.res.' num2str(shankno)];       % load unit
            res = textread(ff);     % spike timestamps
            ff = [inpdir fname '.clu.' num2str(shankno)];
            %         if ~b_isfilename(ff)
            %             continue
            %         end
            clu = textread(ff);     % indices of sorted cells
            clu = clu(2:end);
            ncells = max(clu);        % number of sorted cells (including multiunit)

            for nc = 2:ncells
                vdisc = res(clu==nc)';
                vdisc = round(vdisc/const);  % resample at 'dsr'
                vdisc_theta = vdisc(vdisc>th1&vdisc<th2);
                vdisc_noth = vdisc(vdisc>nth1&vdisc<nth2);

                % Z-SHIFT
                % Run 'zshift' on longest theta interval
                if ~isempty(vdisc_theta)
                    ahee_input = pheeg(th1:th2);
                    vdisc_input = vdisc_theta - th1 + 1;
                    H = figure;
                    if ~isempty(vdisc_theta)
                        [ThetaZMaxLoc,ThetaZMax,ThetaAng,ThetaMvl] = zshift(ahee_input,vdisc_input);
                    end
                    ThetaKappa = A1inv(ThetaMvl);

                    fns = [resdir rid '_' fname '_' num2str(ch) '_' num2str(shankno) '_' num2str(nc) '_ZShift.fig' ];
                    saveas(H, fns)      % save
                    fns = [resdir rid '_' fname '_' num2str(ch) '_' num2str(shankno) '_' num2str(nc) '_ZShift.mat' ];
                    save(fns,'ThetaZMaxLoc','ThetaZMax','ThetaAng','ThetaMvl','ThetaKappa')
                end

                % TRANSFER ENTROPY
                %load sig.lev. & optimal tau
                ff = [tedir rid '_' flist{o} '_' num2str(ch) '_' num2str(shankno) '_' num2str(nc) '_TEsign.mat'];
                load(ff)
                ff = [tedir rid '_' flist{o} '_' num2str(ch) '_' num2str(shankno) '_' num2str(nc) '_mTE.mat'];
                load(ff)
                
                %Create new data (attach theta and non-theta segment)
                eeg_theta = feeg(th1:th2)';
                eeg_noth = feeg(nth1:nth2)';
                eeg_new = [eeg_noth eeg_theta];
                vdisc_new = [vdisc_noth-nth1 vdisc_theta-th1+nth2-nth1];
                seglen_theta = th2 - th1;
                seglen_noth = nth2 - nth1;
                seglen_new = seglen_theta + seglen_noth;

                % Create random unit (shuffle ISIs)
                [psvd_theta psvd_noth] = isi_shuffle(vdisc_theta,vdisc_noth);
                psvd_new = [psvd_noth-nth1 psvd_theta-th1+nth2-nth1];

                % Wavelet transformation of the EEG
                abs1 = eeg_wavelet(eeg_new);

                % Sinc convolution & wavelet transformation of unit
                abs2 = unit_wavelet(vdisc_new,seglen_new,dsr,dsr);   % unit
                abs3 = unit_wavelet(psvd_new,seglen_new,dsr,dsr);    % random unit

                if ~isequal(size(abs1,2),size(abs2,2))    % adjust size
                    mininx = min([size(abs1,2) size(abs2,2)]);
                    abs1 = abs1(:,1:mininx);
                    abs2 = abs2(:,1:mininx);
                    abs3 = abs3(:,1:mininx);
                end

                % Transfer entropy
                min_abs1 = min(abs1(:));    % calculate extrema
                max_abs1 = max(abs1(:));
                abs1_theta = abs1(:,nth2-nth1+1:end);
                abs1_noth = abs1(:,1:nth2-nth1);
                min_abs2 = min(abs2(:));
                max_abs2 = max(abs2(:));
                abs2_theta = abs2(:,nth2-nth1+1:end);
                abs2_noth = abs2(:,1:nth2-nth1);
                min_abs3 = min(abs3(:));
                max_abs3 = max(abs3(:));
                abs3_theta = abs3(:,nth2-nth1+1:end);
                abs3_noth = abs3(:,1:nth2-nth1);
                next = 1;
                [k1 k2] = size(abs1_theta);
                winlen = 1 * dsr;   % window size
                maxi = floor(k2/winlen);
                ovlp = 1;
                tau = mtau_theta;
                mmax = maxi * ovlp - ovlp + 1;
                TE_theta_eu = zeros(1,mmax);
                TE_theta_eu_bias = zeros(1,mmax);
                TE_theta_eu_shuffled = zeros(1,mmax);
                TE_theta_eu_shuffled_bias = zeros(1,mmax);
                NTE_theta_eu = zeros(1,mmax);
                TE_theta_ue = zeros(1,mmax);
                TE_theta_ue_bias = zeros(1,mmax);
                TE_theta_ue_shuffled = zeros(1,mmax);
                TE_theta_ue_shuffled_bias = zeros(1,mmax);
                NTE_theta_ue = zeros(1,mmax);
                for i = 1:mmax        % THETA LOOP
                    inx1 = (i - 1) * winlen / ovlp + 1;  % Note: overlaping windows!
                    inx1 = round(inx1);
                    inx2 = inx1 + winlen - 1;
                    [TE TE_corr TE_bias H_X2FcX2] = ltren(abs1_theta(:,inx1:inx2),abs2_theta(:,inx1:inx2),...
                        min_abs1,max_abs1,min_abs2,max_abs2,1000,tau);  % theta, EEG->unit
                    TE_theta_eu(i) = TE;
                    TE_theta_eu_bias(i) = TE_bias;
                    [TE TE_corr TE_bias] = ltren(abs1_theta(:,inx1:inx2),abs3_theta(:,inx1:inx2),...
                        min_abs1,max_abs1,min_abs3,max_abs3,1000,tau);
                    TE_theta_eu_shuffled(i) = TE;
                    TE_theta_eu_shuffled_bias(i) = TE_bias;
                    NTE_theta_eu(i) = (TE_theta_eu(i) - TE_theta_eu_shuffled(i)) / H_X2FcX2;
                    NTE_theta_eu(i) = max(NTE_theta_eu(i),0);

                    [TE TE_corr TE_bias H_X2FcX2] = ltren(abs2_theta(:,inx1:inx2),abs1_theta(:,inx1:inx2),...
                        min_abs2,max_abs2,min_abs1,max_abs1,1000,tau);  % theta, unit->EEG
                    TE_theta_ue(i) = TE;
                    TE_theta_ue_bias(i) = TE_bias;
                    [TE TE_corr TE_bias] = ltren(abs3_theta(:,inx1:inx2),abs1_theta(:,inx1:inx2),...
                        min_abs3,max_abs3,min_abs1,max_abs1,1000,tau);
                    TE_theta_ue_shuffled(i) = TE;
                    TE_theta_ue_shuffled_bias(i) = TE_bias;
                    NTE_theta_ue(i) = (TE_theta_ue(i) - TE_theta_ue_shuffled(i)) / H_X2FcX2;
                    NTE_theta_ue(i) = max(NTE_theta_ue(i),0);

                    next = next + 1;
                end
                DF_theta_eu = (NTE_theta_eu - NTE_theta_ue) ./ (NTE_theta_eu + NTE_theta_ue);    % direction of flow (pos: EEG->unit)
                DIF_theta_eu = (NTE_theta_eu - NTE_theta_ue);

                [k1 k2] = size(abs1_noth);
                maxi = floor(k2/winlen);
                tau = mtau_noth;
                mmax = maxi * ovlp - ovlp + 1;
                TE_noth_eu = zeros(1,mmax);
                TE_noth_eu_bias = zeros(1,mmax);
                TE_noth_eu_shuffled = zeros(1,mmax);
                TE_noth_eu_shuffled_bias = zeros(1,mmax);
                NTE_noth_eu = zeros(1,mmax);
                TE_noth_ue = zeros(1,mmax);
                TE_noth_ue_bias = zeros(1,mmax);
                TE_noth_ue_shuffled = zeros(1,mmax);
                TE_noth_ue_shuffled_bias = zeros(1,mmax);
                NTE_noth_ue = zeros(1,mmax);
                for i = 1:mmax        % NON-THETA LOOP
                    inx1 = (i - 1) * winlen / ovlp + 1;  % Note: overlaping windows!
                    inx1 = round(inx1);
                    inx2 = inx1 + winlen - 1;
                    [TE TE_corr TE_bias H_X2FcX2] = ltren(abs1_noth(:,inx1:inx2),abs2_noth(:,inx1:inx2),...
                        min_abs1,max_abs1,min_abs2,max_abs2,1000,tau);  % non-theta, EEG->unit
                    TE_noth_eu(i) = TE;
                    TE_noth_eu_bias(i) = TE_bias;
                    [TE TE_corr TE_bias] = ltren(abs1_noth(:,inx1:inx2),abs3_noth(:,inx1:inx2),...
                        min_abs1,max_abs1,min_abs3,max_abs3,1000,tau);
                    TE_noth_eu_shuffled(i) = TE;
                    TE_noth_eu_shuffled_bias(i) = TE_bias;
                    NTE_noth_eu(i) = (TE_noth_eu(i) - TE_noth_eu_shuffled(i)) / H_X2FcX2;
                    NTE_noth_eu(i) = max(NTE_noth_eu(i),0);

                    [TE TE_corr TE_bias H_X2FcX2] = ltren(abs2_noth(:,inx1:inx2),abs1_noth(:,inx1:inx2),...
                        min_abs2,max_abs2,min_abs1,max_abs1,1000,tau);  % non-theta, unit->EEG
                    TE_noth_ue(i) = TE;
                    TE_noth_ue_bias(i) = TE_bias;
                    [TE TE_corr TE_bias] = ltren(abs3_noth(:,inx1:inx2),abs1_noth(:,inx1:inx2),...
                        min_abs3,max_abs3,min_abs1,max_abs1,1000,tau);
                    TE_noth_ue_shuffled(i) = TE;
                    TE_noth_ue_shuffled_bias(i) = TE_bias;
                    NTE_noth_ue(i) = (TE_noth_ue(i) - TE_noth_ue_shuffled(i)) / H_X2FcX2;
                    NTE_noth_ue(i) = max(NTE_noth_ue(i),0);

                    next = next + 1;
                end
                DF_noth_eu = (NTE_noth_eu - NTE_noth_ue) ./ (NTE_noth_eu + NTE_noth_ue);
                DIF_noth_eu = (NTE_noth_eu - NTE_noth_ue);

                % Significance
                sTE_noth_eu5 = sort(sTE_noth_eu, 'descend');
                sTE_noth_eu5 = sTE_noth_eu5(5);
                sTE_noth_ue5 = sort(sTE_noth_ue, 'descend');
                sTE_noth_ue5 = sTE_noth_ue5(5);
                sTE_theta_eu5 = sort(sTE_theta_eu, 'descend');
                sTE_theta_eu5 = sTE_theta_eu5(5);
                sTE_theta_ue5 = sort(sTE_theta_ue, 'descend');
                sTE_theta_ue5 = sTE_theta_ue5(5);
                sigthetaeu = find(TE_theta_eu >= sTE_theta_eu5);
                sigthetaue = find(TE_theta_ue >= sTE_theta_ue5);
                signotheu = find(TE_noth_eu >= sTE_noth_eu5);
                signothue = find(TE_noth_ue >= sTE_noth_ue5);
                
                % Plot and save
                filenam = [resdir2 rid '_' fname '_' num2str(ch) '_' num2str(shankno) '_' num2str(nc)];
                H1 = figure;
                plot(DF_theta_eu,'r')
                hold on                
                plot(sigthetaeu, DF_theta_eu(sigthetaeu), 'o', 'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                title('DF theta EEG->unit')
                hold off                
                ffn = [filenam '_trDFtheta.fig'];
                saveas(H1,ffn)
                H2 = figure;
                plot(DF_noth_eu)
                hold on
                plot(signotheu, DF_noth_eu(signotheu),  'o', 'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                title('DF noth EEG->unit')
                hold off
                ffn = [filenam '_trDFnoth.fig'];
                saveas(H2,ffn)
                H1b = figure;
                plot(DIF_theta_eu,'r')
                title('DIF theta EEG->unit')
                ffn = [filenam '_trDIFtheta.fig'];
                saveas(H1b,ffn)
                H2b = figure;
                plot(DIF_noth_eu)
                title('DIF non-theta EEG->unit')
                ffn = [filenam  '_trDIFnoth.fig'];
                saveas(H2b,ffn)

                H1c = figure;
                plot(NTE_theta_eu)
                hold on 
                plot(sigthetaeu, NTE_theta_eu(sigthetaeu), 'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                plot(NTE_theta_ue, 'r'); 
                plot(sigthetaue, NTE_theta_ue(sigthetaue), 'o',...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                hold off
                title('NTE theta eu - blue, ue - red')
                ffn = [filenam '_trNTEtheta.fig'];
                saveas(H1c,ffn)
                H2c = figure;                
                plot(NTE_noth_eu)
                hold on
                plot(NTE_noth_ue, 'r')
                plot(signotheu, NTE_noth_eu(signotheu),  'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)                
                plot(signothue, NTE_noth_ue(signothue),  'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                hold off
                title('NTE non-theta eu - blue, ue - red')
                ffn = [filenam '_trNTEnoth.fig'];
                saveas(H2c,ffn)
                
                H1d = figure;                
                plot(TE_theta_eu)
                hold on 
                plot(sigthetaeu, TE_theta_eu(sigthetaeu), 'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                plot(TE_theta_ue, 'r'); 
                plot(sigthetaue, TE_theta_ue(sigthetaue), 'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                hold off
                title('TE theta eu - blue, ue - red')
                ffn = [filenam '_trTEtheta.fig'];
                saveas(H1d,ffn)
                H2d = figure;               
                plot(TE_noth_eu)
                hold on
                plot(TE_noth_ue, 'r')
                plot(signotheu, TE_noth_eu(signotheu), 'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)                
                plot(signothue, TE_noth_ue(signothue), 'o', ...
                    'MarkerFaceColor',[.49 1 .63],'MarkerSize', 8)
                hold off
                title('TE non-theta eu - blue, ue - red')
                ffn = [filenam '_trTEnoth.fig'];
                saveas(H2d,ffn)
                mfn = [resdir2 rid '_' fname '_' num2str(ch) '_' num2str(shankno) '_' ...
                    num2str(nc) '_trTE.mat'];
                save(mfn,'DF_theta_eu','DF_noth_eu','TE_theta_eu','TE_theta_ue','TE_noth_eu','TE_noth_ue',...
                    'NTE_theta_eu','NTE_theta_ue','NTE_noth_eu','NTE_noth_ue',...
                    'TE_theta_eu_bias','TE_theta_eu_shuffled','TE_theta_eu_shuffled_bias',...
                    'TE_theta_ue_bias','TE_theta_ue_shuffled','TE_theta_ue_shuffled_bias',...
                    'TE_noth_eu_bias','TE_noth_eu_shuffled','TE_noth_eu_shuffled_bias',...
                    'TE_noth_ue_bias','TE_noth_ue_shuffled','TE_noth_ue_shuffled_bias');
                close all
                clear abs1 abs2 abs3
                cd(mm)
            end
        end
%     end
end


fclose(datafile);
%     eeg = eeg{60};
%     fn = [resdir rid '_' flist{o} '_eeg.mat'];
%     save(fn,'eeg')
%
% end

% END OF MSZ_TREN


% -------------------------------------------------------------------------
% SEGMENT SELECTION
% -------------------------------------------------------------------------
function segments2 = uniteseg(segments,sr)

len = size(segments,2);
segments2 = segments;
for k = 1:len-1
    la = segments(1,k+1);
    fi = segments(2,k);
    if (la-fi)/sr < 0.5
        [fnx fny] = find(segments2==fi);
        segments2(fnx,fny) = segments(2,k+1);
        segments2 = [segments2(1:2,1:fny) segments2(1:2,fny+2:end)];
    end
end



% ----------------------------------------------------------------------------------
function segments = short_killer(segments)

% Skip short segments
int = segments;
int1 = int(1,:);
int2 = int(2,:);
difint = int2 - int1;
fd = find(difint<30000);         % leaving segments shorter than 3 sec.
int1(fd) = [];
int2(fd) = [];
segments = [int1; int2];



% ----------------------------------------------------------------------------------
% Z-SHIFT
% ----------------------------------------------------------------------------------
function [zmaxloc,zmax,hang,hmvl] = zshift(ahee,vdisc)

% Phase angles - Hilbert
leeg = length(ahee);
bahee = ahee(vdisc(vdisc>0&vdisc<leeg));
n = length(bahee);
ftm = sum(exp(1).^(i*bahee)) / n;    % first trigonometric moment
hang = angle(ftm);   % mean angle
hmvl = abs(ftm);     % mean resultant length

% Shift unit
T = -1000:1:1000;
p = zeros(size(T));
Z = zeros(size(T));
next = 0;    % counter
for t = T
    next = next + 1;
    vt = vdisc(vdisc+t>0&vdisc+t<leeg) + t;  % shift unit
    if isempty(vt)      % Skip segment, if all spikes fall out of range after unit shift
        zmaxloc = NaN;
        zmax = NaN;
        plot(0,0)
        text(0,0,'All spikes out of range after unit shift.','Color','red',...
            'HorizontalAlignment','center');
        return
    end
    bang = ahee(vt);

    % Mean resultant length
    n = length(bang);
    ftm = sum(exp(1).^(i*bang)) / n;    % first trigonometric moment
    mrl = abs(ftm);     % mean resultant length
    z = n * (mrl ^ 2);  % Rayleigh's Z statistic
    Z(next) = z;
    p(next) = exp(1) ^ (-1 * z) * (1 + (2 * z - z ^ 2) / ...
        (4 * n) - (24 * z - 132 * z ^ 2 + 76 * z ^ 3 - 9 * z ^ 4) / (288 * n ^ 2));
end

% Plot result
plot(T,Z)

sl = 0.005;  % level of significance
siglev = -1 * log(sl);
hold on
line(xlim,[siglev siglev],'Color','k','LineStyle','--');

zmax = max(Z);
zmaxloc = find(Z==zmax);
x_lim = xlim;
y_lim = ylim;
text(x_lim(1)+2*(x_lim(2)-x_lim(1))/3,y_lim(1)+2*(y_lim(2)-y_lim(1))/3,num2str(T(zmaxloc(1))));
hold off



% -------------------------------------------------------------------------
% RANDOM UNIT
% -------------------------------------------------------------------------
function [psvd_theta psvd_noth] = isi_shuffle(vdisc_theta,vdisc_noth)

% Interspike intervals
isi_theta = diff(vdisc_theta);
isi_noth = diff(vdisc_noth);

% Randomize noth segment
lit = length(isi_theta);    % shuffle theta ISIs
rp = randperm(lit);
while any(rp==(1:lit))
    rp = randperm(lit);
end
psi1 = [];
for it = 1:lit
    psi1 = [psi1 isi_theta(rp(it))];
end
psvd_theta = [vdisc_theta(1) vdisc_theta(1)+cumsum(psi1)];

% Randomize non-theta segment
lin = length(isi_noth);     % shuffle non-theta ISIs
rp = randperm(lin);
while any(rp==(1:lin))
    rp = randperm(lin);
end
psi2 = [];
for in = 1:lin
    psi2 = [psi2 isi_noth(rp(in))];
end
psvd_noth = [vdisc_noth(1) vdisc_noth(1)+cumsum(psi2)];




% -------------------------------------------------------------------------
% WAVELET
% -------------------------------------------------------------------------
function pow = eeg_wavelet(dat)

% Prepare for wavelet transformation
variance = std(dat) ^ 2;
dat = (dat - mean(dat)) / sqrt(variance) ;
n = length(dat);
dt = 1 / 1000;
pad = 1;
dj = 0.08;    
j1 = ceil((1/dj) * log2(n/2));
j1 = ceil(j1);
j = (0:j1);
s0 = 2 * dt; 
s = s0 .* 2 .^ (j * dj);
omega0 = 6;
c = 4 * pi / (omega0 + sqrt(2+omega0^2));
fperiod = c .* s;
f = 1 ./ fperiod;

fnd = find(f>6);    % frequency band bounderies
pwind1 = fnd(end);
fnd = find(f<2.5);
pwind2 = fnd(1);
f = f(pwind1:pwind2);
fperiod = 1 ./ f;
s = fperiod ./ c;

param = -1;
mother = 'Morlet';

% Wavelet transformation
[wave,period,scale,coi] = wavelet(dat,dt,pad,s,mother,param);
pow = abs(wave).^2;



% -------------------------------------------------------------------------
function pow = unit_wavelet(vdisc,lenu,sr,dsr)

% Sinc convolution
fs = sr;     % unit
dto = 1 / fs;
fcut = 100; 
fsnew = dsr;
dtnew = 1 / fsnew;
fsratio = fsnew / fs;
told = vdisc * dto * fcut;
tnew = (1:lenu*fsratio) * dtnew * fcut;
lentold = length(told);

tnew2 = (-lenu*fsratio:lenu*fsratio) * dtnew * fcut;
psinc = sinc(tnew2);
lt = length(tnew);
told2 = round(told/dtnew/fcut);
zint = zeros(1,lt);
for k = 1:lentold
    zint = zint + psinc(lt-told2(k)+2:2*lt-told2(k)+1);
end

% Prepare for wavelet transformation
variance = std(zint) ^ 2;
zint = (zint - mean(zint)) / sqrt(variance) ;
n = length(zint);
dt = 1 / dsr;
pad = 1;
dj = 0.08;    
j1 = ceil((1/dj) * log2(n/2));
j1 = ceil(j1);
j = (0:j1);
s0 = 2 * dt; 
s = s0 .* 2 .^ (j * dj);
omega0 = 6;
c = 4 * pi / (omega0 + sqrt(2+omega0^2));
fperiod = c .* s;
f = 1 ./ fperiod;

fnd = find(f>6);    % frequency band bounderies
pwind1 = fnd(end);
fnd = find(f<2.5);
pwind2 = fnd(1);
f = f(pwind1:pwind2);
fperiod = 1 ./ f;
s = fperiod ./ c;

param = -1;
mother = 'Morlet';

% Wavelet transformation
[wave,period,scale,coi] = wavelet(zint,dt,pad,s,mother,param);
pow = abs(wave).^2;



% -------------------------------------------------------------------------
function [wave,period,scale,coi] = wavelet(Y,dt,pad,s,mother,param)
%WAVELET   Wavelet with scales for every frequency.
%
%   Copyright (C) 1995-1998, Christopher Torrence and Gilbert P. Compo
%   University of Colorado, Program in Atmospheric and Oceanic Sciences.
%   This software may be used, copied, or redistributed as long as it is not
%   sold and this copyright notice is reproduced on each copy made.  This
%   routine is provided as is without any express or implied warranties
%   whatsoever.

n1 = length(Y);

% Construct time series to analyze, pad if necessary
x(1:n1) = Y - mean(Y);
if (pad == 1)
	base2 = fix(log(n1)/log(2)+0.4999);   % power of 2 nearest to N
	x = [x,zeros(1,2^(base2+1)-n1)];
end
n = length(x);

% Construct wavenumber array used in transform [Eqn(5)]
k = 1:fix(n/2);
k = k .* ((2 .* pi) / (n * dt));
k = [0., k, -k(fix((n-1)/2):-1:1)];

% Compute FFT of the (padded) time series
f = fft(x);    % [Eqn(3)]

% Construct SCALE array & empty PERIOD & WAVE arrays
scale = s;

% Loop through all scales and compute transform
wave = zeros(length(scale),n1);  % define the wavelet array
for a1 = 1:length(scale)
	[daughter,fourier_factor,coi,dofmin] = wave_bases(mother,k,scale(a1),param);	
	ifd = ifft(f.*daughter);  % wavelet transform[Eqn(4)]
    ifd = ifd(1:n1);
    wave(a1,:) = ifd;
end

period = fourier_factor * scale;
coi = coi * dt * [1E-5,1:((n1+1)/2-1),fliplr((1:(n1/2-1))),1E-5];  % COI [Sec.3g]



% -------------------------------------------------------------------------
% ENTROPY
% -------------------------------------------------------------------------
function [TE TE_corr TE_bias H_X2FcX2] = ltren(W1,W2,minW1,maxW1,minW2,maxW2,sr,tau)
%TREN   Transfer entropy.
%   TE = TREN(W1,W2,SR) calculates transfer entropy for time series W1 and
%   W2 sampled on SR (TE_W1->W2).
%
%   TE = TREN(W1,W2,SR,TAU) uses TAU ms as time lag between current and
%   "future" values.
%
%   [TE TE_CORR TE_BIAS] = TREN(W1,W2,SR,TAU) returns unbiased estimate of
%   transfer entropy applying Treves-Panzeri bias correction algorithm. The
%   amount of the bias of the original estimate is also returned.
%
%   [TE TE_CORR TE_BIAS H] = TREN(W1,W2,SR,TAU) returns H(X2F|X2)
%   conditonal entropy for transfer entropy normalization.
%
%   References:
%   (1) Panzeri S, Senatore R, Montemurro MA, Petersen RS (2007)
%   Correcting for the sampling bias problem in spike train information
%   measures. J Neurophysiol 98:1064-1072.
%   (2) Gourévitch B, Eggermont JJ (2007) Evaluating information transfer
%   between auditory cortical neurons. J Neurophysiol 97:2533-2543.
%   (3) Imas OA, Ropella KM, Ward BD, Wood JD, Hudetz AG (2005) Volatile
%   anesthetics disrupt frontal-posterior recurrent information transfer at
%   gamma frequencies in rat. Neurosci Lett 387:145-50.

% Input argument check
error(nargchk(7,8,nargin))
if nargin < 8
    tau = 100;   % time lag in ms
end    

% Extracting the random variables
tau2 = tau / 1000 * sr;     % time lag in data points
X1 = W1(:,1:end-tau2);
X2 = W2(:,1:end-tau2);
X2F = W2(:,tau2+1:end);
n = numel(X1);

% Calculating joint histograms
bno = fix(exp(0.626+0.4*log(n-1)));   % bin number for histogram estimates
bno = 30;
minX1 = minW1;    % discretization of X1
maxX1 = maxW1;
binwidth1 = (maxX1 - minX1) ./ bno;
xx1 = minX1 + binwidth1 * (0:bno);   % bin edges
xx1(length(xx1)) = maxX1;
xx1(1) = -inf;
nbin1 = length(xx1);

minX2 = minW2;    % discretization of X2
maxX2 = maxW2;
binwidth2 = (maxX2 - minX2) ./ bno;
xx2 = minX2 + binwidth2 * (0:bno);   % bin edges
xx2(length(xx2)) = maxX2;
xx2(1) = -inf;
nbin2 = length(xx2);

h_X2F_X2_X1 = zeros(nbin2-1,nbin2-1,nbin1-1);   % P(X2F, X2, X1)
t1 = X2F(:) - minX2;
t2 = X2(:) - minX2;
t3 = X1(:) - minX1;
p1 = fix((t1-1000000*eps)/binwidth2) + 1;
p2 = fix((t2-1000000*eps)/binwidth2) + 1;
p3 = fix((t3-1000000*eps)/binwidth1) + 1;
p1(p1>size(h_X2F_X2_X1,1)) = size(h_X2F_X2_X1,1);
p2(p2>size(h_X2F_X2_X1,1)) = size(h_X2F_X2_X1,1);
p3(p3>size(h_X2F_X2_X1,1)) = size(h_X2F_X2_X1,1);
h_X2F_X2_X1 = accumarray([p1 p2 p3],1,[nbin2-1 nbin2-1 nbin1-1]);   % 4-D histogram
h_X2F_X2_X1 = h_X2F_X2_X1 / sum(sum(sum(h_X2F_X2_X1)));      % normalization

% Calculating marginal histograms
h_X2_X1 = squeeze(sum(h_X2F_X2_X1,1));   % P(X2, X1)
h_X2F_X2 = squeeze(sum(h_X2F_X2_X1,3));   % P(X2F, X2)
h_X2 = squeeze(sum(h_X2_X1,2));          % P(X2)

% Calculating transfer entropy
% PaPb = h_X2F_X2_X1 .* repmat(h_X2',[nbin2-1 1 nbin1-1]);
% PcPd = permute(repmat(h_X2_X1,[1 1 nbin2-1]),[3 1 2]) .* repmat(h_X2F_X2,[1 1 nbin1-1]);
% pte = h_X2F_X2_X1 .* log2(PaPb./PcPd);
% pte = pte(:);
% TE = nansum(pte);

% Alternative calculation of transfer entropy (for bias correction purposes)
ph = h_X2F_X2 .* log2(h_X2F_X2);       % H(X2F, X2)
H_X2F_X2 = -nansum(ph(:));
ph = h_X2 .* log2(h_X2);       % H(X2F, X2)
H_X2 = -nansum(ph(:));
H_X2FcX2 = H_X2F_X2 - H_X2;     % H(X2F|X2)
ph = h_X2F_X2_X1 .* log2(h_X2F_X2_X1);    % H(X2F, X2, X1)
H_X2F_X2_X1 = -nansum(ph(:));
ph = h_X2_X1 .* log2(h_X2_X1);    % H(X2,X1)
H_X2_X1 = -nansum(ph(:));
H_X2FcX2_X1 = H_X2F_X2_X1 - H_X2_X1;    % H(X2F|X1,X2)
TE = H_X2FcX2 - H_X2FcX2_X1;

% Bias correction
Nt = n;   % total number of trials
Rs_bar = zeros(1,size(h_X2F_X2,2));
for k2 = 1:size(h_X2F_X2,2)
    Rs_bar(k2) = bayescount(Nt,h_X2F_X2(:,k2));
end
Bias_HRS = ((-1) / (2 * Nt * log(2))) * sum(Rs_bar-1);
H_X2FcX2_corr = H_X2FcX2 - Bias_HRS;

Nt = n;   % total number of trials
Rs_bar = zeros(size(h_X2F_X2_X1,2),size(h_X2F_X2_X1,3));
for k2 = 1:size(h_X2F_X2_X1,2)
    for k3 = 1:size(h_X2F_X2_X1,3)
        Rs_bar(k2,k3) = bayescount(Nt,h_X2F_X2_X1(:,k2,k3));
    end
end
Bias_HRS = ((-1) / (2 * Nt * log(2))) * sum(sum(Rs_bar-1));
H_X2FcX2_X1_corr = H_X2FcX2_X1 - Bias_HRS;

TE_corr = H_X2FcX2_corr - H_X2FcX2_X1_corr;
TE_bias = TE - TE_corr;


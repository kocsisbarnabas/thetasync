function cell_groups(str, issave)
%CELL_GROUPS   Assign septal cell groups based on theta-rhythmicity.
%   CELL_GROUPS(STR, ISSAVE) loads allCell matrix from DATA_LOADER() output
%   and creates cell groups according to their firing rhythmicity. 
%   STR: 3 letter ID of cell group (string, e.g.: 'CTB' -> constitutive
%       bursting theta cells) 
%   ISSAVE: save?
%
%   See also MSHCSP, DATA_LOADER, CELLTYPE, IMAGECCGS, CELL_FEATUES, LOAD_RADPOISSON, RANDPOISSON_PROCESS, .

%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

global RESULTDIR
global NSR
global CGWINDOW
global CGBINS
resultdir = fullfile(RESULTDIR, 'MSsync');

% Input arguments
if nargin == 0
    str = 'CTB';
    issave = 0;
end

% Find indices (in allCell matrix) of cells belonging to STR group
ex = celltype(str);

% Create folders
if issave
    rdr = fullfile(resultdir, 'cell_groups', str);
    if ~isdir(rdr)
        mkdir(rdr);
    end
end

% Load data table
load(fullfile(resultdir, 'data_loader', 'allCell.mat'));

% Load map for allCell matrix (mO):
load(fullfile(resultdir, 'data_loader', 'mapObj.mat'));

% Open and save individual plots of ACGs
for it = 1: size(ex, 1)
    openfig(fullfile(resultdir, 'cell_features', 'figures', ['20100' num2str(allCell(ex(it), mO('recordId'))) '_autocorr_of_cell_' num2str(allCell(ex(it), mO('cellId'))) '_shn' num2str(allCell(ex(it), mO('shankId'))) '.fig']));
    if issave
        savefig(fullfile(resultdir, 'cell_groups', str, ['20100' num2str(allCell(ex(it), mO('recordId'))) '_autocorr_of_cell_' num2str(allCell(ex(it), mO('cellId'))) '_shn' num2str(allCell(ex(it), mO('shankId'))) '.fig']));
        close all
    end
end

% Make image of all ACGs in the group
% Ticks and labels for displaying
le = CGWINDOW * NSR * 2 / CGBINS; % ACG length
xticks = [1, le/6, 2*le/6, 5*le/12, 3*le/6, 7*le/12, 4*le/6, 5*le/6, le];
windowms = NSR * CGWINDOW; % window in msec
xlabels = {-windowms -windowms*2/3 -windowms/3 -windowms/6 0 windowms/6 windowms/3 windowms*2/3 windowms};
imageccgs(sortrows(allCell(ex, :), mO('ThAcgThInx')), mO('thetaAcg'), mO('deltaAcg'), xticks, xlabels);

% Save
if issave
    savefig(fullfile(resultdir, 'cell_groups', str, 'image'));
    close all
end

end
function GUI_bandFilter(eeg, nsr)
%GUI_BANDFILTER(EEG, NSR) graphical interface for analysing anaesthetized
%mouse hippocampal unit activity.
%   Lower and upper theta and delta band thresholds can be adjusted in
%   pop-up window.
%   EEG input signal from one channel, NSR: sampling rate.
%
%   See also MSHCsp, TTK_MSPREADEEG_RW, TSCCG.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 16/06/2017

%default values:
thetaL = 2.5; % theta band lower threshold
thetaU = 6;
deltaL = 0.5;
deltaU = 2.5;


figure('Position', [500, 500, 300, 100]);
pos = get(gcf, 'Position');
width = pos(3);
height = pos(4);


text1 = uicontrol('Style', 'edit', 'String', thetaL,...
    'Position', [10 height-30 50 20],...
    'Callback', @thetaL_callback);
text2 = uicontrol('Style', 'edit', 'String', thetaU,...
    'Position', [70 height-30 50 20],...
    'Callback', @thetaU_callback);
text3 = uicontrol('Style', 'edit', 'String', deltaL,...
    'Position', [10 height-60 50 20],...
    'Callback', @deltaL_callback);
text4 = uicontrol('Style', 'edit', 'String', deltaU,...
    'Position', [70 height-60 50 20],...
    'Callback', @deltaU_callback);
statText1 = uicontrol('Style', 'text', 'String', 'theta band (Hz)',...
    'Position', [130 height-35 100 20]);
statText2 = uicontrol('Style', 'text', 'String', 'delta band (Hz)',...
    'Position', [130 height-65 100 20]);
btn1 = uicontrol('Style', 'pushbutton', 'String', 'Plot',...
    'Position', [10 height-90 50 20],...
    'Callback', @recalculate_callback);
 
    function thetaL_callback(hObject, eventdata, handles)
        input = str2double(get(hObject,'String'));
        if isnan(input)
            errordlg('You must enter a numeric value','Invalid Input','modal')
            uicontrol(hObject)
            return
        else
            thetaL = input;
        end
    end
    function thetaU_callback(hObject, eventdata, handles)
        input = str2double(get(hObject,'String'));
        if isnan(input)
            errordlg('You must enter a numeric value','Invalid Input','modal')
            uicontrol(hObject)
            return
        else
            thetaU = input;
        end
    end
    function deltaL_callback(hObject, eventdata, handles)
        input = str2double(get(hObject,'String'));
        if isnan(input)
            errordlg('You must enter a numeric value','Invalid Input','modal')
            uicontrol(hObject)
            return
        else
            deltaL = input;
        end
    end
    function deltaU_callback(hObject, eventdata, handles)
        input = str2double(get(hObject,'String'));
        if isnan(input)
            errordlg('You must enter a numeric value','Invalid Input','modal')
            uicontrol(hObject)
            return
        else
            deltaU = input;
        end
    end

    function recalculate_callback(hObject, eventdata, handles)
        unit_activity_Plot();
    end

    function unit_activity_Plot()
        time = linspace(0, length(eeg)/nsr, length(eeg));
        seeg = (eeg - mean(eeg)) ./ std(eeg); % standardize
        figure; plot(time, seeg)
        thetaFilter = fir1(1024,[thetaL, thetaU]/(nsr/2),'bandpass');
        thetaData = filtfilt(thetaFilter,1,eeg);
        sthetaData = (thetaData - mean(thetaData)) ./ std(eeg); % standardize
        hold on; plot(time, sthetaData);
        deltaFilter = fir1(1024,[deltaL, deltaU]/(nsr/2),'bandpass');
        deltaData = filtfilt(deltaFilter,1,eeg);
        sdeltaData = (deltaData - mean(deltaData)) ./ std(eeg); % standardize
        plot(time, sdeltaData);
        ratio = abs(hilbert(sthetaData))./abs(hilbert(sdeltaData));
        ratio(ratio>10) = 10;
        wSize = nsr*5;
        coeff = ones(1, wSize)/wSize;
        fratio = filtfilt(coeff, 1, ratio);
        plot(time, fratio)
        logicRatio = zeros(size(fratio));
        logicRatio(fratio>1) = 1;
        plot(time, logicRatio, 'k')
        title(['']);
        legend('unit', ['theta:' num2str(thetaL) '-' num2str(thetaU)],...
            ['delta:' num2str(deltaL) '-' num2str(deltaU)], 'ratio', 'istheta');
        xlabel('Seconds');
        hold off;
%         spectralanalysis(eeg', nsr, 1)
    end

end
function channel_selector(varargin)
%CHANNEL_SELECTOR   Select hippocampal theta reference channel.
%   CHANNEL_SELECTOR(VARARGIN) called by CHANNEL_SELECTOR_CALLER(S)
%   Visualizes theta phase change to find pyramidal layer. 
%   Parameters:
%       ANIMAL: animal folder path,
%       RECORD: file name, 
%       THETATIME: time of dominant theta (2x1 vector: start and end point
%           in seconds), 
%       DELTATIME: time of dominant delta (2x1 vector, same as above), 
%       DISPCHANNELS: channel numbers that needs to be displayed (2x1 
%           vector, from channel... to channel...), 
%       S: save?, logic varriable
%       RESDIR: target directory to save outputs
%
%   Reads field data. Makes matrices (e.g.: 65 channel * (10*sampling rate)
%   -> 10 sec long data for each channel) for both dominant theta and dominant
%   delta. (Their time intervalls provided by THETATIME and DELTATIME
%   vectors). Filter to ripple ([90, 220] Hz), and  filter to theta
%   ([THBAND] Hz), then plot data and save figure.
%
%   See also MSHCSP, LOADPAR, CHANNEL_SELECTOR_CALLER, MSPREADEEG_RW.
%
%   Author: Barnabas Kocsis
%   Institute of Experimental Medicine, MTA
%   Date: 18/04/2017

% Global variables
global DATADIR
global RESULTDIR
global SR
global THBAND
DATADIR = 'F:\MSHCSp\';

% Default variables
defaultAnimal = 'viktor5\';
defaultRecord = '201003292';
defaultThetaTime = [970, 975];
defaultDeltaTime = [50, 100];
defaultDispChannels = [1, 32];
defaultSave = false;
defaultResdir = fullfile(RESULTDIR,'channel_selector');

% Input arguments
p = inputParser;
addOptional(p, 'animal', defaultAnimal, @ischar);
addOptional(p, 'record', defaultRecord, @ischar);
addOptional(p, 'thetaTime', defaultThetaTime, @isnumeric)
addOptional(p, 'deltaTime', defaultDeltaTime, @isnumeric)
addOptional(p, 'dispChannels', defaultDispChannels, @isnumeric)
addOptional(p, 's', defaultSave, @islogical)
addOptional(p, 'resdir', defaultResdir, @ischar)
parse(p,varargin{:});

animal = p.Results.animal;
recording = p.Results.record;
thetaTime = p.Results.thetaTime;
deltaTime = p.Results.deltaTime;
dispChannels = p.Results.dispChannels;
s = p.Results.s;
resdir = p.Results.resdir;

% Load parameters
fname = fullfile(DATADIR,animal,recording);
pars = LoadPar([fname '.par']); %Load parameter file
pars.nChannels; % 65
pars.nBits;
pars.nElecGps;
pars.ElecGp;

% Create channel list
pars.ElecGp = cellfun(@(x) x',pars.ElecGp,'UniformOutput',false);
chOrder = cell2mat(pars.ElecGp);   % channel sequence
chOrder = chOrder + 1;   % because of matrix indices can't be 0

% Open data file
datafile = fopen([fname '.dat'],'r');

% Allocate data matrices
ripple_field = zeros(pars.nChannels, (deltaTime(2)-deltaTime(1))*SR);
theta_field = zeros(pars.nChannels, (thetaTime(2)-thetaTime(1))*SR);

% Read data
maxLe = max(thetaTime(2), deltaTime(2));  % we will read only the required amount of data
for x = 1:maxLe
    data = fread(datafile,[pars.nChannels,SR],'int16');  % read one second (1*sampling rate) with int16 precision (reading immediately the whole data would crash the memory)
    if x>deltaTime(1) && x<deltaTime(2)+1  % store resampled data if it's between the provided timepoints (dominant delta)
        ripple_field(1:pars.nChannels, ((x-deltaTime(1)-1)*SR+1):((x-deltaTime(1))*SR)) =  data; 
    end
    if x>thetaTime(1) && x<thetaTime(2)+1  % store resampled data if it's between the provided timepoints (dominant theta)
        theta_field(1:pars.nChannels, ((x-thetaTime(1)-1)*SR+1):((x-thetaTime(1))*SR)) =  data; 
    end
end
fclose(datafile);

% Order channels, based on channel sequence
oRippleField = ripple_field(chOrder,:);
oThetaField = theta_field(chOrder,:);

%used channels:
oRippleField = oRippleField(33:65,:);  % channels used 
oThetaField = oThetaField(33:65,:);
oThetaField = oThetaField(dispChannels(1):dispChannels(2),:);
oRippleField = oRippleField(dispChannels(1):dispChannels(2),:);

%time vector (x axis):
thetaLength = thetaTime(2)-thetaTime(1);
timev = linspace(thetaTime(1),thetaTime(2), thetaLength*SR);

% Orignal (resampled) data
EEGShift = repmat(((1:size(oThetaField, 1))*1800)', 1, (thetaTime(2)-thetaTime(1))*SR);
H = figure;
plot(timev, (EEGShift'+oThetaField'))
set(gca, 'YTick', [1:dispChannels(2)-dispChannels(1)+1]'.*1800, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
title(['EEG, ' recording ', from: ' num2str(thetaTime(1)) ' to ' num2str(thetaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
xlabel('s');

if s
    savefig(fullfile(resdir,[recording '_EEGShift.fig']));
end
close(H);

% Filter data to theta band
thetaFilter = fir1(1024,THBAND/(SR/2),'bandpass');   % theta band
thetaData = filtfilt(thetaFilter,1,oThetaField');
thetaShift = repmat(((1:size(oThetaField, 1))*1800)', 1, (thetaTime(2)-thetaTime(1))*SR);
H = figure;
plot(timev, (thetaShift'+thetaData))
set(gca, 'YTick', [1:dispChannels(2)-dispChannels(1)+1]'.*1800, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
title(['Theta phase change, ' recording ', from: ' num2str(thetaTime(1)) ' to ' num2str(thetaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
xlabel('s');
if s
    savefig(fullfile(resdir,[recording '_thetaShift.fig']));
end

%analying phase change:
% channel_difference(thetaData, thetaTime);

close(H)

deltaLength = deltaTime(2)-deltaTime(1);
timev = linspace(deltaTime(1),deltaTime(2), deltaLength*SR);

rippleFilter = fir1(1024,[90 220]/(SR/2),'bandpass');   % ripple band
rippleData = filtfilt(rippleFilter,1,oRippleField');
rippleShift = repmat(((1:size(oRippleField, 1))*500)', 1, (deltaTime(2)-deltaTime(1))*SR);
H = figure;
plot(timev, (rippleShift'+rippleData))
set(gca, 'YTick', [1:dispChannels(2)-dispChannels(1)+1]'.*500, 'YTickLabel', num2str([dispChannels(1):dispChannels(2)]'));
title(['Ripples, ' recording ', from: ' num2str(deltaTime(1)) ' to ' num2str(deltaTime(2)) ', for channels (top-bottom): ' num2str(dispChannels(2)) '-' num2str(dispChannels(1))]);
xlabel('s');
if s
    savefig(fullfile(resdir,[recording '_ripples.fig']));
end
close(H)

end